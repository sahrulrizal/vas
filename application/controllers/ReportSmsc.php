<?php


defined('BASEPATH') or exit('No direct script access allowed');

class ReportSmsc extends CI_Controller
{

	function __construct()
	{

		parent::__construct();

		$this->load->library('session');
	}

	public function reportDataSmscLima()
	{
		$database = $this->load->database('a2p', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');




		if ($status) {
			$database->where('status', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$database->where('DATE(sms_date) >=', date('Y-m-d', strtotime($startdate)));
			$database->where('DATE(sms_date) <=', date('Y-m-d', strtotime($enddate)));
		}
		$database->order_by('sms_date', 'DESC');
		// $q = $database->get('info_balance');
		$q =  $database->get_where('traffic_sms_det', array('smsc_id' => '5'), 6000);
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}

	public function reportDataSmscEnam()
	{


		$database = $this->load->database('a2p', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');


			if ($status) {
				$database->where('status', $status);
			}

			if ($startdate != "" and $enddate != "") {
				$database->where('DATE(sms_date) >=', date('Y-m-d', strtotime($startdate)));
				$database->where('DATE(sms_date) <=', date('Y-m-d', strtotime($enddate)));
			}
			$database->order_by('sms_date', 'DESC');
			// $q = $database->get('info_balance');
			$q =  $database->get_where('traffic_sms_det', array('smsc_id' => '6'), 6000);
			if ($q) {
				$result = array('success' => true, 'data' => $q->result());
			} else {
				$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
			}
			$result['debugq'] = $this->db->last_query();
			echo json_encode($result);
		}

	public function reportDataSmscDelapan()
	{

		$database = $this->load->database('a2p', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');


		if ($status) {
			$database->where('status', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$database->where('DATE(sms_date) >=', date('Y-m-d', strtotime($startdate)));
			$database->where('DATE(sms_date) <=', date('Y-m-d', strtotime($enddate)));
		}
		$database->order_by('sms_date', 'DESC');
		// $q = $database->get('info_balance');
		$q =  $database->get_where('traffic_sms_det', array('smsc_id' => '8'), 6000);
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}

	public function reportDataSmscSembilan()
	{

		$database = $this->load->database('a2p', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');


		if ($status) {
			$database->where('status', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$database->where('DATE(sms_date) >=', date('Y-m-d', strtotime($startdate)));
			$database->where('DATE(sms_date) <=', date('Y-m-d', strtotime($enddate)));
		}
		$database->order_by('sms_date', 'DESC');
		// $q = $database->get('info_balance');
		$q =  $database->get_where('traffic_sms_det', array('smsc_id' => '9'), 6000);
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}
	//load view tampil data Report Smsc 5
	public function showDataReportSmscLima()
	{
		if ($this->session->userdata('level') == '2') {
			$name = 'showDataReportSmscLima';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}


	//load view tampil data Report Smsc 6
	public function showDataReportSmscEnam()
	{
		if ($this->session->userdata('level') == '2') {
			$name = 'showDataReportSmscEnam';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}


	//load view tampil data Report Smsc 8
	public function showDataReportSmscDelapan()
	{
		if ($this->session->userdata('logged_in')) {
			$name = 'showDataReportSmscDelapan';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}

	//load view tampil data Report Smsc 9
	public function showDataReportSmscSembilan()
	{
		if ($this->session->userdata('logged_in')) {
			$name = 'showDataReportSmscSembilan';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}
}
 
 /* End of file ReportUmb.php */
