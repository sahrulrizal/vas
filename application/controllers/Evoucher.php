<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Evoucher extends CI_Controller
{

	function __construct()
	{

		parent::__construct();

		$this->load->library('session');
	}
	
	public function reportDataEvoucherSdp()
	{
		$vas_refill_voucher = $this->load->database('vas_refill_voucher', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');

		if ($status) {
			$vas_refill_voucher->where('counter', $status);
		}
		
		
		if ($startdate != "" and $enddate != "") {
			$vas_refill_voucher->where('DATE(created_date) >=', date('Y-m-d', strtotime($startdate)));
			$vas_refill_voucher->where('DATE(created_date) <=', date('Y-m-d', strtotime($enddate)));
		}
		$vas_refill_voucher->order_by('created_date', 'desc');
		//cara satu versi farhan mastah wkwkwk
		$q =  $vas_refill_voucher->get_where('info_Evoucher', array('node_id' => '180','source_chanel' => 'EVOUCHER'));
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}

	// report Evoucher 185
	public function reportDataEvoucherSdl()
	{
		$vas_refill_voucher = $this->load->database('vas_refill_voucher', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');

		if ($status) {
			$vas_refill_voucher->where('counter', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$vas_refill_voucher->where('DATE(created_date) >=', date('Y-m-d', strtotime($startdate)));
			$vas_refill_voucher->where('DATE(created_date) <=', date('Y-m-d', strtotime($enddate)));
		}
		//cara dua versi farhan mastah wkwkwk
		$vas_refill_voucher->where('source_chanel="EVOUCHER"');
		$vas_refill_voucher->where('node_id="185"');
		$vas_refill_voucher->order_by('created_date', 'DESC');
		$q = $vas_refill_voucher->get('info_Evoucher');
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}
	//load view tampil data Report BalanceTransfer 180
	public function showDataReportEvoucherSdp()
	{
		if ($this->session->userdata('level')=='2') {
			$name = 'showDataReportEvoucherSdp';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}


	//load view tampil data Report BalanceTransfer 185
	public function showDataReportEvoucherSdl()
	{
		if ($this->session->userdata('level')=='2') {
			$name = 'showDataReportEvoucherSdl';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}



	public function showDataGrafikEvoucherSdp()
	{
		if ($this->session->userdata('level')=='2') {
			$name = 'showDataGrafikEvoucherSdp';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}

	public function showDataGrafikEvoucherSdl()
	{
		if ($this->session->userdata('level')=='2') {
			$name = 'showDataGrafikEvoucherSdl';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}
}
 
 /* End of file ReportUmb.php */
