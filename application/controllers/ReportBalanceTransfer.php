<?php


defined('BASEPATH') or exit('No direct script access allowed');

class ReportBalanceTransfer extends CI_Controller
{

	function __construct()
	{

		parent::__construct();

		$this->load->library('session');
	}
	// public function getStatusBalanceTransfer()
	// {
	// 	$database = $this->load->database('vas_refill_voucher', TRUE);

	// 	$query = $database->query("SELECT * FROM `info_balance` where created_date = date(now())")->result();
	// 	echo json_encode($query);
	// }
	public function reportDataBalanceTransferSdp()
	{
		$vas_refill_voucher = $this->load->database('vas_refill_voucher', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');

		if ($status) {
			$vas_refill_voucher->where('info', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$vas_refill_voucher->where('DATE(created_date) >=', date('Y-m-d', strtotime($startdate)));
			$vas_refill_voucher->where('DATE(created_date) <=', date('Y-m-d', strtotime($enddate)));
		}
		$vas_refill_voucher->order_by('created_date', 'DESC');
		// $q = $vas_refill_voucher->get('info_balance');
		$q =  $vas_refill_voucher->get_where('info_balance', array('node' => '180'));
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}

	// report balance transfer 185
	public function reportDataBalanceTransferSdl()
	{
		$vas_refill_voucher = $this->load->database('vas_refill_voucher', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');

		if ($status) {
			$vas_refill_voucher->where('info', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$vas_refill_voucher->where('DATE(created_date) >=', date('Y-m-d', strtotime($startdate)));
			$vas_refill_voucher->where('DATE(created_date) <=', date('Y-m-d', strtotime($enddate)));
		}
		$vas_refill_voucher->order_by('created_date', 'DESC');
		// $q = $vas_refill_voucher->get('info_balance');
		$q =  $vas_refill_voucher->get_where('info_balance', array('node' => '185'));
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}
	//load view tampil data Report BalanceTransfer 185
	public function showDataReportBalanceTransferSdp()
	{
		if ($this->session->userdata('level') =='2') {
			$name = 'showDataReportBalanceTransferSdp';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}


	//load view tampil data Report BalanceTransfer 185
	public function showDataReportBalanceTransferSdl()
	{
		if ($this->session->userdata('level') =='2') {
			$name = 'showDataReportBalanceTransferSdl';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}
}
 
 /* End of file ReportUmb.php */
