<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Adjust extends CI_Controller {

	public function showGrafikAdjustSdp()
	{
		$name = 'showGrafikAdjustSdp';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	} 
	public function showGrafikAdjustSdl()
	{
		$name = 'showGrafikAdjustSdl';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}  

}

/* End of file Adjust.php */
