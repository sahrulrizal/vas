<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DeleteDatabase extends CI_Controller {

	public function index()
	{
		
			$a2p = $this->load->database('a2p', TRUE);
			// $umb = $this->load->database('umb', TRUE);
			$vas_refill_voucher = $this->load->database('vas_refill_voucher', TRUE);
	
			// query a2p
			$a2p->query("DELETE FROM traffic_sms_det WHERE sms_date < '2020-03-01'");
			$a2p->query("DELETE FROM account_purchase WHERE purchase_date < '2020-03-01'");
			$a2p->query("DELETE FROM loadBalancer WHERE time < '2020-03-01'");
	
			// connect umb default 
			$this->db->query("DELETE FROM ussdnode71 WHERE time < '2020-03-01'");
			$this->db->query("DELETE FROM ussdnode72 WHERE time < '2020-03-01'");
			$this->db->query("DELETE FROM ussdnode80 WHERE time < '2020-03-01'");
			$this->db->query("DELETE FROM ussdnode81 WHERE time < '2020-03-01'");
	
			// query vas_refill_voucher
			$vas_refill_voucher->query("DELETE FROM balance_transfer WHERE created_date < '2020-03-01'");
			$vas_refill_voucher->query("DELETE FROM EVoucher WHERE created_date < '2020-03-01'");
			$vas_refill_voucher->query("DELETE FROM extend_me WHERE created_date < '2020-03-01'");
			$vas_refill_voucher->query("DELETE FROM games_mobiwin WHERE created_date < '2020-03-01'");
			$vas_refill_voucher->query("DELETE FROM games_tl WHERE created_date < '2020-03-01'");
			$vas_refill_voucher->query("DELETE FROM refill_voucher WHERE created_date < '2020-03-01'");
			$vas_refill_voucher->query("DELETE FROM transfer_quota WHERE created_date < '2020-03-01'");
			$vas_refill_voucher->query("DELETE FROM voucher_games WHERE created_date < '2020-03-01'");
		
	}



	

}

/* End of file DeleteDatabase.php */






