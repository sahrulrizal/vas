<?php


defined('BASEPATH') or exit('No direct script access allowed');

class GamesTl extends CI_Controller
{

	function __construct()
	{

		parent::__construct();

		$this->load->library('session');
	}

	public function reportDataGamesTlSdp()
	{
		$database = $this->load->database('vas_refill_voucher', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');




		if ($status) {
			$database->where('info', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$database->where('DATE(created_date) >=', date('Y-m-d', strtotime($startdate)));
			$database->where('DATE(created_date) <=', date('Y-m-d', strtotime($enddate)));
		}
		$database->order_by('created_date', 'DESC');
		// $q = $database->get('info_gamesTl');
		$q =  $database->get_where('info_gamesTl', array('node_id' => '180'), 6000);
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}

	public function reportDataGamesTlSdl()
	{


		$database = $this->load->database('vas_refill_voucher', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');


		if ($status) {
			$database->where('info', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$database->where('DATE(created_date) >=', date('Y-m-d', strtotime($startdate)));
			$database->where('DATE(created_date) <=', date('Y-m-d', strtotime($enddate)));
		}
		$database->order_by('created_date', 'DESC');
		// $q = $database->get('info_balance');
		$q =  $database->get_where('info_gamesTl', array('node_id' => '185'), 6000);
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}


	//load view tampil data Report GamesTl 
	public function showDataReportGamesTlSdp()
	{
		if ($this->session->userdata('level') == '2') {
			$name = 'showDataReportGamesTlSdp';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}


	//load view tampil data Report Smsc 6
	public function showDataReportGamesTlSdl()
	{
		if ($this->session->userdata('level') == '2') {
			$name = 'showDataReportGamesTlSdl';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}


	//load view tampil data Report Smsc 8
	public function showDataReportSmscDelapan()
	{
		if ($this->session->userdata('level') == '2') {
			$name = 'showDataReportSmscDelapan';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}
}
 
 /* End of file ReportUmb.php */
