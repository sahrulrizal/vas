<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	function __construct()
	{

		parent::__construct();

		// $this->load->library('session');
	}

	public function index()
	{
		// if ($this->session->userdata('level') =='2' ) {

			$successRate = ["FAILED_DIALOG_TIMEOUT", "FAILED_PROVIDER_ABORT", "FAILED_DIALOG_USER_ABORT", "FAILED_DIALOG_REJECTED"];




			$vas_refill_voucher = $this->load->database('vas_refill_voucher', TRUE);
			$a2p  = $this->load->database('a2p', TRUE);
			$bonita  = $this->load->database('244', TRUE);


			// query awal smsc 5
			$queryData_SmscLima = $a2p->query("SELECT sms_date time, status counter, COUNT(status) count
	                            FROM	traffic_sms_det
								WHERE TIMESTAMP(sms_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)
								AND smsc_id = '5' GROUP BY status ");

			$rawData_SmscLima = $queryData_SmscLima->result();
			$success_SmscLima = 0;
			$failed_SmscLima = 0;


			foreach ($rawData_SmscLima as $rd) {
				if ($rd->counter == "success") {
					$success_SmscLima += $rd->count;
				} else {
					if ($rd->counter == "success_esme") {
						$success_SmscLima += $rd->count;
					} else {
						$failed_SmscLima += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_SmscLima."<br/>";
			// echo "Failed : ". $failed_SmscLima;
			if ($success_SmscLima != 0) {
				$hasilSukses_SmscLima = $success_SmscLima / ($success_SmscLima + $failed_SmscLima) * 100;
				$data['hasilSukses_SmscLima'] = number_format($hasilSukses_SmscLima, 2);
			} else {
				$data['hasilSukses_SmscLima'] = 0;
			}

			if ($failed_SmscLima != 0) {
				$hasilError_SmscLima = $failed_SmscLima / ($success_SmscLima + $failed_SmscLima) * 100;
				$data['hasilError_SmscLima'] = number_format($hasilError_SmscLima, 2);
			} else {
				$data['hasilError_SmscLima'] = 0;
			}

			// query akhir smsc 5
			// query awal smsc 6
			$queryData_SmscEnam = $a2p->query("SELECT sms_date time, status counter, COUNT(status) count
	                            FROM	traffic_sms_det
								WHERE TIMESTAMP(sms_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)
								AND smsc_id = '6' GROUP BY status ");

			$rawData_SmscEnam = $queryData_SmscEnam->result();
			$success_SmscEnam = 0;
			$failed_SmscEnam = 0;


			foreach ($rawData_SmscEnam as $rd) {
				if ($rd->counter == "success") {
					$success_SmscEnam += $rd->count;
				} else {
					if ($rd->counter == "success_esme") {
						$success_SmscEnam += $rd->count;
					} else {
						$failed_SmscEnam += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_SmscEnam."<br/>";
			// echo "Failed : ". $failed_SmscEnam;
			if ($success_SmscEnam != 0) {
				$hasilSukses_SmscEnam = $success_SmscEnam / ($success_SmscEnam + $failed_SmscEnam) * 100;
				$data['hasilSukses_SmscEnam'] = number_format($hasilSukses_SmscEnam, 2);
			} else {
				$data['hasilSukses_SmscEnam'] = 0;
			}

			if ($failed_SmscEnam != 0) {
				$hasilError_SmscEnam = $failed_SmscEnam / ($success_SmscEnam + $failed_SmscEnam) * 100;
				$data['hasilError_SmscEnam'] = number_format($hasilError_SmscEnam, 2);
			} else {
				$data['hasilError_SmscEnam'] = 0;
			}

			// query akhir smsc 6
			// query awal smsc 8
			$queryData_SmscDelapan = $a2p->query("SELECT sms_date time, status counter, COUNT(status) count
	                            FROM	traffic_sms_det
								WHERE TIMESTAMP(sms_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)
								AND smsc_id = '8' GROUP BY status ");

			$rawData_SmscDelapan = $queryData_SmscDelapan->result();
			$success_SmscDelapan = 0;
			$failed_SmscDelapan = 0;


			foreach ($rawData_SmscDelapan as $rd) {
				if ($rd->counter == "success") {
					$success_SmscDelapan += $rd->count;
				} else {
					if ($rd->counter == "success_esme") {
						$success_SmscDelapan += $rd->count;
					} else {
						$failed_SmscDelapan += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_SmscDelapan."<br/>";
			// echo "Failed : ". $failed_SmscDelapan;
			if ($success_SmscDelapan != 0) {
				$hasilSukses_SmscDelapan = $success_SmscDelapan / ($success_SmscDelapan + $failed_SmscDelapan) * 100;
				$data['hasilSukses_SmscDelapan'] = number_format($hasilSukses_SmscDelapan, 2);
			} else {
				$data['hasilSukses_SmscDelapan'] = 0;
			}

			if ($failed_SmscDelapan != 0) {
				$hasilError_SmscDelapan = $failed_SmscDelapan / ($success_SmscDelapan + $failed_SmscDelapan) * 100;
				$data['hasilError_SmscDelapan'] = number_format($hasilError_SmscDelapan, 2);
			} else {
				$data['hasilError_SmscDelapan'] = 0;
			}

			// query akhir smsc 8

			// query awal smsc 9
			$queryData_SmscSembilan = $a2p->query("SELECT sms_date time, status counter, COUNT(status) count
	                            FROM	traffic_sms_det
								WHERE TIMESTAMP(sms_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)
								AND smsc_id = '9' GROUP BY status ");

			$rawData_SmscSembilan = $queryData_SmscSembilan->result();
			$success_SmscSembilan = 0;
			$failed_SmscSembilan = 0;


			foreach ($rawData_SmscSembilan as $rd) {
				if ($rd->counter == "success") {
					$success_SmscSembilan += $rd->count;
				} else {
					if ($rd->counter == "success_esme") {
						$success_SmscSembilan += $rd->count;
					} else {
						$failed_SmscSembilan += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_SmscSembilan."<br/>";
			// echo "Failed : ". $failed_SmscSembilan;
			if ($success_SmscSembilan != 0) {
				$hasilSukses_SmscSembilan = $success_SmscSembilan / ($success_SmscSembilan + $failed_SmscSembilan) * 100;
				$data['hasilSukses_SmscSembilan'] = number_format($hasilSukses_SmscSembilan, 2);
			} else {
				$data['hasilSukses_SmscSembilan'] = 0;
			}

			if ($failed_SmscSembilan != 0) {
				$hasilError_SmscSembilan = $failed_SmscSembilan / ($success_SmscSembilan + $failed_SmscSembilan) * 100;
				$data['hasilError_SmscSembilan'] = number_format($hasilError_SmscSembilan, 2);
			} else {
				$data['hasilError_SmscSembilan'] = 0;
			}

			// query akhir smsc 9


			// query SimcardPredefine
			$queryData_SimcardPredefine = $bonita->query("SELECT 
		  SUM((SELECT 
			  COUNT(ofd.id)
			FROM
			  output_file_data ofd
			WHERE
			  ofd.msisdn >= td.msisdn_start
				AND ofd.msisdn <= td.msisdn_end
				AND ofd.status = 'N'
				AND ISNULL(ofd.provisioning_date))) AS jml_progress,
		  SUM((SELECT 
			  COUNT(ofd.id)
			FROM
			  output_file_data ofd
			WHERE
			  ofd.msisdn >= td.msisdn_start
				AND ofd.msisdn <= td.msisdn_end
				AND ofd.status = 'Y'
				AND ! ISNULL(ofd.provisioning_date))) AS jml_success,
		  SUM((SELECT 
			  COUNT(ofd.id)
			FROM
			  output_file_data ofd
			WHERE
			  ofd.msisdn >= td.msisdn_start
				AND ofd.msisdn <= td.msisdn_end
				AND ofd.status = 'N'
				AND ! ISNULL(ofd.provisioning_date))) AS jml_failed,
		  td.tx_date
		  FROM
		  trx_detail td,
		  transaction t
		  WHERE
		  td.transaction_fk = t.id
			AND t.jenis_produk = 1
			AND date(td.tx_date) = '2019-11-05'
		GROUP BY date(td.tx_date)
		ORDER BY t.tx_date DESC;");

			$rawData_SimcardPredefine = $queryData_SimcardPredefine->result();
			$success_SimcardPredefine = 0;
			$failed_SimcardPredefine = 0;

			foreach ($rawData_SimcardPredefine as $rd) {
				if ($rd->jml_success != 0) {
					$success_SimcardPredefine += $rd->jml_success;
				} else {
					if ($rd->jml_failed != 0) {
						$failed_SimcardPredefine += $rd->jml_failed;
					}
				}
			}
			// echo "Success : ".$success_SimcardPredefine."<br/>";
			// echo "Failed : ". $failed_SimcardPredefine;
			if ($success_SimcardPredefine != 0) {
				$hasilSukses_SimcardPredefine = $success_SimcardPredefine / ($success_SimcardPredefine + $failed_SimcardPredefine) * 100;
				$data['hasilSukses_SimcardPredefine'] = number_format($hasilSukses_SimcardPredefine, 2);
			} else {
				$data['hasilSukses_SimcardPredefine'] = 0;
			}

			if ($failed_SimcardPredefine != 0) {
				$hasilError_SimcardPredefine = $failed_SimcardPredefine / ($success_SimcardPredefine + $failed_SimcardPredefine) * 100;
				$data['hasilError_SimcardPredefine'] = number_format($hasilError_SimcardPredefine, 2);
			} else {
				$data['hasilError_SimcardPredefine'] = 0;
			}

			// akhir Simcard Predefine


			// query akhir transfer quota 180

			$queryData_transferQuotaSdp = $vas_refill_voucher->query("SELECT created_date time, info counter,count(info) count FROM info_transferQuota WHERE   TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and node ='180'   GROUP BY info");

			$rawData_transferQuotaSdp = $queryData_transferQuotaSdp->result();
			$success_transferQuotaSdp = 0;
			$failed_transferQuotaSdp = 0;


			foreach ($rawData_transferQuotaSdp as $rd) {
				if ($rd->counter == "SUCCESS REVENUE") {
					$success_transferQuotaSdp += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$success_transferQuotaSdp += $rd->count;
					} else if ($rd->counter == "SUCCESS REST TIMEOUT") {
						$success_transferQuotaSdp += $rd->count;
					} else {
						$failed_transferQuotaSdp += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_transferQuotaSdp != 0) {
				$hasilSukses_transferQuotaSdp = $success_transferQuotaSdp / ($success_transferQuotaSdp + $failed_transferQuotaSdp) * 100;
				$data['hasilSukses_transferQuotaSdp'] = number_format($hasilSukses_transferQuotaSdp, 2);
			} else {
				$data['hasilSukses_transferQuotaSdp'] = 0;
			}

			if ($failed_transferQuotaSdp != 0) {
				$hasilError_transferQuotaSdp = $failed_transferQuotaSdp / ($success_transferQuotaSdp + $failed_transferQuotaSdp) * 100;
				$data['hasilError_transferQuotaSdp'] = number_format($hasilError_transferQuotaSdp, 2);
			} else {
				$data['hasilError_transferQuotaSdp'] = 0;
			}

			// akhir akhir transfer quota 180
			// query tranfer  quota 185
			$queryData_transferQuotaSdl = $vas_refill_voucher->query("SELECT created_date time, info counter,count(info) count FROM info_transferQuota WHERE   TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and node ='185'   GROUP BY info");

			$rawData_transferQuotaSdl = $queryData_transferQuotaSdl->result();
			$success_transferQuotaSdl = 0;
			$failed_transferQuotaSdl = 0;


			foreach ($rawData_transferQuotaSdl as $rd) {
				if ($rd->counter == "SUCCESS REVENUE") {
					$success_transferQuotaSdl += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$success_transferQuotaSdl += $rd->count;
					} else if ($rd->counter == "SUCCESS REST TIMEOUT") {
						$success_transferQuotaSdl += $rd->count;
					} else {
						$failed_transferQuotaSdl += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_transferQuotaSdl != 0) {
				$hasilSukses_transferQuotaSdl = $success_transferQuotaSdl / ($success_transferQuotaSdl + $failed_transferQuotaSdl) * 100;
				$data['hasilSukses_transferQuotaSdl'] = number_format($hasilSukses_transferQuotaSdl, 2);
			} else {
				$data['hasilSukses_transferQuotaSdl'] = 0;
			}

			if ($failed_transferQuotaSdl != 0) {
				$hasilError_transferQuotaSdl = $failed_transferQuotaSdl / ($success_transferQuotaSdl + $failed_transferQuotaSdl) * 100;
				$data['hasilError_transferQuotaSdl'] = number_format($hasilError_transferQuotaSdl, 2);
			} else {
				$data['hasilError_transferQuotaSdl'] = 0;
			}


			// akhir transfer quota 180

			// awal gamesMobiwin 180
			$queryData_gamesMobiwinSdp = $vas_refill_voucher->query("SELECT created_date time, info counter,count(info) count FROM info_gamesMobiwin WHERE   TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and node_id  ='180'   GROUP BY info");

			$rawData_gamesMobiwinSdp = $queryData_gamesMobiwinSdp->result();
			$success_gamesMobiwinSdp = 0;
			$failed_gamesMobiwinSdp = 0;


			foreach ($rawData_gamesMobiwinSdp as $rd) {
				if ($rd->counter == "SUCCESS REVENUE") {
					$success_gamesMobiwinSdp += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$success_gamesMobiwinSdp += $rd->count;
					} else if ($rd->counter == "SUCCESS REST TIMEOUT") {
						$success_gamesMobiwinSdp += $rd->count;
					} else {
						$failed_gamesMobiwinSdp += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_gamesMobiwinSdp != 0) {
				$hasilSukses_gamesMobiwinSdp = $success_gamesMobiwinSdp / ($success_gamesMobiwinSdp + $failed_gamesMobiwinSdp) * 100;
				$data['hasilSukses_gamesMobiwinSdp'] = number_format($hasilSukses_gamesMobiwinSdp, 2);
			} else {
				$data['hasilSukses_gamesMobiwinSdp'] = 0;
			}

			if ($failed_gamesMobiwinSdp != 0) {
				$hasilError_gamesMobiwinSdp = $failed_gamesMobiwinSdp / ($success_gamesMobiwinSdp + $failed_gamesMobiwinSdp) * 100;
				$data['hasilError_gamesMobiwinSdp'] = number_format($hasilError_gamesMobiwinSdp, 2);
			} else {
				$data['hasilError_gamesMobiwinSdp'] = 0;
			}
			// akhir Games MObiwin 180


			// awal gamesMobiwin 185
			$queryData_gamesMobiwinSdl = $vas_refill_voucher->query("SELECT created_date time, info counter,count(info) count FROM info_gamesMobiwin WHERE   TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and node_id  ='185'   GROUP BY info");

			$rawData_gamesMobiwinSdl = $queryData_gamesMobiwinSdl->result();
			$success_gamesMobiwinSdl = 0;
			$failed_gamesMobiwinSdl = 0;


			foreach ($rawData_gamesMobiwinSdl as $rd) {
				if ($rd->counter == "SUCCESS REVENUE") {
					$success_gamesMobiwinSdl += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$success_gamesMobiwinSdl += $rd->count;
					} else if ($rd->counter == "SUCCESS REST TIMEOUT") {
						$success_gamesMobiwinSdl += $rd->count;
					} else {
						$failed_gamesMobiwinSdl += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_gamesMobiwinSdl != 0) {
				$hasilSukses_gamesMobiwinSdl = $success_gamesMobiwinSdl / ($success_gamesMobiwinSdl + $failed_gamesMobiwinSdl) * 100;
				$data['hasilSukses_gamesMobiwinSdl'] = number_format($hasilSukses_gamesMobiwinSdl, 2);
			} else {
				$data['hasilSukses_gamesMobiwinSdl'] = 0;
			}

			if ($failed_gamesMobiwinSdl != 0) {
				$hasilError_gamesMobiwinSdl = $failed_gamesMobiwinSdl / ($success_gamesMobiwinSdl + $failed_gamesMobiwinSdl) * 100;
				$data['hasilError_gamesMobiwinSdl'] = number_format($hasilError_gamesMobiwinSdl, 2);
			} else {
				$data['hasilError_gamesMobiwinSdl'] = 0;
			}
			// akhir Games MObiwin 185
			// awal gamestl 180
			$queryData_gamesTlSdp = $vas_refill_voucher->query("SELECT created_date time, info counter,count(info) count FROM info_gamesTl WHERE   TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and node_id  ='180'   GROUP BY info");

			$rawData_gamesTlSdp = $queryData_gamesTlSdp->result();
			$success_gamesTlSdp = 0;
			$failed_gamesTlSdp = 0;


			foreach ($rawData_gamesTlSdp as $rd) {
				if ($rd->counter == "SUCCESS REVENUE") {
					$success_gamesTlSdp += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$success_gamesTlSdp += $rd->count;
					} else if ($rd->counter == "SUCCESS REST TIMEOUT") {
						$success_gamesTlSdp += $rd->count;
					} else {
						$failed_gamesTlSdp += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_gamesTlSdp != 0) {
				$hasilSukses_gamesTlSdp = $success_gamesTlSdp / ($success_gamesTlSdp + $failed_gamesTlSdp) * 100;
				$data['hasilSukses_gamesTlSdp'] = number_format($hasilSukses_gamesTlSdp, 2);
			} else {
				$data['hasilSukses_gamesTlSdp'] = 0;
			}

			if ($failed_gamesTlSdp != 0) {
				$hasilError_gamesTlSdp = $failed_gamesTlSdp / ($success_gamesTlSdp + $failed_gamesTlSdp) * 100;
				$data['hasilError_gamesTlSdp'] = number_format($hasilError_gamesTlSdp, 2);
			} else {
				$data['hasilError_gamesTlSdp'] = 0;
			}
			// akhir Games tl 180

			// awal gamestl 185
			$queryData_gamesTlSdl = $vas_refill_voucher->query("SELECT created_date time, info counter,count(info) count FROM info_gamesTl WHERE   TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and node_id  ='185'   GROUP BY info");

			$rawData_gamesTlSdl = $queryData_gamesTlSdl->result();
			$success_gamesTlSdl = 0;
			$failed_gamesTlSdl = 0;


			foreach ($rawData_gamesTlSdl as $rd) {
				if ($rd->counter == "SUCCESS REVENUE") {
					$success_gamesTlSdl += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$success_gamesTlSdl += $rd->count;
					} else if ($rd->counter == "SUCCESS REST TIMEOUT") {
						$success_gamesTlSdl += $rd->count;
					} else {
						$failed_gamesTlSdl += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_gamesTlSdl != 0) {
				$hasilSukses_gamesTlSdl = $success_gamesTlSdl / ($success_gamesTlSdl + $failed_gamesTlSdl) * 100;
				$data['hasilSukses_gamesTlSdl'] = number_format($hasilSukses_gamesTlSdl, 2);
			} else {
				$data['hasilSukses_gamesTlSdl'] = 0;
			}

			if ($failed_gamesTlSdl != 0) {
				$hasilError_gamesTlSdl = $failed_gamesTlSdl / ($success_gamesTlSdl + $failed_gamesTlSdl) * 100;
				$data['hasilError_gamesTlSdl'] = number_format($hasilError_gamesTlSdl, 2);
			} else {
				$data['hasilError_gamesTlSdl'] = 0;
			}
			// akhir Games tl 185


			// awal voucher games 180
			$queryData_voucherGamesSdp = $vas_refill_voucher->query("SELECT created_date time, info counter,count(info) count FROM info_voucherGames WHERE   TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and node_id  ='180'   GROUP BY info");

			$rawData_voucherGamesSdp = $queryData_voucherGamesSdp->result();
			$success_voucherGamesSdp = 0;
			$failed_voucherGamesSdp = 0;


			foreach ($rawData_voucherGamesSdp as $rd) {
				if ($rd->counter == "SUCCESS REVENUE") {
					$success_voucherGamesSdp += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$success_voucherGamesSdp += $rd->count;
					} else if ($rd->counter == "SUCCESS REST TIMEOUT") {
						$success_voucherGamesSdp += $rd->count;
					} else {
						$failed_voucherGamesSdp += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_voucherGamesSdp != 0) {
				$hasilSukses_voucherGamesSdp = $success_voucherGamesSdp / ($success_voucherGamesSdp + $failed_voucherGamesSdp) * 100;
				$data['hasilSukses_voucherGamesSdp'] = number_format($hasilSukses_voucherGamesSdp, 2);
			} else {
				$data['hasilSukses_voucherGamesSdp'] = 0;
			}

			if ($failed_voucherGamesSdp != 0) {
				$hasilError_voucherGamesSdp = $failed_voucherGamesSdp / ($success_voucherGamesSdp + $failed_voucherGamesSdp) * 100;
				$data['hasilError_voucherGamesSdp'] = number_format($hasilError_voucherGamesSdp, 2);
			} else {
				$data['hasilError_voucherGamesSdp'] = 0;
			}
			// akhir voucher games  180

			// awal voucher games 185
			$queryData_voucherGamesSdl = $vas_refill_voucher->query("SELECT created_date time, info counter,count(info) count FROM info_voucherGames WHERE   TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and node_id  ='185'   GROUP BY info");

			$rawData_voucherGamesSdl = $queryData_voucherGamesSdl->result();
			$success_voucherGamesSdl = 0;
			$failed_voucherGamesSdl = 0;


			foreach ($rawData_voucherGamesSdl as $rd) {
				if ($rd->counter == "SUCCESS REVENUE") {
					$success_voucherGamesSdl += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$success_voucherGamesSdl += $rd->count;
					} else if ($rd->counter == "SUCCESS REST TIMEOUT") {
						$success_voucherGamesSdl += $rd->count;
					} else {
						$failed_voucherGamesSdl += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_voucherGamesSdl != 0) {
				$hasilSukses_voucherGamesSdl = $success_voucherGamesSdl / ($success_voucherGamesSdl + $failed_voucherGamesSdl) * 100;
				$data['hasilSukses_voucherGamesSdl'] = number_format($hasilSukses_voucherGamesSdl, 2);
			} else {
				$data['hasilSukses_voucherGamesSdl'] = 0;
			}

			if ($failed_voucherGamesSdl != 0) {
				$hasilError_voucherGamesSdl = $failed_voucherGamesSdl / ($success_voucherGamesSdl + $failed_voucherGamesSdl) * 100;
				$data['hasilError_voucherGamesSdl'] = number_format($hasilError_voucherGamesSdl, 2);
			} else {
				$data['hasilError_voucherGamesSdl'] = 0;
			}
			// akhir voucher games  185


			// awal ExtendMe 180
			$queryData_extendMeSdp = $vas_refill_voucher->query("SELECT created_date time, info counter,count(info) count FROM info_extendMe WHERE   TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and node_id  ='180'   GROUP BY info");

			$rawData_extendMeSdp = $queryData_extendMeSdp->result();
			$success_extendMeSdp = 0;
			$failed_extendMeSdp = 0;


			foreach ($rawData_extendMeSdp as $rd) {
				if ($rd->counter == "SUCCESS REVENUE") {
					$success_extendMeSdp += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$success_extendMeSdp += $rd->count;
					} else if ($rd->counter == "SUCCESS REST TIMEOUT") {
						$success_extendMeSdp += $rd->count;
					} else {
						$failed_extendMeSdp += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_extendMeSdp != 0) {
				$hasilSukses_extendMeSdp = $success_extendMeSdp / ($success_extendMeSdp + $failed_extendMeSdp) * 100;
				$data['hasilSukses_extendMeSdp'] = number_format($hasilSukses_extendMeSdp, 2);
			} else {
				$data['hasilSukses_extendMeSdp'] = 0;
			}

			if ($failed_extendMeSdp != 0) {
				$hasilError_extendMeSdp = $failed_extendMeSdp / ($success_extendMeSdp + $failed_extendMeSdp) * 100;
				$data['hasilError_extendMeSdp'] = number_format($hasilError_extendMeSdp, 2);
			} else {
				$data['hasilError_extendMeSdp'] = 0;
			}
			// akhir ExtendMe  180

			// awal ExtendMe 185
			$queryData_extendMeSdl = $vas_refill_voucher->query("SELECT created_date time, info counter,count(info) count FROM info_extendMe WHERE   TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and node_id  ='185'   GROUP BY info");

			$rawData_extendMeSdl = $queryData_extendMeSdl->result();
			$success_extendMeSdl = 0;
			$failed_extendMeSdl = 0;


			foreach ($rawData_extendMeSdl as $rd) {
				if ($rd->counter == "SUCCESS REVENUE") {
					$success_extendMeSdl += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$success_extendMeSdl += $rd->count;
					} else if ($rd->counter == "SUCCESS REST TIMEOUT") {
						$success_extendMeSdl += $rd->count;
					} else {
						$failed_extendMeSdl += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_extendMeSdl != 0) {
				$hasilSukses_extendMeSdl = $success_extendMeSdl / ($success_extendMeSdl + $failed_extendMeSdl) * 100;
				$data['hasilSukses_extendMeSdl'] = number_format($hasilSukses_extendMeSdl, 2);
			} else {
				$data['hasilSukses_extendMeSdl'] = 0;
			}

			if ($failed_extendMeSdl != 0) {
				$hasilError_extendMeSdl = $failed_extendMeSdl / ($success_extendMeSdl + $failed_extendMeSdl) * 100;
				$data['hasilError_extendMeSdl'] = number_format($hasilError_extendMeSdl, 2);
			} else {
				$data['hasilError_extendMeSdl'] = 0;
			}
			// akhir ExtendMe  185


			// awal balanceTransfer 185
			$queryData_balanceTransferSdp = $vas_refill_voucher->query("SELECT created_date time, info counter,count(info) count FROM info_balance WHERE   TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and node  ='180'   GROUP BY info");

			$rawData_balanceTransferSdp = $queryData_balanceTransferSdp->result();
			$success_balanceTransferSdp = 0;
			$failed_balanceTransferSdp = 0;


			foreach ($rawData_balanceTransferSdp as $rd) {
				if ($rd->counter == "SUCCESS REVENUE") {
					$success_balanceTransferSdp += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$success_balanceTransferSdp += $rd->count;
					} else if ($rd->counter == "SUCCESS REST TIMEOUT") {
						$success_balanceTransferSdp += $rd->count;
					} else {
						$failed_balanceTransferSdp += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_balanceTransferSdp != 0) {
				$hasilSukses_balanceTransferSdp = $success_balanceTransferSdp / ($success_balanceTransferSdp + $failed_balanceTransferSdp) * 100;
				$data['hasilSukses_balanceTransferSdp'] = number_format($hasilSukses_balanceTransferSdp, 2);
			} else {
				$data['hasilSukses_balanceTransferSdp'] = 0;
			}

			if ($failed_balanceTransferSdp != 0) {
				$hasilError_balanceTransferSdp = $failed_balanceTransferSdp / ($success_balanceTransferSdp + $failed_balanceTransferSdp) * 100;
				$data['hasilError_balanceTransferSdp'] = number_format($hasilError_balanceTransferSdp, 2);
			} else {
				$data['hasilError_balanceTransferSdp'] = 0;
			}
			// akhir balanceTransfer  185
			// awal balanceTransfer 180
			$queryData_balanceTransferSdl = $vas_refill_voucher->query("SELECT created_date time, info counter,count(info) count FROM info_balance WHERE   TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and node  ='185'   GROUP BY info");

			$rawData_balanceTransferSdl = $queryData_balanceTransferSdl->result();
			$success_balanceTransferSdl = 0;
			$failed_balanceTransferSdl = 0;


			foreach ($rawData_balanceTransferSdl as $rd) {
				if ($rd->counter == "SUCCESS REVENUE") {
					$success_balanceTransferSdl += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$success_balanceTransferSdl += $rd->count;
					} else if ($rd->counter == "SUCCESS REST TIMEOUT") {
						$success_balanceTransferSdl += $rd->count;
					} else {
						$failed_balanceTransferSdl += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_balanceTransferSdl != 0) {
				$hasilSukses_balanceTransferSdl = $success_balanceTransferSdl / ($success_balanceTransferSdl + $failed_balanceTransferSdl) * 100;
				$data['hasilSukses_balanceTransferSdl'] = number_format($hasilSukses_balanceTransferSdl, 2);
			} else {
				$data['hasilSukses_balanceTransferSdl'] = 0;
			}

			if ($failed_balanceTransferSdl != 0) {
				$hasilError_balanceTransferSdl = $failed_balanceTransferSdl / ($success_balanceTransferSdl + $failed_balanceTransferSdl) * 100;
				$data['hasilError_balanceTransferSdl'] = number_format($hasilError_balanceTransferSdl, 2);
			} else {
				$data['hasilError_balanceTransferSdl'] = 0;
			}
			// akhir balanceTransfer  185









			//AND TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)
			//   query node umb 180 
			$queryDataUmbDp = $this->db->query("SELECT time,info counter,count(info) count FROM info WHERE TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and counter ='umbdata' and USSD_NODE ='80'   GROUP BY info");
			$rawData_UmbDp = $queryDataUmbDp->result();
			$success_UmbDp = 0;
			$failed_UmbDp = 0;


			foreach ($rawData_UmbDp as $rd) {
				if ($rd->counter == "SUCCESS") {
					$success_UmbDp += $rd->count;
				} else {
					if ($rd->counter == "USER_BEHAVIOUR_ERROR") {
						$success_UmbDp += $rd->count;
					} else if ($rd->counter == "CONTENT_EMPTY") {
						$success_UmbDp += $rd->count;
					} else {
						$failed_UmbDp += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_UmbDp != 0) {
				$hasilSukses_UmbDp = $success_UmbDp / ($success_UmbDp + $failed_UmbDp) * 100;
				$data['hasilSukses_UmbDp'] = number_format($hasilSukses_UmbDp, 2);
			} else {
				$data['hasilSukses_UmbDp'] = 0;
			}

			if ($failed_UmbDp != 0) {
				$hasilError_UmbDp = $failed_UmbDp / ($success_UmbDp + $failed_UmbDp) * 100;
				$data['hasilError_UmbDp'] = number_format($hasilError_UmbDp, 2);
			} else {
				$data['hasilError_UmbDp'] = 0;
			}

			// query node umb 180


			//  awal  query node umb 71 
			$queryDataUmbTps = $this->db->query("SELECT time,info counter,count(info) count FROM `info` WHERE TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and counter ='umbdata' and USSD_NODE ='71'   GROUP BY info");
			$rawData_UmbTps = $queryDataUmbTps->result();
			$success_UmbTps = 0;
			$failed_UmbTps = 0;


			foreach ($rawData_UmbTps as $rd) {
				if ($rd->counter == "SUCCESS") {
					$success_UmbTps += $rd->count;
				} else {
					if ($rd->counter == "USER_BEHAVIOUR_ERROR") {
						$success_UmbTps += $rd->count;
					} else if ($rd->counter == "CONTENT_EMPTY") {
						$success_UmbTps += $rd->count;
					} else {
						$failed_UmbTps += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_UmbTps != 0) {
				$hasilSukses_UmbTps = $success_UmbTps / ($success_UmbTps + $failed_UmbTps) * 100;
				$data['hasilSukses_UmbTps'] = number_format($hasilSukses_UmbTps, 2);
			} else {
				$data['hasilSukses_UmbTps'] = 0;
			}

			if ($failed_UmbTps != 0) {
				$hasilError_UmbTps = $failed_UmbTps / ($success_UmbTps + $failed_UmbTps) * 100;
				$data['hasilError_UmbTps'] = number_format($hasilError_UmbTps, 2);
			} else {
				$data['hasilError_UmbTps'] = 0;
			}

			// akhir query node umb 71

			//  awal  query node umb 72 
			$queryDataUmbTpd = $this->db->query("SELECT time,info counter,count(info) count FROM `info` WHERE  TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) and counter ='umbdata' and USSD_NODE ='72'   GROUP BY info");
			$rawData_UmbTpd = $queryDataUmbTpd->result();
			$success_UmbTpd = 0;
			$failed_UmbTpd = 0;


			foreach ($rawData_UmbTpd as $rd) {
				if ($rd->counter == "SUCCESS") {
					$success_UmbTpd += $rd->count;
				} else {
					if ($rd->counter == "USER_BEHAVIOUR_ERROR") {
						$success_UmbTpd += $rd->count;
					} else if ($rd->counter == "CONTENT_EMPTY") {
						$success_UmbTpd += $rd->count;
					} else {
						$failed_UmbTpd += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_UmbTpd != 0) {
				$hasilSukses_UmbTpd = $success_UmbTpd / ($success_UmbTpd + $failed_UmbTpd) * 100;
				$data['hasilSukses_UmbTpd'] = number_format($hasilSukses_UmbTpd, 2);
			} else {
				$data['hasilSukses_UmbTpd'] = 0;
			}

			if ($failed_UmbTpd != 0) {
				$hasilError_UmbTpd = $failed_UmbTpd / ($success_UmbTpd + $failed_UmbTpd) * 100;
				$data['hasilError_UmbTpd'] = number_format($hasilError_UmbTpd, 2);
			} else {
				$data['hasilError_UmbTpd'] = 0;
			}

			// akhir query node umb 72

			// query balance tranfer 180
			$queryData_balance = $vas_refill_voucher->query('SELECT created_date time,info counter,COUNT(info) count FROM info_balance  where TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)  GROUP BY counter');

			$rawData_balance = $queryData_balance->result();
			$success_balance = 0;
			$failed_balance = 0;


			foreach ($rawData_balance as $rd) {
				if ($rd->counter == "SUCCESS REVENUE") {
					$success_balance += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$success_balance += $rd->count;
					} else if ($rd->counter == "SUCCESS REST TIMEOUT") {
						$success_balance += $rd->count;
					} else {
						$failed_balance += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_balance != 0) {
				$hasilSukses_balance = $success_balance / ($success_balance + $failed_balance) * 100;
				$data['hasilSukses_balance'] = number_format($hasilSukses_balance, 2);
			} else {
				$data['hasilSukses_balance'] = 0;
			}

			if ($failed_balance != 0) {
				$hasilError_balance = $failed_balance / ($success_balance + $failed_balance) * 100;
				$data['hasilError_balance'] = number_format($hasilError_balance, 2);
			} else {
				$data['hasilError_balance'] = 0;
			}

			// akhir query balance transfer


			//query vasrefill voucher -185
			//   $queryData = $vas_refill_voucher->query('SELECT created_date time,info counter,COUNT(info) count FROM vas_refill_voucher.info where TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)  GROUP BY counter');
			$queryData_revil = $vas_refill_voucher->query('SELECT created_date time,info counter,COUNT(info) count FROM info  where TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)  GROUP BY counter');

			$rawData_revil = $queryData_revil->result();
			$success_revil = 0;
			$failed_revil = 0;


			foreach ($rawData_revil as $rd) {
				if ($rd->counter == "SUCCESS REVENUE") {
					$success_revil += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$success_revil += $rd->count;
					} else if ($rd->counter == "SUCCESS REST TIMEOUT") {
						$success_revil += $rd->count;
					} else {
						$failed_revil += $rd->count;
					}
				}
			}
			// echo "Success : ".$success_revil."<br/>";
			// echo "Failed : ". $failed_revil;
			if ($success_revil != 0) {
				$hasilSukses_revil = $success_revil / ($success_revil + $failed_revil) * 100;
				$data['hasilSukses_revil'] = number_format($hasilSukses_revil, 2);
			} else {
				$data['hasilSukses_revil'] = 0;
			}

			if ($failed_revil != 0) {
				$hasilError_revil = $failed_revil / ($success_revil + $failed_revil) * 100;
				$data['hasilError_revil'] = number_format($hasilError_revil, 2);
			} else {
				$data['hasilError_revil'] = 0;
			}

			// akhir query revill voucher 185
			//query ussd 182
			$queryData = $this->db->query('SELECT time , counter , SUM(count) count FROM ussdnode72 where counter!="UMBDATA" AND ussd_node="182"  AND TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) GROUP BY counter');
			// $queryData = $this->db->query('SELECT FROM_UNIXTIME( ( UNIX_TIMESTAMP(time) DIV (15* 60) ) * (15*60) ) time , counter , SUM(count) count FROM umb.ussdnode72 WHERE time BETWEEN "2019-10-03 21:15:00"  AND "2019-10-03 21:30:00" GROUP BY counter');
			$rawData = $queryData->result();
			$success = 0;
			$failed = 0;
			// for($i = 0; $i<= sizeof($successRate);$i++){

			foreach ($rawData as $rd) {
				if ($rd->counter == "SUCCESS") {
					$success += $rd->count;
				} else {
					if ($rd->counter == "FAILED_DIALOG_USER_ABORT") {
						$success += $rd->count;
					} else if ($rd->counter == "FAILED_DIALOG_TIMEOUT") {
						$success += $rd->count;
					} else if ($rd->counter == "FAILED_PROVIDER_ABORT") {
						$success += $rd->count;
					} else if ($rd->counter == "FAILED_DIALOG_REJECTED") {
						$success += $rd->count;
					} else {
						$failed += $rd->count;
					}
				}
			}
			// }
			// echo "Success : ".$success."<br/>";
			// echo "Failed : ". $failed;
			if ($success != 0) {
				$hasilSukses = $success / ($success + $failed) * 100;
				$data['hasilSukses'] = number_format($hasilSukses, 2);
			} else {
				$data['hasilSukses'] = 0;
			}

			if ($failed != 0) {
				$hasilError = $failed / ($success + $failed) * 100;
				$data['hasilError'] = number_format($hasilError, 2);
			} else {
				$data['hasilError'] = 0;
			}

			// akhir query ussd node 182
			//query ussd 187
			$queryDataSdt = $this->db->query('SELECT time , counter , SUM(count) count FROM ussdnode72 where counter!="UMBDATA" AND ussd_node="187"  AND TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) GROUP BY counter');
			// $queryData = $this->db->query('SELECT FROM_UNIXTIME( ( UNIX_TIMESTAMP(time) DIV (15* 60) ) * (15*60) ) time , counter , SUM(count) count FROM umb.ussdnode72 WHERE time BETWEEN "2019-10-03 21:15:00"  AND "2019-10-03 21:30:00" GROUP BY counter');
			$rawData = $queryDataSdt->result();
			$successSdt = 0;
			$failedSdt = 0;
			// for($i = 0; $i<= sizeof($successRate);$i++){

			foreach ($rawData as $rd) {
				if ($rd->counter == "SUCCESS") {
					$successSdt += $rd->count;
				} else {
					if ($rd->counter == "FAILED_DIALOG_USER_ABORT") {
						$successSdt += $rd->count;
					} else if ($rd->counter == "FAILED_DIALOG_TIMEOUT") {
						$successSdt += $rd->count;
					} else if ($rd->counter == "FAILED_PROVIDER_ABORT") {
						$successSdt += $rd->count;
					} else if ($rd->counter == "FAILED_DIALOG_REJECTED") {
						$successSdt += $rd->count;
					} else {
						$failedSdt += $rd->count;
					}
				}
			}
			// }
			// echo "Success : ".$success."<br/>";
			// echo "Failed : ". $failed;
			if ($successSdt != 0) {
				$hasilSuksesSdt = $successSdt / ($successSdt + $failedSdt) * 100;
				$data['hasilSuksesSdt'] = number_format($hasilSuksesSdt, 2);
			} else {
				$data['hasilSuksesSdt'] = 0;
			}

			if ($failedSdt != 0) {
				$hasilErrorSdt = $failedSdt / ($successSdt + $failedSdt) * 100;
				$data['hasilErrorSdt'] = number_format($hasilErrorSdt, 2);
			} else {
				$data['hasilErrorSdt'] = 0;
			}

			// akhir query ussd node 187

			// awal query account purchase 180
			$queryDataApSdp = $a2p->query("SELECT 
											time, info counter, COUNT(info) count 
										FROM
											info_account_purchase
										WHERE
												node_id = '180'
												AND TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)
										GROUP BY counter");
			// $queryData = $this->db->query('SELECT FROM_UNIXTIME( ( UNIX_TIMESTAMP(time) DIV (15* 60) ) * (15*60) ) time , counter , SUM(count) count FROM umb.ussdnode72 WHERE time BETWEEN "2019-10-03 21:15:00"  AND "2019-10-03 21:30:00" GROUP BY counter');
			$rawData = $queryDataApSdp->result();
			$successAccountPurchaseSdp = 0;
			$failedAccountPurchaseSdp = 0;
			// for($i = 0; $i<= sizeof($successRate);$i++){

			foreach ($rawData as $rd) {
				if ($rd->counter == "SUCCESS") {
					$successAccountPurchaseSdp += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$successAccountPurchaseSdp += $rd->count;
					}  else {
						$failedAccountPurchaseSdp += $rd->count;
					}
				}
			}
			// }
			// echo "Success : ".$success."<br/>";
			// echo "Failed : ". $failed;
			if ($successAccountPurchaseSdp != 0) {
				$hasilSuksesAccountPurchaseSdp = $successAccountPurchaseSdp / ($successAccountPurchaseSdp + $failedAccountPurchaseSdp) * 100;
				$data['hasilSuksesAccountPurchaseSdp'] = number_format($hasilSuksesAccountPurchaseSdp, 2);
			} else {
				$data['hasilSuksesAccountPurchaseSdp'] = 0;
			}

			if ($failedAccountPurchaseSdp != 0) {
				$hasilErrorAccountPurchaseSdp = $failedAccountPurchaseSdp / ($successAccountPurchaseSdp + $failedAccountPurchaseSdp) * 100;
				$data['hasilErrorAccountPurchaseSdp'] = number_format($hasilErrorAccountPurchaseSdp, 2);
			} else {
				$data['hasilErrorAccountPurchaseSdp'] = 0;
			}

			// akhir query account purchase 180
			// awal query account purchase 185
			$queryDataApSdl = $a2p->query("SELECT 
											time, info counter, COUNT(info) count 
										FROM
											info_account_purchase
										WHERE
												node_id = '185'
												AND TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)
										GROUP BY counter");
			// $queryData = $this->db->query('SELECT FROM_UNIXTIME( ( UNIX_TIMESTAMP(time) DIV (15* 60) ) * (15*60) ) time , counter , SUM(count) count FROM umb.ussdnode72 WHERE time BETWEEN "2019-10-03 21:15:00"  AND "2019-10-03 21:30:00" GROUP BY counter');
			$rawData = $queryDataApSdl->result();
			$successAccountPurchaseSdl = 0;
			$failedAccountPurchaseSdl = 0;
			// for($i = 0; $i<= sizeof($successRate);$i++){

			foreach ($rawData as $rd) {
				if ($rd->counter == "SUCCESS") {
					$successAccountPurchaseSdl += $rd->count;
				} else {
					if ($rd->counter == "SUCCESS NON REVENUE") {
						$successAccountPurchaseSdl += $rd->count;
					}  else {
						$failedAccountPurchaseSdl += $rd->count;
					}
				}
			}
			// }
			// echo "Success : ".$success."<br/>";
			// echo "Failed : ". $failed;
			if ($successAccountPurchaseSdl != 0) {
				$hasilSuksesAccountPurchaseSdl = $successAccountPurchaseSdl / ($successAccountPurchaseSdl + $failedAccountPurchaseSdl) * 100;
				$data['hasilSuksesAccountPurchaseSdl'] = number_format($hasilSuksesAccountPurchaseSdl, 2);
			} else {
				$data['hasilSuksesAccountPurchaseSdl'] = 0;
			}

			if ($failedAccountPurchaseSdl != 0) {
				$hasilErrorAccountPurchaseSdl = $failedAccountPurchaseSdl / ($successAccountPurchaseSdl + $failedAccountPurchaseSdl) * 100;
				$data['hasilErrorAccountPurchaseSdl'] = number_format($hasilErrorAccountPurchaseSdl, 2);
			} else {
				$data['hasilErrorAccountPurchaseSdl'] = 0;
			}

			// akhir query account purchase 185

			// awal query adjust 180
			$queryDataAdjustSdp = $a2p->query("SELECT 
											time, info counter, COUNT(info) count 
										FROM
										info_account_adjusmentDA
										WHERE
												node_id = '180'
												AND TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)
										GROUP BY counter");
			// $queryData = $this->db->query('SELECT FROM_UNIXTIME( ( UNIX_TIMESTAMP(time) DIV (15* 60) ) * (15*60) ) time , counter , SUM(count) count FROM umb.ussdnode72 WHERE time BETWEEN "2019-10-03 21:15:00"  AND "2019-10-03 21:30:00" GROUP BY counter');
			$rawDataAdjustSdp = $queryDataAdjustSdp->result();
			$successAdjustSdp = 0;
			$failedAdjustSdp = 0;
			// for($i = 0; $i<= sizeof($successRate);$i++){

			foreach ($rawDataAdjustSdp as $rd) {
				if ($rd->counter == "SUCCESS") {
					$successAdjustSdp += $rd->count;
				} else {
					
						$failedAdjustSdp += $rd->count;
					
				}
			}
			// }
			// echo "Success : ".$success."<br/>";
			// echo "Failed : ". $failed;
			if ($successAdjustSdp != 0) {
				$hasilSuksesAdjustSdp = $successAdjustSdp / ($successAdjustSdp + $failedAdjustSdp) * 100;
				$data['hasilSuksesAdjustSdp'] = number_format($hasilSuksesAdjustSdp, 2);
			} else {
				$data['hasilSuksesAdjustSdp'] = 0;
			}

			if ($failedAdjustSdp != 0) {
				$hasilErrorAdjustSdp = $failedAdjustSdp / ($successAdjustSdp + $failedAdjustSdp) * 100;
				$data['hasilErrorAdjustSdp'] = number_format($hasilErrorAdjustSdp, 2);
			} else {
				$data['hasilErrorAdjustSdp'] = 0;
			}

			// akhir query adjust 180

			// awal query adjust 185
			$queryDataAdjustSdl = $a2p->query("SELECT 
											time, info counter, COUNT(info) count 
										FROM
										info_account_adjusmentDA
										WHERE
												node_id = '185'
												AND TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)
										GROUP BY counter");
			// $queryData = $this->db->query('SELECT FROM_UNIXTIME( ( UNIX_TIMESTAMP(time) DIV (15* 60) ) * (15*60) ) time , counter , SUM(count) count FROM umb.ussdnode72 WHERE time BETWEEN "2019-10-03 21:15:00"  AND "2019-10-03 21:30:00" GROUP BY counter');
			$rawDataAdjustSdl = $queryDataAdjustSdl->result();
			$successAdjustSdl = 0;
			$failedAdjustSdl = 0;
			// for($i = 0; $i<= sizeof($successRate);$i++){

			foreach ($rawDataAdjustSdl as $rd) {
				if ($rd->counter == "SUCCESS") {
					$successAdjustSdl += $rd->count;
				} else {
					
						$failedAdjustSdl += $rd->count;
					
				}
			}
			// }
			// echo "Success : ".$success."<br/>";
			// echo "Failed : ". $failed;
			if ($successAdjustSdl != 0) {
				$hasilSuksesAdjustSdl = $successAdjustSdl / ($successAdjustSdl + $failedAdjustSdl) * 100;
				$data['hasilSuksesAdjustSdl'] = number_format($hasilSuksesAdjustSdl, 2);
			} else {
				$data['hasilSuksesAdjustSdl'] = 0;
			}

			if ($failedAdjustSdl != 0) {
				$hasilErrorAdjustSdl = $failedAdjustSdl / ($successAdjustSdl + $failedAdjustSdl) * 100;
				$data['hasilErrorAdjustSdl'] = number_format($hasilErrorAdjustSdl, 2);
			} else {
				$data['hasilErrorAdjustSdl'] = 0;
			}

			// akhir query adjust 185

	// awal query loadbalancer node 68
			$queryDataLoadBalancerEd = $a2p->query("SELECT 
					time, info counter, COUNT(info) count 
				FROM
				  ViewLoadBalancer   
        		WHERE 
				node_id='68'  and info !='' AND TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)
			GROUP BY counter");
		// $queryData = $this->db->query('SELECT FROM_UNIXTIME( ( UNIX_TIMESTAMP(time) DIV (15* 60) ) * (15*60) ) time , counter , SUM(count) count FROM umb.ussdnode72 WHERE time BETWEEN "2019-10-03 21:15:00"  AND "2019-10-03 21:30:00" GROUP BY counter');
		$rawData = $queryDataLoadBalancerEd->result();
		$successLoadBalancerEd = 0;
		$failedLoadBalancerEd = 0;
		// for($i = 0; $i<= sizeof($successRate);$i++){

		foreach ($rawData as $rd) {
		if ($rd->counter == "Success") {
		$successLoadBalancerEd += $rd->count;
		} else {
		  $failedLoadBalancerEd += $rd->count;
		
		}
		}
		// }
		// echo "Success : ".$success."<br/>";
		// echo "Failed : ". $failed;
		if ($successLoadBalancerEd != 0) {
		$hasilSuksesLoadBalancerEd = $successLoadBalancerEd / ($successLoadBalancerEd + $failedLoadBalancerEd) * 100;
		$data['hasilSuksesLoadBalancerEd'] = number_format($hasilSuksesLoadBalancerEd, 2);
		} else {
		$data['hasilSuksesLoadBalancerEd'] = 0;
		}

		if ($failedLoadBalancerEd != 0) {
		$hasilErrorLoadBalancerEd = $failedLoadBalancerEd / ($successLoadBalancerEd + $failedLoadBalancerEd) * 100;
		$data['hasilErrorLoadBalancerEd'] = number_format($hasilErrorLoadBalancerEd, 2);
		} else {
		$data['hasilErrorLoadBalancerEd'] = 0;
		}

	// akhir query loadbalancer node 68

	// awal query loadbalancer node 69
			$queryDataLoadBalancerEd = $a2p->query("SELECT 
			time, info counter, COUNT(info) count 
			FROM
			ViewLoadBalancer   
			WHERE 
			node_id='69'  and info !='' AND TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)
			GROUP BY counter");
			// $queryData = $this->db->query('SELECT FROM_UNIXTIME( ( UNIX_TIMESTAMP(time) DIV (15* 60) ) * (15*60) ) time , counter , SUM(count) count FROM umb.ussdnode72 WHERE time BETWEEN "2019-10-03 21:15:00"  AND "2019-10-03 21:30:00" GROUP BY counter');
			$rawData = $queryDataLoadBalancerEd->result();
			$successLoadBalancerEs = 0;
			$failedLoadBalancerEs = 0;
			// for($i = 0; $i<= sizeof($successRate);$i++){

			foreach ($rawData as $rd) {
			if ($rd->counter == "Success") {
			$successLoadBalancerEs += $rd->count;
			} else {
			$failedLoadBalancerEs += $rd->count;
			
			}
			}
			// }
			// echo "Success : ".$success."<br/>";
			// echo "Failed : ". $failed;
			if ($successLoadBalancerEs != 0) {
			$hasilSuksesLoadBalancerEs = $successLoadBalancerEs / ($successLoadBalancerEs + $failedLoadBalancerEs) * 100;
			$data['hasilSuksesLoadBalancerEs'] = number_format($hasilSuksesLoadBalancerEs, 2);
			} else {
			$data['hasilSuksesLoadBalancerEs'] = 0;
			}

			if ($failedLoadBalancerEs != 0) {
			$hasilErrorLoadBalancerEs = $failedLoadBalancerEs / ($successLoadBalancerEs + $failedLoadBalancerEs) * 100;
			$data['hasilErrorLoadBalancerEs'] = number_format($hasilErrorLoadBalancerEs, 2);
			} else {
			$data['hasilErrorLoadBalancerEs'] = 0;
			}

		// akhir query loadbalancer node 68


			// Etopup
			$db244 = $this->load->database('244', TRUE);
			$queryE = $db244->query("SELECT COUNT(id) AS count,IF(info < 6 AND info > 0,'Success','Failed') AS counter,date FROM historyEtopup WHERE TIMESTAMP(date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) AND info IN (1,2,6) GROUP BY counter");
			$rawDataE = $queryE->result();
			$successE = 0;
			$failedE = 0;

			foreach ($rawDataE as $rd) {
				if ($rd->counter == "Success") {
					$successE += $rd->count;
				} else {
					$failedE += $rd->count;
				}
			}
			if ($successE != 0) {
				$hasilSE = $successE / ($successE + $failedE) * 100;
				$data['hasilSE'] = number_format($hasilSE, 2);
			} else {
				$data['hasilSE'] = 0;
			}


			if ($failedE != 0) {
				$hasilEE = $failedE / ($successE + $failedE) * 100;
				$data['hasilEE'] = number_format($hasilEE, 2);
			} else {
				$data['hasilEE'] = 0;
			}



			// SMSC Daily
			$a2p = $this->load->database('a2p', TRUE);
			$querySmsc = $a2p->query("SELECT sms_date,IF(status = 'success' OR status = 'success_esme','success','failed') AS status,COUNT(*) count FROM traffic_sms_det WHERE DATE(sms_date) = SUBDATE(CURRENT_DATE(), 1) GROUP BY status");
			$rawDataSmsc = $querySmsc->result();
			$successSmsc = 0;
			$failedSmsc = 0;

			foreach ($rawDataSmsc as $rd) {
				if ($rd->status == "success") {
					$successSmsc += $rd->count;
				} else {
					$failedSmsc += $rd->count;
				}
			}
			if ($successSmsc != 0) {
				$hasil = $successSmsc / ($successSmsc + $failedSmsc) * 100;
				$data['successSmsc'] = number_format($hasil, 2);
			} else {
				$data['successSmsc'] = 0;
			}

			if ($failedSmsc != 0) {
				$hasil = $failedSmsc / ($successSmsc + $failedSmsc) * 100;
				$data['failedSmsc'] = number_format($hasil, 2);
			} else {
				$data['failedSmsc'] = 0;
			}
	   
			//  tampil nilai depan Evoucher 180
			$queryData_evoucherSdp = $vas_refill_voucher->query("SELECT created_date time, counter,COUNT(counter) count FROM info_Evoucher  where source_chanel='evoucher' AND node_id='180' AND TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)  GROUP BY counter");

			$rawData_evoucherSdp = $queryData_evoucherSdp->result();
			$success_evoucherSdp = 0;
			$failed_evoucherSdp = 0;


			foreach ($rawData_evoucherSdp as $rd) {
				if ($rd->counter == "SUCCESS") {
					$success_evoucherSdp += $rd->count;
				} else {
					if ($rd->counter == "BALANCE TIDAK CUKUP") {
						$success_evoucherSdp += $rd->count;
					} else{

						$failed_evoucherSdp += $rd->count;
					}
					
				}
			}
			// echo "Success : ".$success_evoucherSdp."<br/>";
			// echo "Failed : ". $failed_evoucherSdp;
			if ($success_evoucherSdp != 0) {
				$hasilSukses_evoucherSdp = $success_evoucherSdp / ($success_evoucherSdp + $failed_evoucherSdp) * 100;
				$data['hasilSukses_evoucherSdp'] = number_format($hasilSukses_evoucherSdp, 2);
			} else {
				$data['hasilSukses_evoucherSdp'] = 0;
			}

			if ($failed_evoucherSdp != 0) {
				$hasilError_evoucherSdp = $failed_evoucherSdp / ($success_evoucherSdp + $failed_evoucherSdp) * 100;
				$data['hasilError_evoucherSdp'] = number_format($hasilError_evoucherSdp, 2);
			} else {
				$data['hasilError_evoucherSdp'] = 0;
			}


			// akhir nilai Evoucher 180

			//  tampil nilai depan Evoucher 185
			$queryData_evoucherSdl = $vas_refill_voucher->query("SELECT created_date time, counter,COUNT(counter) count FROM info_Evoucher  where source_chanel='evoucher' AND node_id='185' AND TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)  GROUP BY counter");

			$rawData_evoucherSdl = $queryData_evoucherSdl->result();
			$success_evoucherSdl = 0;
			$failed_evoucherSdl = 0;


			foreach ($rawData_evoucherSdl as $rd) {
				if ($rd->counter == "SUCCESS") {
					$success_evoucherSdl += $rd->count;
				} else {
					
					if ($rd->counter == "BALANCE TIDAK CUKUP") {
						$success_evoucherSdl += $rd->count;
					} else{

						$failed_evoucherSdl += $rd->count;
					}
					
				}
			}
			// echo "Success : ".$success_evoucherSdl."<br/>";
			// echo "Failed : ". $failed_evoucherSdl;
			if ($success_evoucherSdl != 0) {
				$hasilSukses_evoucherSdl = $success_evoucherSdl / ($success_evoucherSdl + $failed_evoucherSdl) * 100;
				$data['hasilSukses_evoucherSdl'] = number_format($hasilSukses_evoucherSdl, 2);
			} else {
				$data['hasilSukses_evoucherSdl'] = 0;
			}

			if ($failed_evoucherSdl != 0) {
				$hasilError_evoucherSdl = $failed_evoucherSdl / ($success_evoucherSdl + $failed_evoucherSdl) * 100;
				$data['hasilError_evoucherSdl'] = number_format($hasilError_evoucherSdl, 2);
			} else {
				$data['hasilError_evoucherSdl'] = 0;
			}


			// akhir nilai Evoucher 180

			
            


			$name = 'dashboard';
            
			$data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		// } else {
		// 	redirect('login');
		// }
	}

	public function detail()
	{
		if ($this->session->userdata('level') =='2' ) {
			$name = 'detail';

			$data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}
	public function games()
	{
		if ($this->session->userdata('level') =='2' ) {
			$name = 'games';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}

	public function a2p()
	{
		if ($this->session->userdata('level') =='2' ) {
			$name = 'smsc';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}

	public function smsc()
	{
		if ($this->session->userdata('level') =='2' ) {
			$name = 'a2p';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}

	public function cdr_vouchergame()
	{
		if ($this->session->userdata('level') =='2' ) {
			$name = 'cdr_vouchergame';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}

	public function detail_googleplay()
	{
		if ($this->session->userdata('level') =='2' ) {
			$name = 'detail_googleplay';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}

	public function  purchasePendingMobiwin()
	{
		$name = 'purchasePendingMobiwin';

		// $data['js'] = $name . '.js';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	// public function  purchasePendingMobiwin()
	// {
	// 	$name = 'purchasePendingMobiwin';

	// 	// $data['js'] = $name . '.js';
	// 	$data['file'] = 'content/' . $name;

	// 	$this->load->view('index', $data);
	// }

	public function ussd()
	{
		$name = 'ussd';

		// $data['js'] = $name . '.js';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	public function ussdSdt()
	{
		$name = 'ussdSdt';

		// $data['js'] = $name . '.js';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	public function refillVoucher()
	{
		$name = 'refillVoucher';

		// $data['js'] = $name . '.js';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}


	public function BalanceTransfer()
	{
		$name = 'BalanceTransfer';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	public function etopup()
	{
		$name = 'etopup';

		// $data['js'] = $name . '.js';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	public function report_etopup()
	{
		$name = 'report_etopup';


		// $data['js'] = $name . '.js';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	public function smscdet()
	{
		$name = 'smscdet';


		// $data['js'] = $name . '.js';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}


	// show grafik view node 

	public function showgrafiknodeTps()
	{
		$name = 'showgrafiknodeTps';


		// $data['js'] = $name . '.js';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	public function showgrafiknodeTpd()
	{
		$name = 'showgrafiknodeTpd';


		// $data['js'] = $name . '.js';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	public function showgrafiknodeDp()
	{
		$name = 'showgrafiknodeTpd';


		// $data['js'] = $name . '.js';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	// show grafik view node

	public function simcard_predefine()
	{
		$name = 'simcard_predefine';


		// $data['js'] = $name . '.js';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	// transferquota180
	public function TransferQuotaSdp()
	{
		$name = 'TransferQuotaSdp';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}
	//transfer quota 185

	public function TransferQuotaSdl()
	{
		$name = 'TransferQuotaSdl';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}
	//Games Mobiwin 180
	public function gamesMobiwinSdp()
	{
		$name = 'gamesMobiwinSdp';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	//Games Mobiwin 185
	public function gamesMobiwinSdl()
	{
		$name = 'gamesMobiwinSdl';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	// Games TL 180
	public function gamesTlSdp()
	{
		$name = 'gamesTlSdp';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	// Games TL 185
	public function gamesTlSdl()
	{
		$name = 'gamesTlSdl';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	//voucher games 180
	public function voucherGamesSdp()
	{
		$name = 'voucherGamesSdp';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	//voucher games 185		
	public function voucherGamesSdl()
	{
		$name = 'voucherGamesSdl';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	//Extend Me 180
	public function extendmeSdp()
	{
		$name = 'extendmeSdp';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	//Extend Me 185
	public function extendmeSdl()
	{
		$name = 'extendmeSdl';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}
	// BalanceTransfer 180
	public function BalanceTransferSdp()
	{
		$name = 'BalanceTransferSdp';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	//Balance Transfer 185
	public function BalanceTransferSdl()
	{
		$name = 'BalanceTransferSdl';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	//SMSC 5
	public function smscLima()
	{
		$name = 'smscLima';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	//SMSC 5
	public function smscEnam()
	{
		$name = 'smscEnam';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	public function smscDelapan()
	{
		$name = 'smscDelapan';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	public function smscSembilan()
	{
		$name = 'smscSembilan';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	public function simcardprefidene()
	{
		$name = 'simcardprefidene';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

	// account purchase view grafik

	public function showdataAccountVoucherSdp()
	{
		$name = 'showdataAccountVoucherSdp';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}   
	
	// akhir account purchase

	// account purchase view grafik

	public function showdataAccountVoucherSdl()
	{
		$name = 'showdataAccountVoucherSdl';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}   
	
	// akhir account purchase


	public function limaEn()
	{
		$name = 'limaEn';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}






	public function hariini()
	{

		$data =  $this->db->query("SELECT * FROM `info` WHERE time='2019-11-05'")->result();
		echo json_encode($data);
		//query vasrefill voucher -185

		$vas_refill_voucher = $this->load->database('vas_refill_voucher', TRUE);

		//   $queryData = $vas_refill_voucher->query('SELECT created_date time,info counter,COUNT(info) count FROM vas_refill_voucher.info where TIMESTAMP(created_date) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE)  GROUP BY counter');
		$queryData_revil = $vas_refill_voucher->query('SELECT created_date time,info counter,COUNT(info) count FROM info  GROUP BY counter');

		$rawData_revil = $queryData_revil->result();
		$success_revil = 0;
		$failed_revil = 0;


		foreach ($rawData_revil as $rd) {
			if ($rd->counter == "SUCCESS REVENUE") {
				$success_revil += $rd->count;
			} else {
				if ($rd->counter == "SUCCESS NON REVENUE") {
					$success_revil += $rd->count;
				} else {
					$failed_revil += $rd->count;
				}
			}
		}
		// echo "Success : ".$success_revil."<br/>";
		// echo "Failed : ". $failed_revil;
		if ($success_revil != 0) {
			$hasilSukses_revil = $success_revil / ($success_revil + $failed_revil) * 100;
			$data['hasilSukses_revil'] = number_format($hasilSukses_revil, 2);
		} else {
			$data['hasilSukses_revil'] = 0;
		}

		if ($failed_revil != 0) {
			$hasilError_revil = $failed_revil / ($success_revil + $failed_revil) * 100;
			$data['hasilError_revil'] = number_format($hasilError_revil, 2);
		} else {
			$data['hasilError_revil'] = 0;
		}

		// akhir query baru


		// $queryData = $this->db->query('SELECT time , counter , SUM(count) count FROM ussdnode72 where TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 135 MINUTE) GROUP BY counter');
		// $rawData = $queryData->result();
		// $success = 0;
		// $failed = 0;
		// echo "Success : ".$success."<br/>";
		// 	echo "Failed : ". $failed;
		// 	if($success != 0){
		// 		$hasilSukses = $success/($success + $failed) * 100;
		// 		$data['hasilSukses'] = number_format($hasilSukses,2);
		// 	 }else{
		// 		 $data['hasilSukses'] = 0;
		// 	 }

		// 	 if($failed != 0){
		// 		 $hasilError = $failed/($success + $failed)*100;
		// 		 $data['hasilError'] = number_format($hasilError,2);
		// 	 }else{
		// 		 $data['hasilError']= 0; 
		// 	 }
		// $queryData = $this->db->query('SELECT FROM_UNIXTIME( ( UNIX_TIMESTAMP(time) DIV (15* 60) ) * (15*60) ) time , counter , SUM(count) count FROM ussdnode72 WHERE time >=now()-interval 15 minute GROUP BY counter');
		// // $queryData = $this->db->query('SELECT FROM_UNIXTIME( ( UNIX_TIMESTAMP(time) DIV (15* 60) ) * (15*60) ) time , counter , SUM(count) count FROM umb.ussdnode72 WHERE time BETWEEN "2019-10-03 21:15:00"  AND "2019-10-03 21:30:00" GROUP BY counter');
		// $rawData = $queryData->result();
		// $success = 0;
		// $failed = 0;
		// // for($i = 0; $i<= sizeof($successRate);$i++){

		// foreach($rawData as $rd){
		// 	if($rd->counter=="SUCCESS"){
		// 		$success += $rd->count;
		// 	}else{
		// 		if($rd->counter=="FAILED_DIALOG_USER_ABORT"){
		// 			$success += $rd->count;
		// 		}else if($rd->counter=="FAILED_DIALOG_TIMEOUT"){
		// 			$success += $rd->count;
		// 		}else if($rd->counter=="FAILED_PROVIDER_ABORT"){
		// 			$success += $rd->count;
		// 		}else if($rd->counter=="FAILED_DIALOG_REJECTED"){
		// 			$success += $rd->count;
		// 		}else{
		// 			$failed += $rd->count;
		// 		}
		// 	}
		// }
		// // }
		// echo "Success : ".$success."<br/>";
		// echo "Failed : ". $failed;
		// $hasilSukses = $success/($success + $failed) * 100;
		// $data['hasilSukses'] = number_format($hasilSukses,2);
		// // echo "<br>";
		// $hasilError = $failed/($success + $failed)*100;
		// echo  number_format($hasilError,2);

		// echo  Date("Y-m-d H:i:s", time()+60*60*7);
		// echo "<br>";
		// echo Date("Y-m-d H:i:s", time());

		// $q = 20;
		// $b = 22;
		// $o = 23;
		// $c = 24;

		// $waktu_Sekarang= 35;
		// $kurang = 15;
		// $hasilKUrang = $waktu_Sekarang - $kurang; //jadi 20
		// echo "ini hasil kurang".$hasilKUrang;
		// echo "<br>";

		// if ($q >= $hasilKUrang ) {
		// 	echo "ini lebih dari 20 karena nilai ini adalah ".$q ;
		// }elseif ($b >= $hasilKUrang) {
		// 	echo "ini lebih dari 20 karena nilai ini adalah ".$b;	
		// }elseif ($o >= $hasilKUrang) {
		// 	echo "ini lebih dari 20 karena nilai ini adalah ".$o;	
		// }elseif($c >= $hasilKUrang) {
		// 	echo "ini lebih dari 20 karena nilai ini adalah ".$c;	
		// }else{
		// 	echo "ini ketika sudah sama dengan 20".$hasilKUrang;

		// $waktuserver = 40;
		// $waktucron = 30;

		// $jarak = $waktuserver - 10;
		// echo $jarak;
		// if ($waktucron =$jarak) {
		// 	echo "nilai sudah sama,nilai nya adalah ".$jarak;
		// }elseif($waktucron>= $jarak){
		//  echo "oke ini elseif";
		// } else {
		// 	echo "nilai tidak sama";
		// }


	}

	// WHEN (`vas_refill_voucher`.`voucher_games`.`response_code` = 500) THEN 'ERROR'
	// WHEN
	// 	((`vas_refill_voucher`.`voucher_games`.`response_code` = 0)
	// 		AND (`vas_refill_voucher`.`voucher_games`.`response_client` = 0))
	// THEN
	// 	'SUCCESS REVENUE'
	// WHEN
	// 	((`vas_refill_voucher`.`voucher_games`.`response_code` = 0)
	// 		AND (`vas_refill_voucher`.`voucher_games`.`response_client` = 1))
	// THEN
	// 	'SUCCESS REST TIMEOUT'
	// WHEN
	// 	((`vas_refill_voucher`.`voucher_games`.`response_code` <> 0)
	// 		AND (`vas_refill_voucher`.`voucher_games`.`response_client` = 1))
	// THEN
	// 	'SUCCESS NON REVENUE'
	// WHEN
	// 	((`vas_refill_voucher`.`voucher_games`.`response_code` <> 0)
	// 		AND (`vas_refill_voucher`.`voucher_games`.`response_client` = 0))
	// THEN
	// 	'SUCCESS NON REVENUE'
	// WHEN
	// 	(((`vas_refill_voucher`.`voucher_games`.`response_code` <> 0)
	// 		AND (`vas_refill_voucher`.`voucher_games`.`response_client` <> 1))
	// 		OR (`vas_refill_voucher`.`voucher_games`.`response_code` <> 500))
	// THEN
	// 	'SUCCESS NON REVENUE'


}
