<?php


defined('BASEPATH') or exit('No direct script access allowed');

class UssdReportController extends CI_Controller
{

	function __construct()
	{

		parent::__construct();

		$this->load->library('session');
	}

	// report ussd 182
	public function reportDataUssdSdd()
	{
		// $vas_refill_voucher = $this->load->database('vas_refill_voucher', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');

		if ($status) {
			$this->db->where('counter', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$this->db->where('DATE(time) >=', date('Y-m-d', strtotime($startdate)));
			$this->db->where('DATE(time) <=', date('Y-m-d', strtotime($enddate)));
		}
		$this->db->order_by('time', 'DESC');
		// $q = $this->db->get('info_VoucherGames');
		$q =  $this->db->get_where('ussdnode72', array('USSD_NODE' => '182'));
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}

	// report ussd 187
	public function reportDataUssdSdt()
	{
		// $vas_refill_voucher = $this->load->database('vas_refill_voucher', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');

		if ($status) {
			$this->db->where('counter', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$this->db->where('DATE(time) >=', date('Y-m-d', strtotime($startdate)));
			$this->db->where('DATE(time) <=', date('Y-m-d', strtotime($enddate)));
		}
		$this->db->order_by('time', 'DESC');
		// $q = $this->db->get('info_VoucherGames');
		$q =  $this->db->get_where('ussdnode72', array('USSD_NODE' => '187'));
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}
	//load view tampil data Report ussd 182
	public function showDataReportUssdSdd()
	{
		if ($this->session->userdata('level') == '2') {
			$name = 'showDataReportUssdSdd';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}


	//load view tampil data Report ussd 187
	public function showDataReportUssdSdt()
	{
		if ($this->session->userdata('level') == '2') {
			$name = 'showDataReportUssdSdt';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}


	// get status filter usdd
	public function getStatusUssd()
	{
		$query = $this->db->query("SELECT * FROM ussdnode72 where counter !='UMBDATA' group by counter ")->row();
		echo json_encode($query);
	}
}
 
 /* End of file ReportUmb.php */
