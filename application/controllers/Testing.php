<?php

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testing extends CI_Controller {

	public function index()
	{

		$name = 'testing';
		$data['js'] = $name . '.js';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}

}

/* End of file Testing.php */
/* Location: ./application/controllers/Testing.php */