<?php


defined('BASEPATH') or exit('No direct script access allowed');

class ReportRbt extends CI_Controller
{

	function __construct()
	{

		parent::__construct();

		// $this->load->library('session');
	}

	

	public function getDataBton()
	{


		$btone = $this->load->database('btone', TRUE);

		$artist = $this->input->get('artist');
		$sessionlevel = $this->session->userdata('level');
		$sessionusername = $this->session->userdata('username');
		

		if ($artist) {
			$btone->where('tones.artist =',$artist);
		}
         
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');
		if ($startdate != "" and $enddate != "") {
			$btone->where('DATE(tonesbox.timestamp) >=', date('Y-m-d', strtotime($startdate)));
			$btone->where('DATE(tonesbox.timestamp) <=', date('Y-m-d', strtotime($enddate)));
			
		}
		// berdasarkan level ,artist  = 1 dan superuser (sts dan tcel) level = 2 
        if ($sessionlevel == 2) { //level 2 = super user(sts dan tcel)
			$btone->select('tonesbox.timestamp,tonesbox
			.msisdn,tones.name,tones.artist,tones.voucher_price');
			$btone->from('tonesbox');
			$btone->join('tones', 'tonesbox.toneid = tones.toneid','LEFT');
			$btone->limit(10000);
			$q = $btone->get();
		}else if ($sessionlevel == 1) {  //level 1 = artist
			$btone->select('tonesbox.timestamp,tonesbox
			.msisdn,tones.name,tones.artist,tones.voucher_price');
			$btone->from('tonesbox');
			$btone->join('tones', 'tonesbox.toneid = tones.toneid','LEFT');
			$btone->where('tones.artist =',$sessionusername);

			$btone->limit(10000);
			$q = $btone->get();
		}
		
		// $q = $btone->query(" SELECT tonesbox.timestamp,tonesbox.msisdn,tones.name,tones.artist FROM `tonesbox` INNER JOIN tones ON tones.toneid=tonesbox.toneid limit 20000");
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
		// SELECT * FROM `tonesbox` INNER JOIN tones ON tones.toneid=tonesbox.toneid
	}

	public function testJson()
	{
		$btone = $this->load->database('btone', TRUE);
		// $kelas = [];
        $result = ['kelas' => array(),'data'=> array()];
		$query = $btone->query('SELECT count(tonesbox.timestamp) as count,tonesbox.timestamp,tonesbox.msisdn,tones.name,tones.artist FROM `tonesbox` INNER JOIN tones ON tones.toneid=tonesbox.toneid GROUP BY tonesbox.timestamp limit 100 ')->result();
		
		foreach ($query as $key => $row) {
			array_push($result['kelas'],$row->count);
			// array_push($result['data'],$row);
		}
		echo json_encode($result);
		
		
	}


	public function getArtist()
	{
		$btone = $this->load->database('btone', TRUE);
		$datasesion = $this->session->userdata('username');
       
		$queryData = $btone->query("SELECT toneid,artist  FROM btone.tones group by artist");
		// echo json_encode($queryData);
		if ($queryData) {
			$result = array('success' => true, 'data' => $queryData->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data artist');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
		
	}


	public function showGrafikRbt()
	{
		$name = 'showGrafikRbt';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	} 
	public function reportRbt()
	{
		$name = 'reportRbt';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}  

	

	
}
 
 /* End of file ReportUmb.php */
