<?php


defined('BASEPATH') or exit('No direct script access allowed');

class ReportUmb extends CI_Controller
{

	function __construct()
	{

		parent::__construct();

		$this->load->library('session');
	}
	public function reportDataUmbDp()
	{

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');


		if ($status) {
			$this->db->where('info', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$this->db->where('DATE(time) >=', date('Y-m-d', strtotime($startdate)));
			$this->db->where('DATE(time) <=', date('Y-m-d', strtotime($enddate)));
		}
		$this->db->order_by('time', 'DESC');
		// $q = $this->db->get('info_balance');
		$q =  $this->db->get_where('info', array('USSD_NODE' => '80'), 6000);
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}

	public function reportDataUmbTps()
	{


		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');


		if ($status) {
			$this->db->where('info', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$this->db->where('DATE(time) >=', date('Y-m-d', strtotime($startdate)));
			$this->db->where('DATE(time) <=', date('Y-m-d', strtotime($enddate)));
		}
		$this->db->order_by('time', 'DESC');
		// $q = $this->db->get('info_balance');
		$q =  $this->db->get_where('info', array('USSD_NODE' => '71'), 6000);
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}

	public function reportDataUmbTpd()
	{

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');


		if ($status) {
			$this->db->where('info', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$this->db->where('DATE(time) >=', date('Y-m-d', strtotime($startdate)));
			$this->db->where('DATE(time) <=', date('Y-m-d', strtotime($enddate)));
		}
		$this->db->order_by('time', 'DESC');
		// $q = $this->db->get('info_balance');
		$q =  $this->db->get_where('info', array('USSD_NODE' => '72'), 6000);
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}
	//load view tampil data umb node 80
	public function showDataUmbNodeDP()
	{
		if ($this->session->userdata('level') == '2' ) {
			$name = 'showdataUmbNodeDP';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}


	//load view tampil data umb node 71
	public function showDataUmbNodeTps()
	{
		if ($this->session->userdata('level') == '2' ) {
			$name = 'showdataUmbNodeTps';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}


	//load view tampil data umb node 71
	public function showDataUmbNodeTpd()
	{
		if ($this->session->userdata('level') == '2' ) {
			$name = 'showdataUmbNodeTpd';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}


	// membuat query for smsc
	public function reportDataSmscLima()
	{

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');


		if ($status != '') {
			$data =  $this->db->query("SELECT time,info counter,count(info) count FROM `info` WHERE info='$status' and counter ='umbdata' and USSD_NODE ='80'  GROUP BY info");
		} elseif ($startdate != '' and $enddate != '') {
			$data =  $this->db->query("SELECT time,info counter,count(info) count FROM `info` WHERE  counter ='umbdata' and USSD_NODE ='80' AND  time >= '$startdate' AND time <= '$enddate'   GROUP BY info");
		} elseif ($status != '' and $startdate != '' and $enddate != '') {
			$data =  $this->db->query("SELECT time,info counter,count(info) count FROM `info` WHERE info='$status' and counter ='umbdata' and USSD_NODE ='80' AND  time >= '$startdate' AND time <= '$enddate'   GROUP BY info");
		} else {
			$data =  $this->db->query("SELECT time,info counter,count(info) count FROM `info` WHERE  counter ='umbdata' and USSD_NODE ='80' GROUP BY info order by time desc");
		}
		$hasil = $data;

		if ($hasil) {
			$result = array('success' => true, 'data' => $hasil->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data ');
		}
		echo json_encode($result);
	}
	// akhir query smsc




}
 
 /* End of file ReportUmb.php */
