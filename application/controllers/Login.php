<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct() {

        parent::__construct();
        date_default_timezone_set("Asia/Jayapura");

		$this->load->library('session');
		$this->load->model('ModelLogin');
    }

	public function index()
	{
		$name = 'dashboard';

		$data['js'] = $name.'.js';
		$data['file'] = 'content/'.$name;

		$this->load->view('login');
	}

	public function check_login()
	{
		$btone = $this->load->database('vas_refill_voucher',TRUE);

		
		$log = [];
        $username = htmlspecialchars($_POST['username']);
        $password = htmlspecialchars($_POST['password']);
		// $user = $this->ModelLogin->islogin($data);
        $user = $btone->get_where('users', array('username' => $username, 'password' => $password))->row_array();
		

		// echo $username;
		// echo $user['id_artis'];
		// echo $user->id_artis;
		// echo $this->session->use rdata('username');
        if ($user) {
			// $data = array();
            $data = array(
                'id_artist' => $user['id_artist'],
                'username' => $user['username'],
                'level' => $user['level'],
            );

            $this->session->set_userdata($data);
			
			if ($user['username'] == 'sts') {
				$log = [
					'status' => true,
					'url' => base_url('index.php/Dashboard'),
					'session' => $data,
				];
			}else if($user['username'] == 'tcel'){
				$log = [
					'status' => true,
					'url' => base_url('index.php/Dashboard'),
					'session' => $data,
				];
			}else{
				// login untuk artis
				$log = [
					'status' => true,
					'url' => base_url('index.php/ReportRbt/reportRbt'),
					'session' => $data,
				];

				$this->session->flashdata('artist','Berhasil Login');
				
			}
            
			

            //  OPEN :: LOG
            // $this->load->model('LogModel', 'lm');

            // $msgLog = "User : " . $user['username'] . " -> Login";
            // $this->lm->id_user = $user['id'];
            // $this->lm->inLogActivity($msgLog);
        } else {
            $log = [
                'status' => false,
                'msg' => 'Tidak dapat login, harap periksa kembali username dan password anda',
            ];
		}
		// redirect('ReportRbt/showGrafikRbt');

        echo json_encode($log);
	}

	public function Logout()
	{
		$this->session->sess_destroy();

		redirect('login');
	}
}
