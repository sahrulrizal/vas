<?php


defined('BASEPATH') or exit('No direct script access allowed');

class VoucherGames extends CI_Controller
{

	function __construct()
	{

		parent::__construct();

		$this->load->library('session');
	}

	public function reportDataVoucherGamesSdp()
	{
		$vas_refill_voucher = $this->load->database('vas_refill_voucher', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');

		if ($status) {
			$vas_refill_voucher->where('info', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$vas_refill_voucher->where('DATE(created_date) >=', date('Y-m-d', strtotime($startdate)));
			$vas_refill_voucher->where('DATE(created_date) <=', date('Y-m-d', strtotime($enddate)));
		}
		$vas_refill_voucher->order_by('created_date', 'DESC');
		// $q = $vas_refill_voucher->get('info_VoucherGames');
		$q =  $vas_refill_voucher->get_where('info_voucherGames', array('node_id' => '180'));
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}

	// report balance transfer 185
	public function reportDataVoucherGamesSdl()
	{
		$vas_refill_voucher = $this->load->database('vas_refill_voucher', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');

		if ($status) {
			$vas_refill_voucher->where('info', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$vas_refill_voucher->where('DATE(created_date) >=', date('Y-m-d', strtotime($startdate)));
			$vas_refill_voucher->where('DATE(created_date) <=', date('Y-m-d', strtotime($enddate)));
		}
		$vas_refill_voucher->order_by('created_date', 'DESC');
		// $q = $vas_refill_voucher->get('info_balance');
		$q =  $vas_refill_voucher->get_where('info_voucherGames', array('node_id' => '185'));
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}
	//load view tampil data Report BalanceTransfer 185
	public function showDataReportVoucherGamesSdp()
	{
		if ($this->session->userdata('level') == '2') {
			$name = 'showDataReportVoucherGamesSdp';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}


	//load view tampil data Report BalanceTransfer 185
	public function showDataReportVoucherGamesSdl()
	{
		if ($this->session->userdata('level') == '2') {
			$name = 'showDataReportVoucherGamesSdl';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}
}
 
 /* End of file ReportUmb.php */
