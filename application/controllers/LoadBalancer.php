<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class LoadBalancer extends CI_Controller {

	public function showGrafikLoadBalancerEd()
	{
		$name = 'showGrafikLoadBalancerEd';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	} 
	public function showGrafikLoadBalancerEs()
	{
		$name = 'showGrafikLoadBalancerEs';
		$data['file'] = 'content/' . $name;

		$this->load->view('index', $data);
	}  

}

/* End of file LoadBalancer.php */
