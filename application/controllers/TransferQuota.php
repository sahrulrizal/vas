<?php


defined('BASEPATH') or exit('No direct script access allowed');

class TransferQuota extends CI_Controller
{

	function __construct()
	{

		parent::__construct();

		$this->load->library('session');
	}

	public function reportDataTransferQuotaSdp()
	{
		$vas_refill_voucher = $this->load->database('vas_refill_voucher', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');

		if ($status) {
			$vas_refill_voucher->where('info', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$vas_refill_voucher->where('DATE(created_date) >=', date('Y-m-d', strtotime($startdate)));
			$vas_refill_voucher->where('DATE(created_date) <=', date('Y-m-d', strtotime($enddate)));
		}
		$vas_refill_voucher->order_by('created_date', 'DESC');
		// $q = $vas_refill_voucher->get('info_VoucherGames');
		$q =  $vas_refill_voucher->get_where('info_transferQuota', array('node' => '180'));
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}

	// report balance transfer 185
	public function reportDataTransferQuotaSdl()
	{
		$vas_refill_voucher = $this->load->database('vas_refill_voucher', TRUE);

		$status = $this->input->get('status');
		$startdate = $this->input->get('stDate');
		$enddate = $this->input->get('endDate');

		if ($status) {
			$vas_refill_voucher->where('info', $status);
		}

		if ($startdate != "" and $enddate != "") {
			$vas_refill_voucher->where('DATE(created_date) >=', date('Y-m-d', strtotime($startdate)));
			$vas_refill_voucher->where('DATE(created_date) <=', date('Y-m-d', strtotime($enddate)));
		}
		$vas_refill_voucher->order_by('created_date', 'DESC');
		// $q = $vas_refill_voucher->get('info_balance');
		$q =  $vas_refill_voucher->get_where('info_transferQuota', array('node' => '185'));
		if ($q) {
			$result = array('success' => true, 'data' => $q->result());
		} else {
			$result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
		}
		$result['debugq'] = $this->db->last_query();
		echo json_encode($result);
	}
	//load view tampil data Report BalanceTransfer 185
	public function showDataReportTransferQuotaSdp()
	{
		if ($this->session->userdata('level') == '2') {
			$name = 'showDataReportTransferQuotaSdp';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}


	//load view tampil data Report BalanceTransfer 185
	public function showDataReportTransferQuotaSdl()
	{
		if ($this->session->userdata('level') == '2') {
			$name = 'showDataReportTransferQuotaSdl';

			// $data['js'] = $name . '.js';
			$data['file'] = 'content/' . $name;

			$this->load->view('index', $data);
		} else {
			redirect('login');
		}
	}
}
 
 /* End of file ReportUmb.php */
