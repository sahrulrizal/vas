<header>
    <div class="row">
        <div class="col-md-6" style="text-align: left;">
            <b><img
                class="img-responsive"
                style="height: 40px;position: absolute;top: -8px;"
                src="<?= base_url('template/images/telkomcels.png') ?>"></b>
        </div>
        <div class="col-md-6" style="text-align: right;color: #000;">
		   <?php if ($this->session->userdata('username') == 'sts') {?>
				<!-- Hi, Tekomcel (RCM Support01)  -->
				Hi, Tekomcel (<?php echo $this->session->userdata('username');?>) 
		   <?php }elseif ($this->session->userdata('username') == 'tcel') { ?>
				Hi, Tekomcel (<?php echo $this->session->userdata('username') ?>)			   
		   <?php }else{ ?>
			  Hi, (<?php echo $this->session->userdata('username') ?>)			   

		   <?php } ?>
        </div>
    </div>
</header>

 <style>

    .dropdown-submenu {
  position: relative;
}
.dropdown-submenu > .dropdown-menu {
  top: 0;
  left: 100%;
  margin-top: -6px;
  margin-left: -1px;
}
.dropdown-submenu:hover > .dropdown-menu {
  display: block;
}
.dropdown-submenu:hover > a:after {
  border-left-color: #fff;
}
.dropdown-submenu.pull-left {
  float: none;
}
.dropdown-submenu.pull-left > .dropdown-menu {
  left: -100%;
  margin-left: 10px;
}
</style>

<nav
    class="navbar navbar-default navbar-fixed"
    role="navigation"
    id="menu"
    style="z-index: 3;background: #171717;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button
                type="button"
                class="navbar-toggle"
                data-toggle="collapse"
                data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li>
                    <a href="<?= site_url('index.php/Dashboard/index') ?>">
                        <p>DashMon</p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <p>Service Monitoring</p>
                    </a>
                </li>
                <li class="dropdown">
                    <a
                        href="#"
                        class="dropdown-toggle"
                        data-toggle="dropdown"
                        role="button"
                        aria-haspopup="true"
                        aria-expanded="true">
                        <p>Reporting & Statistic
                            <span class="caret"></span></p>
                    </a>
                    <ul class="dropdown-menu" role="menu" id="dropMenu1">
                        <li>
                            <!-- <a href="<?= site_url('index.php/Dashboard/cdr_vouchergame') ?>">Google Play</a> -->
                            <li class="dropdown-submenu" onmouseover="subsubmenu('dropdownmenu2');" onmouseleave="subsubmenuleave('dropdownmenu2');">
                          <a tabindex="-1" href="<?= site_url('index.php/Dashboard/cdr_vouchergame') ?>">Google Play <i class="fa fa-chevron-right"></i></a>
                          <ul class="dropdown-menu" id="dropdownmenu2" style="display: none;">
                                    <li>
                                        <a tabindex="-1" href="<?= site_url('index.php/Dashboard/detail_googleplay') ?>">Detail Google Play
                                        </a>
                                    </li>  
                                                        
                            <!-- <li><a href="#">Lik 2</a></li>
                            <li><a href="#">Link 3</a></li> -->
                          </ul>
                      </li>
                        </li>
                        
                        <li>
                            <a href="<?= site_url('index.php/Dashboard/games') ?>">Voucher Management</a>
                        </li>
                         <li>
                            <a href="<?= site_url('index.php/Dashboard/purchasePendingMobiwin') ?>">Purchase Pending Mobiwin</a>
                        </li>

                        <li>
                            <a href="<?= site_url('index.php/Dashboard/ussd') ?>">Report Ussd</a>

                </li>
               <li>
                                <a href="<?= site_url('index.php/Dashboard/refillVoucher') ?>">RefillVoucher</a>
                  </li>
            
              
                        
                        <li>
                            <a href="<?= site_url('index.php/Dashboard/report_etopup') ?>">Etopup</a>
            </li>
                      <!-- umb node -->
           
                        <li>
                            
                            <li class="dropdown-submenu" onmouseover="subsubmenu('dropdownmenu3');" onmouseleave="subsubmenuleave('dropdownmenu3');">
                                <a tabindex="-1" href="#">Report SMSC <i class="fa fa-chevron-right"></i></a>
                                <ul class="dropdown-menu" id="dropdownmenu3" style="display: none;">
								<li>
									<a href="<?= site_url('index.php/ReportSmsc/showDataReportSmscLima') ?>">SMSC-5</a>
								</li>
								<li>
									<a href="<?= site_url('index.php/ReportSmsc/showDataReportSmscEnam') ?>">SMSC-6</a>
								</li>
								<li>
									<a href="<?= site_url('index.php/ReportSmsc/showDataReportSmscDelapan') ?>">SMSC-8</a>
							</li> 
							<li>
									<a href="<?= site_url('index.php/ReportSmsc/showDataReportSmscSembilan') ?>">SMSC-9</a>
							</li>                        
                                  
                                </ul>
                            </li>
						</li>
						<!-- akhir menu navbar smsc -->
						<!-- awal menu Report Balance Transfer -->
                         <li class="dropdown-submenu" onmouseover="subsubmenu('dropdownmenu4');" onmouseleave="subsubmenuleave('dropdownmenu4');">
                                <a tabindex="-1" href="#"> Report Balance Transfer <i class="fa fa-chevron-right"></i></a>
                                <ul class="dropdown-menu" id="dropdownmenu4" style="display: none;">
								<li>
									<a href="<?= site_url('index.php/ReportBalanceTransfer/showDataReportBalanceTransferSdp') ?>">BalanceTransefer Node-180</a>
								</li>
								<li>
									<a href="<?= site_url('index.php/ReportBalanceTransfer/showDataReportBalanceTransferSdl') ?>">BalanceTransfer Node-185</a>
								</li>                        
                                </ul>
                            </li>
						</li>
						<!-- akhir Menu Report Balance Trasnfer -->
						
						<!-- menu dropdown menu report umb  -->
						<li class="dropdown-submenu" onmouseover="subsubmenu('dropdownmenu5');" onmouseleave="subsubmenuleave('dropdownmenu5');">
                                <a tabindex="-1" href="#"> Report UMB  <i class="fa fa-chevron-right"></i></a>
                                <ul class="dropdown-menu" id="dropdownmenu5" style="display: none;">
								<li>
									<a href="<?= site_url('index.php/ReportUmb/showDataUmbNodeTps') ?>">UMB Node - 71</a>
								</li>
								<li>
									<a href="<?= site_url('index.php/ReportUmb/showDataUmbNodeTpd') ?>">UMB Node 72</a>
								</li>
								<li>
									<a href="<?= site_url('index.php/ReportUmb/showDataUmbNodeDp') ?>">UMB Node 80</a>
								</li>                       
                                </ul>
                            </li>
						</li>
						
						<!-- akhir menu dropdown menu umb -->

						<!-- menu dropdown menu report TransferQuota  -->
						<li class="dropdown-submenu" onmouseover="subsubmenu('dropdownmenu6');" onmouseleave="subsubmenuleave('dropdownmenu6');">
                                <a tabindex="-1" href="#"> Report TransferQuota  <i class="fa fa-chevron-right"></i></a>
                                <ul class="dropdown-menu" id="dropdownmenu6" style="display: none;">
									<li>
										<a href="<?= site_url('index.php/TransferQuota/showDataReportTransferQuotaSdp') ?>">TransferQuota Node - 180</a>
									</li>
									<li>
										<a href="<?= site_url('index.php/TransferQuota/showDataReportTransferQuotaSdl') ?>">TransferQuota Node - 185</a>
									</li>                       
                                </ul>
                            </li>
						</li>
						
						<!-- akhir menu dropdown menu report TransferQuota -->

						<!-- menu dropdown menu report GamesMobiwin  -->
						<li class="dropdown-submenu" onmouseover="subsubmenu('dropdownmenu7');" onmouseleave="subsubmenuleave('dropdownmenu7');">
                                <a tabindex="-1" href="#"> Report GamesMobiwin  <i class="fa fa-chevron-right"></i></a>
                                <ul class="dropdown-menu" id="dropdownmenu7" style="display: none;">
									<li>
									   <a href="<?= site_url('index.php/ReportGamesMobiwin/showDataReportGamesMobiwinSdp') ?>">GamesMobiwin Node - 180</a>
									</li>
									<li>
										<a href="<?= site_url('index.php/ReportGamesMobiwin/showDataReportGamesMobiwinSdl') ?>">GamesMobiwin Node - 185</a>
									</li>                    
                                </ul>
                            </li>
						</li>
						
						<!-- akhir menu dropdown menu report GamesMobiwin -->

						<!-- menu dropdown menu report GamesTL  -->
						<li class="dropdown-submenu" onmouseover="subsubmenu('dropdownmenu8');" onmouseleave="subsubmenuleave('dropdownmenu8');">
                                <a tabindex="-1" href="#"> Report GamesTL  <i class="fa fa-chevron-right"></i></a>
                                <ul class="dropdown-menu" id="dropdownmenu8" style="display: none;">
									<li>
										<a href="<?= site_url('index.php/GamesTl/showDataReportGamesTlSdp') ?>">GamesTL Node - 180</a>
									</li>
									<li>
										<a href="<?= site_url('index.php/GamesTl/showDataReportGamesTlSdl') ?>">GamesTL Node -185</a>
									</li>                    
                                </ul>
                            </li>
						</li>
						
						<!-- akhir menu dropdown menu report GamesTL -->


						<!-- menu dropdown menu report VoucherGames  -->
						<li class="dropdown-submenu" onmouseover="subsubmenu('dropdownmenu9');" onmouseleave="subsubmenuleave('dropdownmenu9');">
                                <a tabindex="-1" href="#"> Report VoucherGames  <i class="fa fa-chevron-right"></i></a>
                                <ul class="dropdown-menu" id="dropdownmenu9" style="display: none;">
								<li>
								  <a href="<?= site_url('index.php/VoucherGames/showDataReportVoucherGamesSdp') ?>">VoucherGames Node - 180</a>
						     	</li>
								<li>
									<a href="<?= site_url('index.php/VoucherGames/showDataReportVoucherGamesSdl') ?>">VoucherGames Node - 185</a>
								</li>                   
                                </ul>
                            </li>
						</li>
						
						<!-- akhir menu dropdown menu report VoucherGames -->

							<!-- menu dropdown menu report ExtendMe  -->
							<li class="dropdown-submenu" onmouseover="subsubmenu('dropdownmenu10');" onmouseleave="subsubmenuleave('dropdownmenu10');">
                                <a tabindex="-1" href="#"> Report ExtendMe  <i class="fa fa-chevron-right"></i></a>
                                <ul class="dropdown-menu" id="dropdownmenu10" style="display: none;">
									<li>
										<a href="<?= site_url('index.php/ExtendMe/showDataReportExtendMeSdp') ?>">ExtendMe Node - 180</a>
									</li>
									<li>
										<a href="<?= site_url('index.php/ExtendMe/showDataReportExtendMeSdl') ?>">ExtendMe Node - 185</a>
									</li>                  
                                </ul>
                            </li>
						</li>
						
						<!-- akhir menu dropdown menu report ExtendMe -->

						<!-- menu dropdown menu report ExtendMe  -->
						<li class="dropdown-submenu" onmouseover="subsubmenu('dropdownmenu11');" onmouseleave="subsubmenuleave('dropdownmenu11');">
                                <a tabindex="-1" href="#"> Report Evoucher  <i class="fa fa-chevron-right"></i></a>
                                <ul class="dropdown-menu" id="dropdownmenu11" style="display: none;">
									<li>
										<a href="<?= site_url('index.php/Evoucher/showDataReportEvoucherSdp') ?>">Evoucher Node - 180</a>
									</li>
									<li>
										<a href="<?= site_url('index.php/Evoucher/showDataReportEvoucherSdl') ?>">Evoucher Node - 185</a>
									</li>                  
                                </ul>
                            </li>
						</li>


						<li class="dropdown-submenu" onmouseover="subsubmenu('dropdownmenu12');" onmouseleave="subsubmenuleave('dropdownmenu12');">
                                <a tabindex="-1" href="#"> Report USSD  <i class="fa fa-chevron-right"></i></a>
                                <ul class="dropdown-menu" id="dropdownmenu12" style="display: none;">
									<li>
										<a href="<?= site_url('index.php/UssdReportController/showDataReportUssdSdd') ?>">USSD Node - 182</a>
									</li>
									<li>
										<a href="<?= site_url('index.php/UssdReportController/showDataReportUssdSdt') ?>">USSD Node - 187</a>
									</li>                  
                                </ul>
                            </li>
						</li>

						<li class="dropdown-submenu" onmouseover="subsubmenu('dropdownmenu13');" onmouseleave="subsubmenuleave('dropdownmenu13');">
                                <a tabindex="-1" href="#"> Report RBT  <i class="fa fa-chevron-right"></i></a>
                                <ul class="dropdown-menu" id="dropdownmenu13" style="display: none;">
									<li>
										<a href="<?= site_url('index.php/ReportRbt/reportRbt') ?>">RBT</a>
									</li>
									                
                                </ul>
                            </li>
						</li>
						
						<!-- akhir menu dropdown menu report ExtendMe -->


                
            
                    </ul>
                </li>

                <li>
                    <a href="#">
                        <p>Documentation</p>
                    </a>
                </li>
<li>
                    <a href="#">
                        <p>Troubleshooting</p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <p>Administrative Tools</p>
                    </a>
                </li>

                <li class="dropdown">
                    <a
                            href="#"
                            class="dropdown-toggle"
                            data-toggle="dropdown"
                            role="button"
                            aria-haspopup="true"
                            aria-expanded="true">
                            <p>SMS Monitoring
                                <span class="caret"></span></p>
                        </a>
                        <ul class="dropdown-menu" role="menu" id="dropMenuA2P">
                            <li>
                                <a href="<?= site_url('index.php/Dashboard/smsc') ?>">SMSC</a>
                            </li>
                            <li>
                                <a href="<?= site_url('index.php/Dashboard/a2p') ?>">A2P</a>
							</li>
							<li>
                                <a href="<?= site_url('index.php/Dashboard/limaEn') ?>">5N</a>
                            </li>
                             <li>
                                <a href="<?= site_url('index.php/Dashboard/ussd') ?>">USSD</a>
                </li>
                            <li>
                                <a href="<?= site_url('index.php/Dashboard/refillVoucher') ?>">RefillVoucher </a>
                            </li>
                            <li>
                                <a href="<?= site_url('index.php/Dashboard/smscdet') ?>">SMSC Daily </a>
                            </li>
                        </ul>
                </li>

                               
          
        </ul>
      </li>
                <li style="display:none">
                    <a href="<?= site_url('index.php/Dashboard/games') ?>">
                        <p>Games</p>
                    </a>
                </li>

              
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <!-- <li class="dropdown"> <a href="#" class="dropdown-toggle"
                data-toggle="dropdown"> <p> Superuser <b class="caret"></b> </p> </a> <ul
                class="dropdown-menu"> <li><a href="#">Action</a></li> <li><a href="#">Another
                action</a></li> <li><a href="#">Something</a></li> <li><a href="#">Another
                action</a></li> <li><a href="#">Something</a></li> <li clas s="divider"></li>
                <li><a href="#">Separated link</a></li> </ul> </li> -->

                <li>
                    <a href="<?= base_url() ?>index.php/Login/Logout">
                        <p>Log out</p>
                    </a>
                </li>
                <li class="separator hidden-lg"></li>
            </ul>
        </div>
    </div>
</nav>
<script>




function subsubmenu(id){
     var element = document.getElementById(id);
     element.removeAttribute("style");
}

function subsubmenuleave(id){
     var element = document.getElementById(id);
     document.getElementById(id).style.display = "none";
}

</script>
