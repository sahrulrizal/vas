 <footer class="footer" style="background:#000;">
        <div class="container-fluid">
            <nav class="pull-left">
                <ul>
                    <li>
                        <a href="#">
                            <p>DashMon</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <p>Service Monitoring</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <p>Reporting & Statistic</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <p>Documentation</p>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <p>Troubleshooting</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <p>Administrative Tools</p>
                        </a>
                    </li>
                </ul>
            </nav>
            <p class="copyright pull-right">
                &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.telkomcel.tl">Telkomcel</a>, made with love
            </p>
        </div>
    </footer>
   