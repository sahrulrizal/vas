<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="../images/icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Telkomcel Dashboard</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="<?=base_url()?>template/assets/css/bootstrap.min.css" rel="stylesheet" />


    <!-- Animation library for notifications   -->
    <link href="<?=base_url()?>template/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="<?=base_url()?>template/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>

    <link href="<?=base_url()?>template/assets/css/icons.css" rel="stylesheet" media="all" type="text/css" />

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?=base_url()?>template/assets/css/demo.css" rel="stylesheet" />
    
    <link rel="stylesheet" media="all" type="text/css" href="<?=base_url()?>template/assets/css/toastr.css">
    <link rel="stylesheet" media="all" type="text/css" href="<?=base_url()?>template/js/sweetalert2.css">


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?=base_url()?>template/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link href="https://cdn.anychart.com/releases/v8/css/anychart-ui.min.css?hcode=a0c21fc77e1449cc86299c5faa067dc4" rel="stylesheet" type="text/css">
    
</head>

<style type="text/css">
body{
    background: rgb(249, 244, 218);
}
    .loader {
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid red; /* Blue */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        z-index: 9999999 !important;
        animation: spin 2s linear infinite;
    }

.ex-page-content .text-error {
  color: #252932;
  font-size: 98px;
  font-weight: 700;
  line-height: 150px; }
  .ex-page-content .text-error i {
    font-size: 78px;
    padding: 0 10px; }

    header{
        padding: 15px 48px;
        background: #FFF;  /* fallback for old browsers */
/*background: -webkit-linear-gradient(to right, #ffffff,#fdf5f5, #000000);*/  /* Chrome 10-25, Safari 5.1-6 */
/*background: linear-gradient(to right, #ffffff,#fdf5f5, #000000); *//* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

        color: #FFF;
        font-size: 18px;
    }

    .navbar-default .navbar-nav > li > a:not(.btn) {
    color: #fff;
}

</style>



