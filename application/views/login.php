
<link rel="stylesheet" href="<?php echo base_url() ?>template/js/sweetalert2.min.css">

<style type="text/css">


.progress-bar {
    color: #333;
} 

/*
Reference:
http://www.bootstrapzen.com/item/135/simple-login-form-logo/
*/

* {
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
    outline: none;
}

    .form-control {
      position: relative;
      font-size: 16px;
      height: auto;
      padding: 10px;
        @include box-sizing(border-box);

        &:focus {
          z-index: 2;
        }
    }

body {
    background: url(http://i.imgur.com/GHr12sH.jpg) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}

.login-form {
    margin-top: 60px;
}

form[role=login] {
    color: #5d5d5d;
    background: #f2f2f2;
    padding: 26px;
    border-radius: 10px;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
}
    form[role=login] img {
        display: block;
        margin: 0 auto;
        margin-bottom: 35px;
    }
    form[role=login] input,
    form[role=login] button {
        font-size: 18px;
        margin: 16px 0;
    }
    form[role=login] > div {
        text-align: center;
    }
    
.form-links {
    text-align: center;
    margin-top: 1em;
    margin-bottom: 50px;
}
    .form-links a {
        color: #fff;
    }

#center {
  margin: auto;
  width: 30%;
  padding: 10px;
}
</style>

<body>

    <div class="wrapper">
        <?php //$this->load->view('incl/navbar'); ?>

        <div class="main-panel" style="width: 100%;float: none;">

            <div class="content">

                <div class="konten">
                    <div class="container" id="center">
  
                      <div class="row" id="pwd-container" style="width: 320px;margin-top: 100px">
                        <div class="col-md-4"></div>
                        
                          <div class="col-md-4">
                            <!-- <form method="post" action="javascript:void(0" role="login" id="formid"> -->
														<form action="javascript:void(0)" id="frmLogin" role="login" method="POST">
                              <img src="<?= base_url('template/images/telkomcels.png') ?>" class="img-responsive" alt="" style="height: 60px;top: 10px;"/>
                              <input type="text" name="username" id="username" placeholder="Username" class="form-control input-lg" required />
                              
                              <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" required />
                              
                              <button type="submit" id="submit" name="login" class="btn btn-primary" style="width: 90px;height: 50px;background-color: #0099ff;color: #f1f1f1">Login</button>
                            
                            </form>  
                          </div>
                      </div>  
                    </div>
                </div>

            </div>

            <?php //$this->load->view('incl/footer'); ?>
        </div>
    </div>


</body>

<!--   Core JS Files   -->
<script src="<?= base_url() ?>template/assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>template/js/sweetalert2.all.min.js"></script>

<!-- z -->




<script type="text/javascript">
    // Ajax post
    $(document).ready(function() {
        
        $("#submit").click(function() {
            var username = $("#username").val();
            var password = $("#password").val();
            // Returns error message when submitted without req fields.
            if (username == '' || password == '') {
                jQuery("div#err_msg").show();
                jQuery("div#msg").html("All fields are required");
								
            } else {
                // AJAX Code To Submit Form.
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('index.php/Login/check_login'); ?>",
                    data: {
                        username: username,
                        password: password
                    },
                    dataType: 'json',
                    cache: false,
                    success: function(result) {
                        if (result.status) {
                            localStorage.setItem('id_artist', result.session.id_artist);
                             Swal.fire(
                              'Success!',
                              'Login Sucess!',
                              'success'
                            )
                            setTimeout(() => {
                                window.location.replace(result.url);
                            }, 200);
								// console.log(result);
                        } else {
                             Swal.fire(
                              'Login Failed!',
                              'Check Username and Password!',
                              'error'
                            );
                        }
                        
                    }
                });
            }
        });
    });
    </script>

</html>
