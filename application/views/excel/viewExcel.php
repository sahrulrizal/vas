<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Latihan Import Data!</title>
  </head>
  <body>
    
	 <!-- <div id="proses">
        <img src="loading.gif" alt="">
	</div> -->
	
	<section class="widget">
            <header>
                <h4>
                    Google Play Voucher List
                    <small>

                    </small>
                </h4>
                <div class="widget-controls">
                    <a title="Options" href="#"><i class="glyphicon glyphicon-cog"></i></a>
                    <a data-widgster="expand" title="Expand" href="#"><i class="glyphicon glyphicon-chevron-up"></i></a>
                    <a data-widgster="collapse" title="Collapse" href="#"><i
                            class="glyphicon glyphicon-chevron-down"></i></a>
                    <a data-widgster="close" title="Close" href="#"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </header>
            <div class="body">
                <div class="button-group">
                    <button class="btn-primary btn" onclick="openModal('add')"><i class="glyphicon glyphicon-plus"></i>
                        Add Voucher</button>
                    <button class="btn-default btn" data-toggle="modal" data-target="#modalFormExcel"><i
                            class="glyphicon glyphicon-upload"></i> Upload Voucher (Excel)</button>
                </div>

               
            </div>
        </section>

    
    <div class="loader-wrap hiding hide">
        <i class="fa fa-circle-o-notch fa-spin"></i>
    </div>
    </div>

    
    <div class="modal fade" id="modalFormExcel" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload GPV
                    </h4>
                </div>
                <form id="submit" enctype="multipart/form-data" method="POST">
                    <div class="modal-body">
                        <div class="well">
                            <i class="fa-info-circle fa"></i>
                            Information
                            <p>
                                Please download template below before uploading<br>
                                <a href="http://localhost/apigoogleplayy/excel/tamplate.xlsx"
                                    class="btn btn-info">Download Template</a>
                            </p>

                            <!-- <div>
                                <img src="loading.gif" id="proses" style="display: none;" alt="">
                                <h1>loading</h1>
                            </div> -->

                        </div>
                        <div class="form-group">
                            <div class="form-line">

                                <label for="inputExcel" class="control-label">Excel File</label>
                                <input type="file" name="file" id="file" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit" id="simpann">
                            <i class="fa fa-save"></i>
                            Save</button>
                        <button class="btn btn-danger" data-dismiss="modal" type="reset">
                            <i class="fa fa-times"></i>
                            Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
   

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

	
	<script>
        //inport excel

        $(document).ready(function () {
            // alert("hello wordl");



            $('#submit').submit(function (e) {

                e.preventDefault();
                $.ajax({
                    url: api_url + 'Data/importBack',
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    cache: false,
                    async: false,
                    dataType: "JSON",
                    beforeSend: function () {

                        // alert("hello gaes");

                        // $("#proses")
                        //     .show()
                        //     .html("<img src='loading.gif' height='100px' width='100px'>");
                        $("#proses").show();


                    },
                    success: function (data) {
                        // $("#proses").show(2000).html("<img src='loading.gif' height='100'>");

                        if (data.success) {

                            let timerInterval
                            Swal.fire({
                                title: '',
                                html: 'Loading..',
                                timer: 3000,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                        // Swal.getContent().querySelector('strong')
                                        //     .textContent = Swal.getTimerLeft()
                                    }, 100)
                                },
                                onClose: () => {
                                    Swal.fire(
                                        data.title,
                                        'Data telah tersimpan',
                                        'success'
                                    )
                                }
                            }).then((result) => {
                                if (
                                    result.dismiss === Swal.DismissReason.timer
                                ) {

                                }
                            });
                            getData();
                            // $("#proses").hide();
                        } else {
                            Swal.fire({
                                position: 'center',
                                type: 'error',
                                title: data.title,
                                customClass: {
                                    popup: 'animated tada'
                                },
                                animation: false,
                                html: '<div style="font-size:16px;">' + data.msg + '</div>',
                                showConfirmButton: false,
                                timer: 4000
                            })
                        }
                        $('input[name=file]').val('');
                        $('#modalFormExcel').modal('hide');
                    }
                });
            });

        });
    </script>



  </body>
</html>
