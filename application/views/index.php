<?php $this->load->view('incl/head'); ?>

<body>

	<div class="wrapper">
		<?php $this->load->view('incl/navbar'); ?>

		<div class="main-panel" style="width: 100%;float: none;">

			<div class="content">

				<div class="konten">
					<?php $this->load->view($file); ?>
				</div>

			</div>

			<?php $this->load->view('incl/footer'); ?>
		</div>
	</div>


</body>

<!--   Core JS Files   -->
<script src="<?= base_url() ?>template/assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>template/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>template/js/jquery.sticky-kit.min.js" type="text/javascript"></script>

<script src="https://cdn.anychart.com/releases/v8/js/anychart-core.min.js?hcode=a0c21fc77e1449cc86299c5faa067dc4">
</script>
<script src="https://cdn.anychart.com/releases/v8/js/anychart-cartesian-3d.min.js?hcode=a0c21fc77e1449cc86299c5faa067dc4">
</script>
<script src="https://cdn.anychart.com/releases/v8/js/anychart-exports.min.js?hcode=a0c21fc77e1449cc86299c5faa067dc4">
</script>
<script src="https://cdn.anychart.com/releases/v8/js/anychart-ui.min.js?hcode=a0c21fc77e1449cc86299c5faa067dc4">
</script>
<script src="<?= base_url() ?>template/js/sweetalert2.all.min 2.js" type="text/javascript"></script>


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<!--  Notifications Plugin    -->
<script src="<?= base_url() ?>template/assets/js/bootstrap-notify.js"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="<?= base_url() ?>template/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="<?= base_url() ?>template/assets/js/demo.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/js/toastr.min.js"></script>

<!--  Umum    -->
<script src="<?= base_url() ?>template/js/umum.js?v=<?= rand(1000, 100000); ?>"></script>

<!-- External JS -->
<!-- <?php if ($js != null) { ?> -->

<script src="<?= base_url() ?>template/js/content/<?= $js ?>?v=<?= rand(1000, 100000); ?>"></script>

<!-- <?php  } ?> -->

<script type="text/javascript">
	var successF = 0,
		failedF = 0;
  

	$(document).ready(function() {

		setInterval(function() {
			chart();
			// Swal.fire({
				// position: 'top-end',
				// icon: 'success',
				// title: 'Succcess Reload Dashboard',
				// showConfirmButton: false,
				// timer: 200000
				// });
			// alert('hello guys')
			location.reload()
            }, 300000);

		$.notify({
			icon: 'pe-7s-info',
			message: "Welcome to <b>Telkomcel Dashboard</b> - for a better solution management dashboard."

		}, {
			type: 'info',
			timer: 1000
		});

		$.ajax({
			url: "http://localhost/dashetopup/getData.php?params=main",
			method: 'GET',
			dataType: 'json',
			success: function(dt) {
				if (dt.success) {
					console.log('data etopup', dt.data);
				}
			}
		});

	});

	$("#menu").stick_in_parent();

	// grafik


	var apiLocal = 'http://localhost/apigoogleplayy/index.php/',
		apiServer = 'http://150.242.111.235/apigoogleplay/index.php/',
		api_url = 'http://150.242.111.235/vas/index.php/Dashboard'

	// awal
	nama = ['SMSC - 5', 'SMSC - 6', 'SMSC - 8', 'SMSC - 9', 'USSD Node - 182', 'USSD Node - 187', 'UMB Node - 71', 'UMB Node - 72', 'UMB Node - 80', 'VAS - RefilVoucher - 185',  'Etopup', 'SMSC-Daily', 'TransferQuota - 180', 'TransferQuota - 185', 'GamesMobiwin-180', 'GamesMobiwin - 185', 'GamesTL-180', 'GamesTL-185', 'Voucher-Games-180', 'Voucher-Games-185', 'Extend-Me-180', 'Extend-Me-185', 'Balance Tranfer 180', 'Balance Tranfer 185', 'Simcard Predefine','Evoucher-180','Evoucher-185','Account-Purchase-180','Account-Purchase-185','Load-Balancer-68','Load-Balancer-69','adjusmentDA-180','adjusmentDA-185','RBT','AR - Data Package', 'AR - Voice Package', 'Provesioning Simcard', 'Purchase Data Package', 'Purchase SMS Package', 'Purchase-Redem-Point', 'Topup-Voucher', 'Video-MAX', 'Game', 'Extend-Me', 'Call-Me', 'Balance-Transfer', 'ISB'];
	data = ['smsc-5', 'smsc-6', 'smsc-8', 'smsc-9', 'ussd-node-182', 'ussd-node-187', 'umb-node-71', 'umb-node-72', 'umb-node-80', 'VAS-RefilVoucher-185', 'etopup', 'smscdet', 'transfer-quota-180', 'transfer-quota-185', 'Games-mobiwin-180', 'Games-mobiwin-185', 'gamestl-180', 'gamestl-185', 'voucher-games-180', 'voucher-games-185', 'Extend-me-180', 'Extend-me-185', 'Balance-tranfer-180', 'Balance-tranfer-185', 'Simcard-Predefine','Evoucher-180','Evoucher-185','Account-Purchase-180','Account-Purchase-185','Load-Balancer-68','Load-Balancer-69','adjusmentDA-180','adjusmentDA-185','RBT','diamet-system', 'ar-rbt', 'ar-data-package', 'ar-voice-packet', 'provesioning-simcard', 'purcase-data-pacakage', 'sms-package', 'redem-point', 'topup-voucher', 'video-max', 'game', 'extend-me', 'call-me'];
	link = ['smsc-5', 'smsc-6', 'smsc-8', 'smsc-9', 'ussd-node-182', 'ussd-node-187', 'umb-node-71', 'umb-node-72', 'umb-node-80', 'VAS-RefilVoucher-185', 'etopup', 'smscdet', 'transfer-quota-180', 'transfer-quota-185', 'Games-mobiwin-180', 'Games-mobiwin-185', 'gamestl-180', 'gamestl-185', 'voucher-games-180', 'voucher-games-185', 'Extend-me-180', 'Extend-me-185', 'Balance-tranfer-180', 'Balance-tranfer-185', 'Simcard-Predefine','Evoucher-180','Evoucher-185','Account-Purchase-180','Account-Purchase-185','Load-Balancer-68','Load-Balancer-69','adjusmentDA-180','adjusmentDA-185','RBT','diamet-system', 'ar-rbt', 'ar-data-package', 'ar-voice-packet', 'provesioning-simcard', 'purcase-data-pacakage', 'sms-package', 'redem-point', 'topup-voucher', 'video-max', 'game', 'extend-me', 'call-me'];
	// akhir
	console.log(nama.length);
	console.log(data.length);
	console.log(data.length);
	// umb
	hasilSukses_UmbTpd = <?php echo $hasilSukses_UmbTpd ?>;
	hasilError_UmbTpd = <?php echo $hasilError_UmbTpd ?>;

	hasilSukses_UmbTps = <?php echo $hasilSukses_UmbTps ?>;
	hasilError_UmbTps = <?php echo $hasilError_UmbTps ?>;


	hasilSukses_UmbDp = <?php echo $hasilSukses_UmbDp ?>;
	hasilError_UmbDp = <?php echo $hasilError_UmbDp ?>;

	// akhir umb


	hasilSukses_balance = <?php echo $hasilSukses_balance ?>;
	hasilError_balance = <?php echo $hasilError_balance ?>;




	hasilSukses_revil = <?php echo $hasilSukses_revil ?>;
	hasilError_revil = <?php echo $hasilError_revil ?>;

	successnode_182 = <?php echo $hasilSukses; ?>;
	errornode_182 = <?php echo $hasilError; ?>;

	hasilSuksesSdt = <?php echo $hasilSuksesSdt; ?>;
	hasilErrorSdt = <?php echo $hasilErrorSdt; ?>;

	successSmsc = <?php echo $successSmsc; ?>;
	failedSmsc = <?php echo $failedSmsc; ?>;

	hasilSukses_transferQuotaSdp = <?php echo $hasilSukses_transferQuotaSdp; ?>;
	hasilError_transferQuotaSdp = <?php echo $hasilError_transferQuotaSdp; ?>;

	hasilSukses_transferQuotaSdl = <?php echo $hasilSukses_transferQuotaSdl; ?>;
	hasilError_transferQuotaSdl = <?php echo $hasilError_transferQuotaSdl; ?>;

	hasilSukses_gamesMobiwinSdp = <?php echo $hasilSukses_gamesMobiwinSdp; ?>;
	hasilError_gamesMobiwinSdp = <?php echo $hasilError_gamesMobiwinSdp; ?>;

	hasilSukses_gamesMobiwinSdl = <?php echo $hasilSukses_gamesMobiwinSdl; ?>;
	hasilError_gamesMobiwinSdl = <?php echo $hasilError_gamesMobiwinSdl; ?>;

	hasilSukses_gamesTlSdp = <?php echo $hasilSukses_gamesTlSdp; ?>;
	hasilError_gamesTlSdp = <?php echo $hasilError_gamesTlSdp; ?>;

	hasilSukses_gamesTlSdl = <?php echo $hasilSukses_gamesTlSdl; ?>;
	hasilError_gamesTlSdl = <?php echo $hasilError_gamesTlSdl; ?>;


	hasilSE = <?php echo $hasilSE; ?>;
	hasilEE = <?php echo $hasilEE; ?>;

	// voucher games 180 and 185
	hasilSukses_voucherGamesSdp = <?php echo $hasilSukses_voucherGamesSdp; ?>;
	hasilError_voucherGamesSdp = <?php echo $hasilError_voucherGamesSdp; ?>;

	hasilSukses_voucherGamesSdl = <?php echo $hasilSukses_voucherGamesSdl; ?>;
	hasilError_voucherGamesSdl = <?php echo $hasilError_voucherGamesSdl; ?>;
	// akhir vouchergames 185 and 185

	// extendMe 180 and 185
	hasilSukses_extendMeSdp = <?php echo $hasilSukses_extendMeSdp; ?>;
	hasilError_extendMeSdp = <?php echo $hasilError_extendMeSdp; ?>;

	hasilSukses_extendMeSdl = <?php echo $hasilSukses_extendMeSdl; ?>;
	hasilError_extendMeSdl = <?php echo $hasilError_extendMeSdl; ?>;
	// akhir extendMe 180 and 185

	// extendMe 180 and 185
	hasilSukses_balanceTransferSdp = <?php echo $hasilSukses_balanceTransferSdp; ?>;
	hasilError_balanceTransferSdp = <?php echo $hasilError_balanceTransferSdp; ?>;

	hasilSukses_balanceTransferSdl = <?php echo $hasilSukses_balanceTransferSdl; ?>;
	hasilError_balanceTransferSdl = <?php echo $hasilError_balanceTransferSdl; ?>;
	// akhir extendMe 180 and 185


	// query smsc 5 6 8
	hasilSukses_SmscLima = <?php echo $hasilSukses_SmscLima; ?>;
	hasilError_SmscLima = <?php echo $hasilError_SmscLima; ?>;



	hasilSukses_SmscEnam = <?php echo $hasilSukses_SmscEnam; ?>;
	hasilError_SmscEnam = <?php echo $hasilError_SmscEnam; ?>;

	hasilSukses_SmscDelapan = <?php echo $hasilSukses_SmscDelapan; ?>;
	hasilError_SmscDelapan = <?php echo $hasilError_SmscDelapan; ?>;
	// hasil akhir sms 5 6 8

	// simcard prefidene
	hasilSukses_SimcardPredefine = <?php echo $hasilSukses_SimcardPredefine; ?>;
	hasilError_SimcardPredefine = <?php echo $hasilError_SimcardPredefine; ?>;

	// akhir simcard prefidene

	// evoucher

   hasilSukses_evoucherSdp = <?php echo $hasilSukses_evoucherSdp; ?>;
	hasilError_evoucherSdp = <?php echo $hasilError_evoucherSdp; ?>;

	hasilSukses_evoucherSdl = <?php echo $hasilSukses_evoucherSdl; ?>;
	hasilError_evoucherSdl = <?php echo $hasilError_evoucherSdl; ?>;

	

	// smsc 9
	hasilSukses_SmscSembilan = <?php echo $hasilSukses_SmscSembilan?>;
	hasilError_SmscSembilan = <?php echo $hasilError_SmscSembilan;?>;
	// akhir smsc 9

	// account purchase


	// akhir account purchase
	// 180
	hasilSuksesAccountPurchaseSdp = <?php echo $hasilSuksesAccountPurchaseSdp; ?>;
	hasilErrorAccountPurchaseSdp  = <?php echo $hasilErrorAccountPurchaseSdp; ?>;
	//185
	hasilSuksesAccountPurchaseSdl = <?php echo $hasilSuksesAccountPurchaseSdl; ?>;
	hasilErrorAccountPurchaseSdl = <?php echo $hasilErrorAccountPurchaseSdl; ?>;

	// loadBalancer
   
	hasilSuksesLoadBalancerEs = <?php echo $hasilSuksesLoadBalancerEs; ?>;
	hasilErrorLoadBalancerEs = <?php echo $hasilErrorLoadBalancerEs; ?>;
	hasilSuksesLoadBalancerEd = <?php echo $hasilSuksesLoadBalancerEd; ?>;
	hasilErrorLoadBalancerEd = <?php echo $hasilErrorLoadBalancerEd; ?>;
	
	// akhir loadbalancer


	// adjust
	hasilSuksesAdjustSdp  = <?php echo $hasilSuksesAdjustSdp;?>;
	hasilErrorAdjustSdp = <?php echo $hasilErrorAdjustSdp;?>;

	hasilSuksesAdjustSdl  = <?php echo $hasilSuksesAdjustSdl;?>;
	hasilErrorAdjustSdl = <?php echo $hasilErrorAdjustSdl;?>;

	// akhir adjust
	 

	success_data = [hasilSukses_SmscLima, hasilSukses_SmscEnam, hasilSukses_SmscDelapan, hasilSukses_SmscSembilan, successnode_182, hasilSuksesSdt, hasilSukses_UmbTps, hasilSukses_UmbTpd, hasilSukses_UmbDp, hasilSukses_revil ,hasilSE, successSmsc, hasilSukses_transferQuotaSdp, hasilSukses_transferQuotaSdl, hasilSukses_gamesMobiwinSdp, hasilSukses_gamesMobiwinSdl, hasilSukses_gamesTlSdp, hasilSukses_gamesTlSdl, hasilSukses_voucherGamesSdp, hasilSukses_voucherGamesSdl, hasilSukses_extendMeSdp, hasilSukses_extendMeSdl, hasilSukses_balanceTransferSdp, hasilSukses_balanceTransferSdl, hasilSukses_SimcardPredefine,hasilSukses_evoucherSdp,hasilSukses_evoucherSdl,hasilSuksesAccountPurchaseSdp,hasilSuksesAccountPurchaseSdl,hasilSuksesLoadBalancerEd,hasilSuksesLoadBalancerEs,hasilSuksesAdjustSdp,hasilSuksesAdjustSdl,100,1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,];
	error_data = [hasilError_SmscLima, hasilError_SmscEnam, hasilError_SmscDelapan, hasilError_SmscSembilan, errornode_182, hasilErrorSdt, hasilError_UmbTps, hasilError_UmbTpd, hasilError_UmbDp, hasilError_revil, hasilEE, failedSmsc, hasilError_transferQuotaSdp, hasilError_transferQuotaSdl, hasilError_gamesMobiwinSdp, hasilError_gamesMobiwinSdl, hasilError_gamesTlSdp, hasilError_gamesTlSdl, hasilError_voucherGamesSdp, hasilError_voucherGamesSdl, hasilError_extendMeSdp, hasilError_extendMeSdl, hasilError_balanceTransferSdp, hasilError_balanceTransferSdl, hasilError_SimcardPredefine,hasilError_evoucherSdp,hasilError_evoucherSdl,hasilErrorAccountPurchaseSdp,hasilErrorAccountPurchaseSdl,hasilErrorLoadBalancerEs,hasilErrorLoadBalancerEd,hasilErrorAdjustSdp,hasilErrorAdjustSdl,0,1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,];


	console.log(success_data.length);
	console.log(error_data.length);



	for (var i = 0; i < data.length; i++) {
		if (data[i] == 'project-list') {
			// $('#charts').append('<a href="'+api_url+'/'+link[i]+'" target="_blank"><div id="'+data[i]+'" class="chart" > <b>'+nama[i]+'</b> </div></a>');
			$('#charts').append('<a href="http://localhost/vas/index.php/Dashboard/detail" target="_blank"><div id="' + data[i] + '" class="chart" > <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'ussd-node-182') {
			// alert("hello");
			// window.location.href = "http://localhost/vas/index.php/Dashboard/ussd";
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/ussd"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'VAS-RefilVoucher-185') {
			// alert("hello");
			// window.location.href = "http://localhost/vas/index.php/Dashboard/ussd";
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/refillVoucher"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'VAS-RefilVoucher-180') {
			// alert("hello");
			// window.location.href = "http://localhost/vas/index.php/Dashboard/ussd";
			$('#charts').append('<a href="http://150.242.111.235/a2p-final/BalanceTransfer.php"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'umb-node-71') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/showgrafiknodeTps"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');

		} else if (data[i] == 'umb-node-72') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/showgrafiknodeTpd"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');

		} else if (data[i] == 'umb-node-80') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/showgrafiknodeDp"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');

		} else if (data[i] == 'transfer-quota-180') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/TransferQuotaSdp"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'transfer-quota-185') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/TransferQuotaSdl"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'Games-mobiwin-180') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/gamesMobiwinSdp"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'Games-mobiwin-185') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/gamesMobiwinSdl"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'gamestl-180') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/gamesTlSdp"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'gamestl-185') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/gamesTlSdl"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'voucher-games-180') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/voucherGamesSdp"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'voucher-games-185') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/voucherGamesSdl"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'Extend-me-180') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/extendmeSdp"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'Extend-me-185') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/extendmeSdl"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'Balance-tranfer-180') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/BalanceTransferSdp"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'Balance-tranfer-185') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/BalanceTransferSdl"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'smsc-5') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/smscLima"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'smsc-6') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/smscEnam"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'smsc-8') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/smscDelapan"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'smsc-9') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/smscSembilan"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else if (data[i] == 'Simcard-Predefine') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/simcardprefidene"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		}else if (data[i] == 'Evoucher-180') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Evoucher/showDataGrafikEvoucherSdp"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		}else if (data[i] == 'Evoucher-185') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Evoucher/showDataGrafikEvoucherSdl"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		}else if (data[i] == 'Account-Purchase-180') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/showdataAccountVoucherSdp"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		}else if (data[i] == 'Account-Purchase-185') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/showdataAccountVoucherSdl"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		}else if (data[i] == 'Load-Balancer-68') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/LoadBalancer/showGrafikLoadBalancerEd"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		}else if (data[i] == 'Load-Balancer-69') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/LoadBalancer/showGrafikLoadBalancerEs"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		}else if (data[i] == 'adjusmentDA-180') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Adjust/showGrafikAdjustSdp"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		}else if (data[i] == 'adjusmentDA-185') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Adjust/showGrafikAdjustSdl"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		}else if (data[i] == 'ussd-node-187') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/Dashboard/ussdSdt"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		}else if (data[i] == 'RBT') {
			$('#charts').append('<a href="http://150.242.111.235/vas/index.php/ReportRbt/showGrafikRbt"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		} else {
			$('#charts').append('<a href="' + api_url + '/' + link[i] + '"><div id="' + data[i] + '" class="chart"> <b>' + nama[i] + '</b> </div></a>');
		}

		chart('', data[i], success_data[i], error_data[i]);


	}

	function chart(name = null, idName = null, success_data, error_data) {
		anychart.onDocumentReady(function() {

			// random
			var rSuccess = Math.floor(Math.random() * 100) + 20;
			var rError = Math.floor(Math.random() * 20) + 1;

			// create data

			var data = [{
					"x": "Success",
					"value": success_data,
					"fill": 'green'
				},
				{
					"x": "Failed",
					"value": error_data,
					"fill": '#ed1d24'
				},
			]


			// create a 3d column chart
			var chart = anychart.column3d();

			chart.maxPointWidth(50);
			chart.labels().useHtml(true);
			chart.labels().enabled(true);
			chart.labels().format("<b style='color:black;'>{%value}</b>");

			var labels = chart.xAxis().labels();
			labels.enabled(false);
			// create a column series and set the data
			var series = chart.column(data);
			series.stroke(null);
			var tooltip = series.tooltip();
			tooltip.format(function(data) {
				return "Count : " + data.value + "%"
			});

			// Get background.
			var background = chart.background();

			// background.stroke('2 rgb(36,102,177)');
			// background.corners(10);
			background.fill({
				keys: [
					'#DDD 0.2',
					'#DDD',
					'#FFF 0.2'
				],
				angle: -90
			});

			// Remove Credits
			var credits = chart.credits();
			credits.text('');

			// set the chart title
			// var title = chart.title();
			// title.enabled(true);
			// title.text('UMB - ALL');

			// Set text font size.
			// title.fontSize(12);

			// set the container id
			chart.container(idName);

			// initiate drawing the chart
			chart.draw();
		});
	}






	$(document).ready(function() {

		$('.pilihan_reportumb').hide();
		$('.pilihan_reportsmsc').hide();
		$('.pilihan_reportbalancetran').hide();
		$('.pilihan_reporttransferquota').hide();
		$('.pilihan_reportgamesmobiwin').hide();
		$('.pilihan_reportgamestl').hide();
		$('.pilihan_reportvouchergames').hide();
		$('.pilihan_reportextendme').hide();

	});


	// report click smsc
	$('#click_reportsmsc').mouseover(function() {
		$('.pilihan_report_smsc').show();
	});

	$('#click_reportsmsc').mouseleave(function() {
		$('.pilihan_report_smsc').hide();

	});

	// akhir click smsc

	// balancetransefer
	$('#click_reportbalancetransfer').mouseover(function() {
		$('.pilihan_report_balancetransfer').show();
	});

	$('#click_reportbalancetransfer').mouseleave(function() {
		$('.pilihan_report_balancetransfer').hide();

	});
	// akhirbalancetransfer

	// umb
	$('#click_reportumb').mouseover(function() {
		$('.pilihan_reportumb').show();
	});

	$('#click_reportumb').mouseleave(function() {
		$('.pilihan_reportumb').hide();

	});
	// akhir umb

	// transferquota
	$('#click_reporttransferquota').mouseover(function() {
		$('.pilihan_reporttransferquota').show();
	});

	$('#click_reporttransferquota').mouseleave(function() {
		$('.pilihan_reporttransferquota').hide();

	});
	// akhirtransferquota

	// transfergamesmobiwin
	$('#click_reportgamesmobiwin').mouseover(function() {
		$('.pilihan_reportgamesmobiwin').show();
	});

	$('#click_reportgamesmobiwin').mouseleave(function() {
		$('.pilihan_reportgamesmobiwin').hide();

	});
	// akhirgamesmobiwin

	// transfergamesTl
	$('#click_reportgamestl').mouseover(function() {
		$('.pilihan_reportgamestl').show();
	});

	$('#click_reportgamestl').mouseleave(function() {
		$('.pilihan_reportgamestl').hide();

	});
	// akhirgamesTl

	// click_reportvouchergame
	$('#click_reportvouchergame').mouseover(function() {
		$('.pilihan_reportvouchergame').show();
	});

	$('#click_reportvouchergame').mouseleave(function() {
		$('.pilihan_reportvouchergame').hide();

	});
	// akhirclick_reportvouchergame

	//pilihan_reportextendme
	$('#click_reportextendme').mouseover(function() {
		$('.pilihan_reportextendme').show();
	});

	$('#click_reportextendme').mouseleave(function() {
		$('.pilihan_reportextendme').hide();

	});
	// pilihan_reportextendme
</script>

</html>
