$('body').css('padding-right', '0px');
var /*api_url = 'http://150.242.111.235/apibonita/index.php/',
 full_base_name = 'http://150.242.111.235/newbonita/';*/
        // api_url = 'http://localhost/apibonita/index.php/',
        // api_urlapp = 'http://localhost/apibonita/',
        // full_base_name = 'http://localhost/newbonita/',
        api_url = 'http://localhost/apibonita/index.php/',
        api_urlapp = 'http://localhost/apibonita/',
        full_base_name = 'http://localhost/tceldashboard/',
        // api_url = 'http://localhost/apibonita/index.php/',
        // api_urlapp = 'http://localhost/apibonita/',
        // full_base_name = 'http://localhost/',
        historyurl = [];
$.pages = 'pages/';

// Privileges
$.listMenu = [];


$.listMenu['login.index'] = 'Y';
$.listMenu['dashboard.index'] = 'Y';
$.listMenu['dashboard.setting'] = 'Y';

// Distributions
$.listMenu['distribution.ballanceRequest'] = 'Y';
$.listMenu['distribution.ballanceAllocation'] = 'Y';
$.listMenu['distribution.addBallanceRequest'] = 'Y';
$.listMenu['dashboard.setting'] = 'Y';

var dd;
$.latstart, $.lngstart;
$(document).ready(function () {
    // $(".breadcrumb").hide();

    $("#titlePage").html("Dashboard");
    $("#openedPage").html("Dashboard");
    $("#modulePage").attr('style', 'display:none');
    var fl = localStorage['firstLog'];

    if (fl==1) {
        $(".page-title").attr('style','display:none');
        $(".breadcrumb").attr('style','display:none');
        $(".side-menu").attr('style','display:none');
        $(".content-page").attr('style','margin-left:0');
        $(".konten").load("template/loginFirst.html");
    }else{
        $(".konten").load("dashboard/dashboard.html");
    }
    dd = getDataLogin();
    $("#loader").fadeOut('fast');


});

function printElement(elem) {
    var domClone = elem.cloneNode(true);
    
    var $printSection = document.getElementById("printSection");
    
    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }
    
    $printSection.innerHTML = "";
    $printSection.appendChild(domClone);
    window.print();
}

function genPDF()
  {
   html2canvas($("#section-to-print")[0],{
   onrendered:function(canvas){

   var img=canvas.toDataURL("image/png");
   var doc = new jsPDF();
   doc.addImage(img,'JPEG',20,20);
   doc.save('test.pdf');
   }

   });

  }

function getToken() {
    var jqXHR = $.ajax({
        url: "https://apimytelkomcel.tk:8443/MyTelkomcelREST/getToken",
        dataType: 'json',
        async: false,
        type: 'get',
        success: function (d) {
            $("#loaderNew").remove();
            return d;
            // debugger;
        }, complete: function (d) {
            $("#loaderNew").remove();
            return d;
        }, error: function (xhr) {
            $("#loaderNew").remove();
            return xhr;
        }
    });
    try {
        return $.parseJSON(jqXHR.responseText);
    } catch (err) {
        console.log(err);
    }
}

var flag = 0;
$(document).ready(function () {
    // checkRules();
    $("#sidebar-menu li a.primary-menu").click(function (event) {
        event.preventDefault();
        event.stopPropagation();
        var th = $(this);
        var url = $(this).attr('href'),
                modul = $(this).data('modul'),
                title = $(this).data('title');
        if (flag==0) {
        	$.ajax({
	            type: 'GET',
	            url: url + '?u=' + Math.random(),
	            beforeSend: function () {
	                // cekSession();
	                flag = 1;
	                $(".breadcrumb").fadeIn();
	                $("#sidebar-menu").css('cursor:wait !important;');
	                $("#sidebar-menu").css('display:none !important;');
	                if($("#sidebar-menu li").hasClass('active')){
	                	$("#sidebar-menu li").removeClass('active');
		            };
	                $(".konten").html('<center><div style="margin-top:25vh;" class="loader"></div></center>');
	            },
	            error: function () {
	                $("#titlePage").html(title);
	                $("#openedPage").html(title);
	                $("#modulePage").html(modul);
	                $(".konten").html('<div class="wrapper-page"> <div class="ex-page-content text-center"> <div class="text-error"><span class="text-primary">4</span><i class="ti-face-sad text-pink"></i><span class="text-info">4</span></div> <h2>Who0ps! Page not found</h2><br> <p class="text-muted">This page cannot found or is missing.</p> <p class="text-muted">Use the navigation above or the button below to get back and track.</p> <br> <a class="btn btn-default waves-effect waves-light" href="index.html"> Return Home</a> </div> </div> ');
	            },
	            success: function (data) {
	                runPages();
	                $("#titlePage").html(title);
	                $("#openedPage").html(title);
	                if (modul == '') {
	                    $("#modulePage").attr('style', 'display:none');
	                } else {
	                    $("#modulePage").attr('style', 'display:block').html(modul);
	                }
	                th.parent().addClass('active');
	                drawBreadCrumb();

	                document.title = "Telkomcel Dashboard - " + title;
	                $(".konten").fadeOut(300, function () {
	                    $(".konten").html(data).delay(100).fadeIn(300);
	                })
	            },
	            complete:function() {
	            	setTimeout(function() {
	            		if (th.data('title')=='Dashboard') {
							demo.initChartist();
		            	}
		            	flag = 0;
	            	},500);
	            }
	        });
        }else{
        	toastr.options = {
        		"closeButton": false,
        		"debug": false,
        		"newestOnTop": false,
        		"progressBar": false,
        		"positionClass": "toast-bottom-right",
        		"preventDuplicates": true,
        		"onclick": null,
        		"showDuration": "300",
        		"hideDuration": "1000",
        		"timeOut": "1000",
        		"extendedTimeOut": "1000",
        		"showEasing": "swing",
        		"hideEasing": "linear",
        		"showMethod": "fadeIn",
        		"hideMethod": "fadeOut"
        	}
        	// debugger;
        	toastr["warning"]("too much action click. Please wait a second :)", "Oops!");
        	$("#sidebar-menu").hide();
        	setTimeout(function() {
	        	$("#sidebar-menu").show();
        	},1500);
        }
        
    });
});

function setLatStart() {
    $.ajax({
        url: api_url + 'mwa/getOutletsNotInRouteTrip?id=9999',
        dataType: 'json',
        method: 'get',
        success: function (data) {
            var n = data.data[0];
            $.latstart = parseFloat(n.lat);
            $.lngstart = parseFloat(n.lng);
            // debugger;
        }
    })
}

function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}
function setMSISDNData(list) {
    var lastStatusMsisdn = '';
    var listFinal = [];
    var keyFinal = 0;

    array = [], arrayend = [];
    for (var key in list) {

        // listFinal[keyFinal]['idnew'] = [];
        // console.log(list[key]);
        // console.log(list[list.length-1]['status'],lastStatusMsisdn);
        if (lastStatusMsisdn != list[key]['status']) {//disini tampung next data karena beda status
            var idmsisdn = list[key]['msisdn_fk'];
            array.push(idmsisdn);
            lastStatusMsisdn = list[key]['status'];

            listFinal[keyFinal] = list[key];
            if (list[key - 1] != null) {

                listFinal[keyFinal - 1]['msisdn_arr'] = [];

                var idmsisdnend = (list[key - 1]['msisdn']) ? list[key - 1]['msisdn'] : list[key + 1]['msisdn'];

                listFinal[keyFinal - 1]['msisdn_end'] = idmsisdnend;//kasih msisdn sebelumnya end msisdn
                listFinal[keyFinal - 1]['idend'] = list[list.length - 1]['idinventory'];//kasih msisdn sebelumnya end msisdn
                // array.push(idmsisdn);
                listFinal[keyFinal - 1]['msisdn_arr'].push(idmsisdn);
                listFinal[keyFinal - 1]['idnyacoeg'] = array;
            }
            if (listFinal[keyFinal]['msisdn_end'] == undefined) {
                // var idmsisdn = list[key]['msisdn_fk'];
                // arrayend.push(idmsisdn);
                listFinal[keyFinal]['msisdn_end'] = list[key].msisdn;
                listFinal[keyFinal]['idnyacoegend'] = array;

            }
            keyFinal++;
        } else if (list[list.length - 1]['status'] == lastStatusMsisdn) {
            listFinal[keyFinal] = list[key];
            if (list[key - 1] != null) {

                var idmsisdnend = (list[list.length - 1]['msisdn']) ? list[list.length - 1]['msisdn'] : '';

                listFinal[keyFinal - 1]['idend'] = list[list.length - 1]['idinventory'];//kasih msisdn sebelumnya end msisdn
                listFinal[keyFinal - 1]['msisdn_end'] = idmsisdnend;//kasih msisdn sebelumnya end msisdn
            }
            if (listFinal[keyFinal]['msisdn_end'] == undefined) {
                listFinal.splice(keyFinal, 1);
                // console.log(listFinal);
            }
        } else {
            var idmsisdn = list[key]['msisdn_fk'];
            array.push(idmsisdn);
            // debugger;
            // list[list.length + 1]['status'] = '2';
        }

        // listFinal[keyFinal]['idnew'] = array;


    }

    return listFinal;
}

function setMSISDNDataDist(list) {
    var lastStatusMsisdn = '';
    var listFinal = [];
    var keyFinal = 0;

    array = [], arrayend = [];
    for (var key in list) {

        // listFinal[keyFinal]['idnew'] = [];
        // console.log(list[key]);
        // console.log(list[list.length-1]['status'],lastStatusMsisdn);

        var msisdnBef = 0;
        var msisdnAft = parseInt(list[key]['msisdn']);

        if (list[key - 1] != null) {
            msisdnBef = parseInt(list[key - 1]['msisdn']) + 1;
        } else {
            msisdnBef = 0;
        }

        if (lastStatusMsisdn != list[key]['status'] || msisdnBef != msisdnAft) {//disini tampung next data karena beda status
            var idmsisdn = list[key]['msisdn_fk'];
            array.push(idmsisdn);
            lastStatusMsisdn = list[key]['status'];

            listFinal[keyFinal] = list[key];
            if (list[key - 1] != null) {

                listFinal[keyFinal - 1]['msisdn_arr'] = [];

                var idmsisdnend = (list[key - 1]['msisdn']) ? list[key - 1]['msisdn'] : list[key + 1]['msisdn'];

                listFinal[keyFinal - 1]['msisdn_end'] = idmsisdnend;//kasih msisdn sebelumnya end msisdn
                listFinal[keyFinal - 1]['idend'] = list[list.length - 1]['idinventory'];//kasih msisdn sebelumnya end msisdn
                // array.push(idmsisdn);
                listFinal[keyFinal - 1]['msisdn_arr'].push(idmsisdn);
                listFinal[keyFinal - 1]['idnyacoeg'] = array;
            }
            if (listFinal[keyFinal]['msisdn_end'] == undefined) {
                // var idmsisdn = list[key]['msisdn_fk'];
                // arrayend.push(idmsisdn);
                listFinal[keyFinal]['msisdn_end'] = list[key].msisdn;
                listFinal[keyFinal]['idnyacoegend'] = array;

            }
            keyFinal++;
        } else if (list[list.length - 1]['status'] == lastStatusMsisdn) {
            listFinal[keyFinal] = list[key];
            if (list[key - 1] != null) {

                var idmsisdnend = (list[list.length - 1]['msisdn']) ? list[list.length - 1]['msisdn'] : '';

                listFinal[keyFinal - 1]['idend'] = list[list.length - 1]['idinventory'];//kasih msisdn sebelumnya end msisdn
                listFinal[keyFinal - 1]['msisdn_end'] = idmsisdnend;//kasih msisdn sebelumnya end msisdn
            }
            if (listFinal[keyFinal]['msisdn_end'] == undefined) {
                listFinal.splice(keyFinal, 1);
                // console.log(listFinal);
            }
        } else {
            var idmsisdn = list[key]['msisdn_fk'];
            array.push(idmsisdn);
            // debugger;
            // list[list.length + 1]['status'] = '2';
        }

        // listFinal[keyFinal]['idnew'] = array;


    }

    return listFinal;
}

function setMSISDNDataDist(list) {
    var lastStatusMsisdn = '';
    var listFinal = [];
    var keyFinal = 0;

    array = [], arrayend = [];
    for (var key in list) {

        // listFinal[keyFinal]['idnew'] = [];
        // console.log(list[key]);
        // console.log(list[list.length-1]['status'],lastStatusMsisdn);

        var msisdnBef = 0;
        var msisdnAft = parseInt(list[key]['msisdn']);

        if (list[key - 1] != null) {
            msisdnBef = parseInt(list[key - 1]['msisdn']) + 1;
        } else {
            msisdnBef = 0;
        }
        console.log(msisdnBef, msisdnAft);
        if (lastStatusMsisdn != list[key]['status'] || msisdnBef != msisdnAft) {//disini tampung next data karena beda status
            var idmsisdn = list[key]['msisdn_fk'];
            array.push(idmsisdn);
            lastStatusMsisdn = list[key]['status'];

            listFinal[keyFinal] = list[key];
            if (list[key - 1] != null) {

                listFinal[keyFinal - 1]['msisdn_arr'] = [];

                var idmsisdnend = (list[key - 1]['msisdn']) ? list[key - 1]['msisdn'] : list[key + 1]['msisdn'];

                listFinal[keyFinal - 1]['msisdn_end'] = idmsisdnend;//kasih msisdn sebelumnya end msisdn
                listFinal[keyFinal - 1]['idend'] = list[list.length - 1]['idinventory'];//kasih msisdn sebelumnya end msisdn
                // array.push(idmsisdn);
                listFinal[keyFinal - 1]['msisdn_arr'].push(idmsisdn);
                listFinal[keyFinal - 1]['idnyacoeg'] = array;
            }
            if (listFinal[keyFinal]['msisdn_end'] == undefined) {
                // var idmsisdn = list[key]['msisdn_fk'];
                // arrayend.push(idmsisdn);
                listFinal[keyFinal]['msisdn_end'] = list[key].msisdn;
                listFinal[keyFinal]['idnyacoegend'] = array;

            }
            keyFinal++;
        } else if (list[list.length - 1]['status'] == lastStatusMsisdn) {
            listFinal[keyFinal] = list[key];
            if (list[key - 1] != null) {

                var idmsisdnend = (list[list.length - 1]['msisdn']) ? list[list.length - 1]['msisdn'] : '';

                listFinal[keyFinal - 1]['idend'] = list[list.length - 1]['idinventory'];//kasih msisdn sebelumnya end msisdn
                listFinal[keyFinal - 1]['msisdn_end'] = idmsisdnend;//kasih msisdn sebelumnya end msisdn
            }
            if (listFinal[keyFinal]['msisdn_end'] == undefined) {
                listFinal.splice(keyFinal, 1);
                // console.log(listFinal);
            }
        } else {
            var idmsisdn = list[key]['msisdn_fk'];
            array.push(idmsisdn);
            // debugger;
            // list[list.length + 1]['status'] = '2';
        }

        // listFinal[keyFinal]['idnew'] = array;


    }

    return listFinal;
}

function setMSISDNDataDist(list) {
    var lastStatusMsisdn = '';
    var listFinal = [];
    var keyFinal = 0;

    array = [], arrayend = [];
    for (var key in list) {

        // listFinal[keyFinal]['idnew'] = [];
        // console.log(list[key]);
        // console.log(list[list.length-1]['status'],lastStatusMsisdn);

        var msisdnBef = 0;
        var msisdnAft = parseInt(list[key]['msisdn']);

        if (list[key - 1] != null) {
            msisdnBef = parseInt(list[key - 1]['msisdn']) + 1;
        } else {
            msisdnBef = 0;
        }
        console.log(msisdnBef, msisdnAft);
        if (lastStatusMsisdn != list[key]['status'] || msisdnBef != msisdnAft) {//disini tampung next data karena beda status
            var idmsisdn = list[key]['msisdn_fk'];
            array.push(idmsisdn);
            lastStatusMsisdn = list[key]['status'];

            listFinal[keyFinal] = list[key];
            if (list[key - 1] != null) {

                listFinal[keyFinal - 1]['msisdn_arr'] = [];

                var idmsisdnend = (list[key - 1]['msisdn']) ? list[key - 1]['msisdn'] : list[key + 1]['msisdn'];

                listFinal[keyFinal - 1]['msisdn_end'] = idmsisdnend;//kasih msisdn sebelumnya end msisdn
                listFinal[keyFinal - 1]['idend'] = list[list.length - 1]['idinventory'];//kasih msisdn sebelumnya end msisdn
                // array.push(idmsisdn);
                listFinal[keyFinal - 1]['msisdn_arr'].push(idmsisdn);
                listFinal[keyFinal - 1]['idnyacoeg'] = array;
            }
            if (listFinal[keyFinal]['msisdn_end'] == undefined) {
                // var idmsisdn = list[key]['msisdn_fk'];
                // arrayend.push(idmsisdn);
                listFinal[keyFinal]['msisdn_end'] = list[key].msisdn;
                listFinal[keyFinal]['idnyacoegend'] = array;

            }
            keyFinal++;
        } else if (list[list.length - 1]['status'] == lastStatusMsisdn) {
            listFinal[keyFinal] = list[key];
            if (list[key - 1] != null) {

                var idmsisdnend = (list[list.length - 1]['msisdn']) ? list[list.length - 1]['msisdn'] : '';

                listFinal[keyFinal - 1]['idend'] = list[list.length - 1]['idinventory'];//kasih msisdn sebelumnya end msisdn
                listFinal[keyFinal - 1]['msisdn_end'] = idmsisdnend;//kasih msisdn sebelumnya end msisdn
            }
            if (listFinal[keyFinal]['msisdn_end'] == undefined) {
                listFinal.splice(keyFinal, 1);
                // console.log(listFinal);
            }
        } else {
            var idmsisdn = list[key]['msisdn_fk'];
            array.push(idmsisdn);
            // debugger;
            // list[list.length + 1]['status'] = '2';
        }

        // listFinal[keyFinal]['idnew'] = array;


    }

    return listFinal;
}

function setTableBlock(d) {
    var no = 1;
    for (var n in d) {
        var dt = d[n],
                stat = dt.status, statText, actText;
        if (stat == '0') {
            actText = '<a data-toggle="modal" data-target="#sim-purchase-order">' +
                    '<span class="label label-success">Active</span>' +
                    '</a>';
            statText = '<a data-toggle="modal" data-target="#sim-purchase-order">' +
                    '<span class="label label-warning">Block</span>' +
                    '</a>';
        } else if (stat == '1') {
            actText = '<a data-toggle="modal" data-target="#sim-purchase-order">' +
                    '<span class="label label-warning">Block</span>' +
                    '</a>';
            statText = '<a data-toggle="modal" data-target="#sim-purchase-order">' +
                    '<span class="label label-success">Active</span>' +
                    '</a>';
        } else {
            actText = '<a data-toggle="modal" data-target="#sim-purchase-order">' +
                    '<span class="label label-danger">Block Permanent</span>' +
                    '</a>';
            statText = "";
        }
        $("#tblCtBlockHistory").append('<tr>' +
                '<th scope="row">' + no + '</th>' +
                '<td>' + dt.block_code + '</td>' +
                '<td>' + dt.channel_name + '</td>' +
                '<td>' + dt.created_date + '</td>' +
                '<td>' + statText + '</td>' +
                '<td>' + actText + '</td>' +
                '<td>' + dt.reason + '</td>' +
                '</tr>');
        no++;
    }
}

$(window).on('hashchange', function () {
    runMenuGenerate();

    if (location.hash != 'login.index') {

        historyurl.push(location.hash);
        if (historyurl.length > 2) {
            historyurl.splice(0, historyurl.length - 2)
        }
        ;
    }
    checkURL();
    runFormPlugins();

});
$(window).on('reload', function () {
    runMenuGenerate();
    checkURL();
    runFormPlugins();
})


function cekMenu() {

    try {
        var ret = $.listMenu[url] == 'Y' ? true : false;
        if (ret) {
            console.log('allowed for ' + url)
        } else {
            console.log('not allowed for ' + url)
        }

        return ret;
    } catch (e) {
        console.log('not allowed for ' + url)
        return false;
    }
}
function checkRules() {


    var allowUrl = localStorage['allowUrl'].toString().split('[|]');

    var listMenu = $('#sidebar-menu li:not(:first) a');

    for (var ls in  listMenu) {
        var mn = listMenu.eq(ls);

        if (mn.attr('href') != 'javascript:void(0);') {
            mn.addClass('hideForever');
        }
    }

    for (var au in allowUrl) {
        var al = allowUrl[au];

    //        console.log('#sidebar-menu a[href="' + al + '"]');

        $('#sidebar-menu a[href="' + al + '"]').removeClass('hideForever');
    }

    $('.hideForever').remove();

    var listParent = $('#sidebar-menu');

    for (var lp in listParent) {
        var ml = listParent.eq(lp);

        if (ml.find('li a').length == 0) {
            ml.parent().remove();
        }
    }

    listParent = $('#sidebar-menu');
    for (var lp in listParent) {
        var ml = listParent.eq(lp);

        if (ml.find('li a').length == 0) {
            ml.parent().remove();
        }
    }

}
function generateMenu() {

}
function runMenuGenerate() {
    if (localStorage['isCustCode']) {
        $.listMenu['contract.overview'] = 'N';
        $(".custMsisdn").addClass('hide');
        $('.afterCustMsisdnCode').addClass('hide');

        $.listMenu['customers.contract'] = 'Y';
        $.listMenu['customers.overview'] = 'Y';
        $('.afterCustCode').removeClass('hide');

    } else if (localStorage['isMsisdnNumber']) {

        $.listMenu['customers.contract'] = 'N';
        $.listMenu['customers.overview'] = 'N';
        $('.afterCustCode').addClass('hide');


        $.listMenu['contract.overview'] = 'Y';
        $.listMenu['contract.account_overview'] = 'Y';
        $(".custMsisdn").removeClass('hide');
        $('.afterCustMsisdnCode').removeClass('hide');
    } else {
        // $('.afterCustCode a').removeAttr('id');
        localStorage.removeItem('isMsisdnNumber');
        localStorage.removeItem('isCustCode');
        // CustOverview

        // // Contract

    }
}
var sessionData = cekSesi();

function cekSesi() {
    try {
        var local = localStorage['status'];
        return local;
    } catch (e) {
        var local = null;
        return local;
    }
}

function checkURL() {
    //get the url by removing the hash
    url = location.hash.replace(/^#/, '');
    console.log(historyurl);
    //handling open new tab by r3 mouse click by benzo
    if (url.indexOf('?') >= 0) {
        url = location.hash.split('?');
        url = url[0].replace(/^#/, '');
    }
    console.log(url);
    console.log(sessionData);
    if (sessionData == null) {
        // if (url=="") {
        url = 'login.index';
    } else if (url == "" && sessionData != null) {
        url = 'dashboard.index';
    } else if (url == 'login.index' && sessionData != null) {
        url = 'dashboard.index';
    } else {
        if (historyurl.length != 0) {

            localStorage.setItem('lastLoggedIn', null);
        }
        url = url;
    }
    container = $('.konten');

    // Do this if url exists (for page refresh, etc...)
    if (url) {
        // remove all active class
        $('.menu ul li.active').removeClass("active");
        // match the url and add the active class

        $('.menu ul li:has(a[href="#' + url + '"])').addClass("active");
        $('.menu ul li a[href="#' + url + '"]').addClass("toggled");
        title = ($('a[href="#' + url + '"]').data('title')) ? $('a[href="#' + url + '"]').data('title') : $('a[href="#' + url + '"]').data('title');

        document.title = (title || document.title);
        loadURL(url, container);
    } else {
        $('.menu ul li:has(a[href="#' + url + '"])').addClass("active");
        $('.menu ul li a[href="#' + url + '"]').addClass("toggled");

        // grab the first URL from nav
        $this = $('.menu ul > ul > li:first-child > a[href!="#"]');

        //update hash

        window.location.hash = $this.attr('href') ? $this.attr('href') : '';
    }

}

// LOAD AJAX PAGES
var dataUrl = {};
function loadURL(url, container) {

    // clearInterval(SI);
    // SI = false;

    if (url.indexOf('?') >= 0) {
        url = url.split('?');
        url = url[0];
    }

    if ($('div#logout').length > 0 && url == 'index') {
        $('.konten').html('');

        return false;
    }
    if (!cekMenu(url, '', '', 'view')) {
        console.log(historyurl);
        swal({
            title: "Info", text: "Not allowed access URL.", type: "warning",

            showCancelButton: false,
            showConfirmButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false
        }, function () {
            swal.close();
            var urlredirect;
            if (historyurl.length > 0) {
                urlredirect = historyurl[0];
            } else {
                if (localStorage['isLoggedIn']) {
                    urlredirect = 'dashboard.index';
                } else {
                    urlredirect = 'login.index';
                }
            }
            window.location.hash = urlredirect;

        });

        return false;
    }

    var startU = microtime();
    var nVar = $('#nLocHash').length > 0 ? $('#nLocHash').val() : url;
    $.ajax({
        type: "GET",
        url: full_base_name + $.pages + url.replace('.', '/') + '.html?&uqqq=' + new Date().getTime(),
        dataType: 'html',
        data: dataUrl,
        cache: false,
        // headers: {
        //     'Content-Type': 'application/x-www-form-urlencoded'
        // },
        // async: true,
        beforeSend: function (request) {
            // request.setRequestHeader("Authorization", "Negotiate");
            $('.page-loader-wrapper').fadeIn();
            if (url != 'login.index') {

                if (container[0] == $(".konten")[0]) {
                    $("html, body").animate({
                        scrollTop: 0
                    }, "fast");
                } else {
                    container.animate({
                        scrollTop: 0
                    }, "fast");
                }
            }
            $("#breadcumb").fadeIn('slow');
        },
        complete: function () {
            dataUrl = {};
            setTimeout(function () {
                $('.page-loader-wrapper').fadeOut();
            }, 100);
        },
        success: function (data) {
        	// debugger;
            // $("#breadcumb").html('<h2>' + document.title + '<label id="lastLoginDate" class="label label-default pull-right afterLogin"></label></h2>');
            drawBreadCrumb();
            // var url = $(this).attr('href'),
            //     modul = $(this).data('modul'),
            //     title = $(this).data('title');
            //     $("#titlePage").html(title);
            //     $("#openedPage").html(title);
            //     $("#modulePage").html(modul);
            // debugger;
            try {
                // console.log(url);
                if (url == 'login.index') {
                    $("#buatShow").html(function (a) {
                        var bodySh = $("#buatShow");

                        // console.log(bodySh);
                        setLoginPage(bodySh);
                    })
                } else {

                    container.css({
                        opacity: '0.0'
                    }).html(data).delay(50).animate({
                        opacity: '1.0'
                    }, 300);
                }


                // $('#debugTextJS').remove();
            } catch (etc) {
                // $('#debugTextJS').remove();
                // if ($.debug_ajax_respond) {
                //     $('body').append('<div id="debugTextJS" style="min-height: 200px;background-color: black;color: white;font-family: lucida;width: 100%;position: absolute;z-index: 100000;padding: 10px;overflow: auto;"><table width="100%"><tr><td align="left"><strong style="font-size:12pt">Debug Tools Javascript : </strong></td><td align="right"><a onclick="$(\'#debugTextJS\').remove();" href="javascript:void(0);">[ X ]</a></td></tr></table><br /><pre style="margin-top:10px">' + etc + '</pre></div>');
                //     $('#debugTextJS').scrollToMe();
                // }
            }


            $.ajaxSetup({
                data: {
                    n: $('#nLocHash').val()
                }
            });


        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('.page-loader-wrapper').fadeOut();
            var bodySh = $(".konten");
            $.ajax({
                url: full_base_name + '404.html',
                method: 'GET',
                dataType: 'html',
                // headers: {
                //     'Content-Type': 'application/x-www-form-urlencoded'
                // },
                beforeSend: function (request) {
                    // request.setRequestHeader("Authorization", "Negotiate");
                },
                success: function (ht) {
                    bodySh.html(ht);
                    // return;
                }
            })
        },
    });
}

function drawBreadCrumb() {
    $("ol.breadcrumb").empty();
    $("ol.breadcrumb").append($("<li class='breadcrumb-item'>Home</li>"));
    $('#sidebar-menu li.active > a').each(function () {
        $("#titlePage").html($(this).data('title'));
        var mdl = $(this).attr('data-modul');
        var kde = $(this).attr('data-title') ? ' (' + $(this).attr('data-title') + ')' : '';
        $("ol.breadcrumb").append($("<li class='breadcrumb-item active'></li>").html(mdl));
        $("ol.breadcrumb").append($("<li class='breadcrumb-item active'></li>").html($.trim($(this).clone().children(".badge").remove().end().text()) + kde));
    });
    // <ol class="breadcrumb breadcrumb-col-teal"> <li><a href="javascript:void(0);">Home</a></li> <li><a href="javascript:void(0);">Library</a></li> <li><a href="javascript:void(0);">Data</a></li> <li class="active">File</li> </ol> }
}
function setLoginPage(bodySh) {
    $.ajax({
        url: full_base_name + 'pages/login/index.html',
        method: 'GET',
        dataType: 'html',
        // headers: {
        //     'Content-Type': 'application/x-www-form-urlencoded'
        // },
        beforeSend: function (request) {
            // request.setRequestHeader("Authorization", "Negotiate");
        },
        success: function (ht) {
            bodySh.addClass('login-page').html(ht);
            // return;
        }
    })
}

function input(n, v) {
    return $("#" + n).val(v);
}


function setTableMembers(data, idtbl) {
    var no = 1,
            tbl = $("#" + idtbl);
    tbl.html('');
    for (var i in data) {
        var n = data[i];
        tbl.append(
                '<tr>' +
                '<td>' + no + '</td>' +
                '<td><a href="javascript:void(0);" onclick="openSalesDetail(\'' + n.id_user + '\',\'' + n.name + '\')" class="text-linked">' + n.username + '</a></td>' +
                '<td>' + n.name + '</td>' +
                '</tr>'
                );

        no++;
    }
}
function setHtmlForm(d) {
    setTimeout(function () {
        if (d.data[0].statusName!=undefined) {
            var statName = d.data[0].statusName;
        }
        var stat = statName, statText;
        if (stat == '0') {
            statText = 'Active';
        } else if (stat == '1') {
            statText = 'Blocked';
        } else {
            statText = 'Blocked Permanent';
        }
        for (key in d.data[0])
        {
            if (d.data[0].hasOwnProperty(key)) {
                if ($('#' + key).hasClass('wysihtml5')) {
                    $('#' + key).data('wysihtml5').editor.setValue(d.data[0][key]);
                } else if ($('#' + key).hasClass('cnno')) {
                    var str = d.data[0][key];
                    str = str.substring(2);
                    $('#' + key).val(str);
                } else if ($('#' + key).hasClass('mnno')) {
                    var str = d.data[0][key];
                    str = str.substring(2);
                    $('#' + key).val(str);
                } else if ($('#' + key).hasClass('status')) {
                    $('#' + key).val(statText);
                } else {
                    $('#' + key).val(d.data[0][key]);
                }
                // console.log(key,d.data[0][key]);
                var strnew = d.data[0]['contact_number'];
                if (strnew != null) {
                    strnew = strnew.substring(0, 2);
                    $('.cnselect option[value=' + strnew + ']').attr('selected', 'selected');
                }

                var strmaster = d.data[0]['master_number'];

                if (strmaster != null) {
                    strmaster = strmaster.substring(0, 2);
                    $('.mnselect option[value=' + strmaster + ']').attr('selected', 'selected');
                }
            } else {

            }
        }
    }, 800);
    setEntity();
}

function setHtmlFormNew(d) {
    setTimeout(function () {
        var stat = d.datanya[0].statusName, statText;
        if (stat == '0') {
            statText = 'Active';
        } else if (stat == '1') {
            statText = 'Blocked';
        } else {
            statText = 'Blocked Permanent';
        }
        for (key in d.datanya[0])
        {
            if (d.datanya[0].hasOwnProperty(key)) {
                if ($('#' + key).hasClass('wysihtml5')) {
                    $('#' + key).data('wysihtml5').editor.setValue(d.datanya[0][key]);
                } else if ($('#' + key).hasClass('cnno')) {
                    var str = d.datanya[0][key];
                    str = str.substring(2);
                    $('#' + key).val(str);
                } else if ($('#' + key).hasClass('mnno')) {
                    var str = d.datanya[0][key];
                    str = str.substring(2);
                    $('#' + key).val(str);
                } else if ($('#' + key).hasClass('status')) {
                    $('#' + key).val(statText);
                } else {
                    $('#' + key).val(d.datanya[0][key]);
                }
                // console.log(key,d.datanya[0][key]);
                var strnew = d.datanya[0]['contact_number'];
                if (strnew != null) {
                    strnew = strnew.substring(0, 2);
                    $('.cnselect option[value=' + strnew + ']').attr('selected', 'selected');
                }

                var strmaster = d.datanya[0]['master_number'];

                if (strmaster != null) {
                    strmaster = strmaster.substring(0, 2);
                    $('.mnselect option[value=' + strmaster + ']').attr('selected', 'selected');
                }
            } else {

            }
        }
    }, 800);
    setEntity();
}

function nulldefault(value) {
    return (value == null) ? "" : value
}

function getDataDetail(d, id, type) {
    console.log(id);
    $.ajax({
        url: api_url + 'distributions/getSCRequestDetail',
        data: {id: id, type: type},
        method: 'get',
        dataType: 'json',
        success: function (r) {
            if (r.data != '') {
                var no = 1;
                var hargatotal = 0;
                for (var i in r.data) {
                    var n = r.data[i];
                    var hargakali = n.total_price;
                    hargatotal += parseFloat(hargakali);
                    var ht = '<tr id="rowId' + no + '">' +
                            '<td>' +
                            no +
                            '</td>' +
                            '<td>' +
                            n.product_name +
                            '</td>' +
                            '<td>' +
                            n.quantity +
                            '</td>' +
                            '<td>' +
                            n.unit_price +
                            '</td>' +
                            '<td>' +
                            parseFloat(hargakali).toFixed(2) +
                            '</td>' +
                            '<td>' +
                            '<input type="number" id="idInput' + no + '" min="0" max="100" step=".01" required value="' + nulldefault(n.discount_percent) + '" name="discount_percent" parsley-type="text" data-parsley-type="number" class="form-control" onchange="setRowPrice(\'rowId' + no + '\',\'idInput' + no + '\')" placeholder=""> <input type="hidden" name="idreqdetail" value="' + n.idrequest_detail + '">' +
                            '</td>' +
                            '<td>' +
                            '<input type="text" class="form-control" name="discount_price" readonly value="' + nulldefault(n.discount_price) + '">' +
                            '</td>' +
                            '<td class="priceTotal">' +
                            '<input type="text" class="form-control" name="total_price" readonly value="' + n.total_price + '">' +
                            '</td>' +
                            '</tr>';
                    $("#rowData").append(ht);
                    no++;
                }
                $("#net_price").val(financial(hargatotal));
            }
        }
    })
}

function getDataDetailVoucher(d, id, type) {
    console.log(id);
    $.ajax({
        url: api_url + 'distributionsVoucher/getSCRequestDetail',
        data: {id: id, type: type},
        method: 'get',
        dataType: 'json',
        success: function (r) {
            if (r.data != '') {
                var no = 1;
                var hargatotal = 0;
                for (var i in r.data) {
                    var n = r.data[i];
                    var hargakali = n.total_price;
                    hargatotal += parseFloat(hargakali);
                    harganya = n.unit_price * n.quantity;

                    var netPrice = parseFloat(harganya) - parseFloat(n.discount_price);
                    var ht = '<tr id="rowId' + no + '">' +
                            '<td>' +
                            no +
                            '</td>' +
                            '<td>' +
                            n.denomination +
                            '</td>' +
                            '<td>' +
                            n.quantity +
                            '</td>' +
                            '<td>' +
                            n.unit_price +
                            '</td>' +
                            '<td>' +
                            parseFloat(harganya) +
                            '</td>' +
                            '<td>' +
                            '<input type="number" id="idInput' + no + '" min="0" max="100" step=".01" required value="' + nulldefault(n.discount_percent) + '" name="discount_percent" parsley-type="text" data-parsley-type="number" class="form-control" onchange="setRowPrice(\'rowId' + no + '\',\'idInput' + no + '\')" placeholder=""> <input type="hidden" name="idreqdetail" value="' + n.idrequest_detail + '">' +
                            '</td>' +
                            '<td>' +
                            '<input type="text" class="form-control" name="discount_price" readonly value="' + nulldefault(n.discount_price) + '">' +
                            '</td>' +
                            '<td class="priceTotal">' +
                            '<input type="text" class="form-control" name="total_price" readonly value="' + netPrice.toFixed(2) + '">' +
                            '</td>' +
                            '</tr>';
                    $("#rowData").append(ht);
                    no++;
                }
                $("#net_price").val(financial(hargatotal));
            }
        }
    })
}
function returnErrorMessage(msg) {
    toastr.warning(msg, 'Sorry', {'positionClass': 'toast-top-right', 'preventDuplicates': true, maxOpened: 1, "preventOpenDuplicates": true});
}
function returnSuccessMessage(msg) {
    toastr.success(msg, 'Success', {'positionClass': 'toast-top-right', 'preventDuplicates': true, maxOpened: 1, "preventOpenDuplicates": true});
}
function returnErrorExist() {
    toastr.warning("This data has been followed up", 'Sorry', {'positionClass': 'toast-top-right'});
}
function returnErrorPrivileges() {
    toastr.warning("You don't have privilege to access this module", 'Sorry', {'positionClass': 'toast-top-right'});
}
function setSummaryVoucher() {
    $.ajax({
        url: api_url + 'trigger/summaryVoucher',
        type: 'get'
    });
}
function setSummaryMsisdn() {
    $.ajax({
        url: api_url + 'trigger/summaryMsisdnAvailable',
        type: 'get'
    });
}
function checkFu(id, type, status) {
    var jqXHR = $.ajax({
        // url:api_url + 'distributions/getBallanceRequestTransaction?trx_parent='+id+'&type='+type,
        url: api_url + 'distributions/getBallanceRequestTransactionRejected?trx_parent=' + id + '&type=' + type + '&status=' + status,
        method: 'get',
        async: false,
        dataType: 'json',
        success: function (d) {
            return d;
        }, complete: function (d) {
            return d;
        }
    });
    try {
        return $.parseJSON(jqXHR.responseText);
    } catch (err) {
        console.log(err);
    }
}
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function isIncreasingSequence(numbers) {
    let numArr = Array.prototype.slice.call(arguments);

    for (var num = 0; num < numArr.length - 1; num++) {
        if (numArr[num] >= numArr[num + 1] || Number.isNaN(numArr[num]) || Number.isNaN(numArr[num + 1])) {
            return false;
        }
    }

    return true;
}

function checkFuIp(id, type) {
    var urll;
    if (type == 'sc') {
        urll = api_url + 'distributions/getInvoicePaymentSC?type=rejected&trx_parent=' + id;
    } else {
        urll = api_url + 'distributions/getInvoicePayment?trx_parent=' + id;
    }
    var jqXHR = $.ajax({
        url: urll,
        method: 'get',
        async: false,
        dataType: 'json',
        success: function (d) {
            return d;
        }, complete: function (d) {
            return d;
        }
    });
    try {
        return $.parseJSON(jqXHR.responseText);
    } catch (err) {
        console.log(err);
    }
}
function getDataLogin() {
    var jqXHR = $.ajax({
        url: api_url + 'auth/getDataLogin',
        data: {id: localStorage['id']},
        dataType: 'json',
        async: false,
        method: 'post',
        success: function (d) {
            return d;
            // debugger;
        }, complete: function (d) {
            $("#loaderNew").remove();
            $("#loaderNew").remove();
            return d;
        }
    });
    try {
        return $.parseJSON(jqXHR.responseText);
    } catch (err) {
        console.log(err);
    }
    // return callback;
}

function runPages() {
    $.ajaxSetup({
        global: true,
        beforeSend: function (jqXHR, settings) {
            $('.content-page').append('<div id="loaderNew" class="loading style-2"><div class="loading-wheel"></div></div>');
            $("#loaderNew").fadeIn('slow');
        },
        complete:function() {
            $("#loaderNew").remove();
        },
        error:function() {
            $("#loaderNew").remove();
        }
    });


    // $('.datepicker-autoclose').daterangepicker({
    //     buttonClasses: ['btn', 'btn-sm'],
    //     applyClass: 'btn-default',
    //     cancelClass: 'btn-white'
    // });

    // $("#userLogin").html(dd.data[0].bussName);
    // Select2
    // $(".select2").select2();
    // $('.tagsinput').tagsinput();

    // $(".select2-limiting").select2({
    //     maximumSelectionLength: 2
    // });

    // $('.selectpicker').selectpicker();

    // $('form').parsley();

    $(document).off('click', '[data-plugin="custommodal"]').on('click', '[data-plugin="custommodal"]', function (e) {
        Custombox.open({
            target: $(this).attr("href"),
            effect: $(this).attr("data-animation"),
            overlaySpeed: $(this).attr("data-overlaySpeed"),
            overlayColor: $(this).attr("data-overlayColor")
        });
        e.preventDefault();
    });
    $("a.action-menu").unbind('click').click(function (e) {
        cekSession();
        e.preventDefault();
        var url = $(this).attr('href')
        modul = $(this).data('modul'),
                title = $(this).data('title');
        document.title = "Bonita - " + title;

        moveUrl(url, modul, title);
    });
    $("[data-toggle='tooltip']").tooltip();

}
function runFormPlugins() {
    $(".select2").select2();


}

function financial(x) {
    return Number.parseFloat(x).toFixed(2);
}
function replaceAll(str, find, replace) {
    return str.split(find).join(replace);
}
function moveUrl(url, modul, title, id, callback) {
    $.ajaxSetup({
        global: true,
        beforeSend: function (jqXHR, settings) {
            $('.content-page').append('<div id="loaderNew" class="loading style-2"><div class="loading-wheel"></div></div>');
            $("#loaderNew").fadeIn('slow');
        },
        complete:function() {
            $("#loaderNew").remove();
        },
        error:function() {
            $("#loaderNew").remove();
        }
    });
    $.ajax({
        type: 'GET',
        url: url + '?u=' + Math.random(),
        beforeSend: function () {
            $(".konten div").fadeOut();
            $(".breadcrumb").fadeIn();
            $(".konten").html('<center><div style="margin-top:25vh;" class="loader"></div></center>');
        },
        error: function () {
            $(".konten").html('<div class="wrapper-page"> <div class="ex-page-content text-center"> <div class="text-error"><span class="text-primary">4</span><i class="ti-face-sad text-pink"></i><span class="text-info">4</span></div> <h2>Who0ps! Page not found</h2><br> <p class="text-muted">This page cannot found or is missing.</p> <p class="text-muted">Use the navigation above or the button below to get back and track.</p> <br> <a class="btn btn-default waves-effect waves-light" href="index.html"> Return Home</a> </div> </div> ');
        },
        success: function (data) {
            $("#titlePage").html(title);
            $(".dta").html("<input type='hidden' id='m_id' value='" + id + "' >");
            $("#openedPage").html(title);
            $("#modulePage").html(modul);
            $(".konten").fadeIn(300, function () {
                $(".konten").html(data).delay(100).fadeIn(300);
                try {
                    if (typeof id == 'function') {
                        id();
                    } else if (typeof callback == 'function') {
                        callback();
                    }
                } catch (e) {
                    console.log(e);
                }
            });

        }
    });
}


function showErrorWithNoAction(m) {
    swal({
        title: "Error", text: m, type: "error",

        showCancelButton: false,
        showConfirmButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
    }, function () {
        swal.close();

    });
    return false;
}
function runDataTable() {
    if ($.fn.dataTable.isDataTable('.dataTable')) {
        $('.dataTable').DataTable({
            "info": false,
        });
    } else {
        $('.dataTable').DataTable({
            "info": false,
        });
    }

}
function runDataTableCustom() {
    if ($.fn.dataTable.isDataTable('.dataTable')) {
        $('.dataTable').DataTable({
            "info": false,
            "order": [[2, "desc"]]
        });
    } else {
        $('.dataTable').DataTable({
            "info": false,
            "order": [[2, "desc"]]
        });
    }

}

function runDataTableCheck() {

    $('.dataTableCheck').DataTable({
        columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }],
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        "rowCallback": function (row, data) {
            if ($.inArray(data.DT_RowId, selected) !== -1) {
                $(row).addClass('selected');
            }
        },
        order: [[1, 'asc']]
    })
}

function openModalGeneral(idmodal, title, linkHtml, callback) {
    $.ajax({
        url: linkHtml,
        method: 'GET',
        success: function (d) {
            Custombox.open({
                target: "#" + idmodal,
                effect: "blur",
                overlaySpeed: "100",
                close: false,
                overlayColor: "#36404a",
                close: function () {
                    // debugger;
                },
                open: function () {
                    try {
                        callback();
                    } catch (e) {

                    }
                }
            });
            $("#" + idmodal + " div.custom-modal-text").html(d);
            $('.custom-modal-title').html(title);


        },
        error: function (e) {
            showErrorWithNoAction('Cannot open page');
        }
    })
}
var Colors = {};
Colors.names = [
    "#00ffff", //aqua
    "f0ffff", //azure
    "#f5f5dc", //beige
    "#000000", //black
    "#0000ff", //blue
    "#a52a2a", //brown
    "#00ffff", //cyan
    "#00008b", //darkblue
    "#008b8b", //darkcyan
    "#a9a9a9", //darkgrey
    "#006400", //darkgreen
    "#bdb76b",
    "#8b008b",
    "#556b2f",
    "#ff8c00",
    "#9932cc",
    "#8b0000",
    "#e9967a",
    "#9400d3",
    "#ff00ff",
    "#ffd700",
    "#008000",
    "#4b0082",
    "#f0e68c",
    "#add8e6",
    "#e0ffff",
    "#90ee90",
    "#d3d3d3",
    "#ffb6c1",
    "#ffffe0",
    "#00ff00",
    "#ff00ff",
    "#800000",
    "#000080",
    "#808000",
    "#ffa500",
    "#ffc0cb",
    "#800080",
    "#800080",
    "#ff0000",
    "#c0c0c0",
    "#ffff00"
];
function makeColor(colorNum, colors) {
    colors = Math.ceil(colors);
    if (colors < 1)
        colors = 1; // defaults to one color - avoid divide by zero
    return Math.ceil(colorNum * (360 / colors) % 360);
}
function RGB2HTML(red, green, blue)
{
    var decColor = 0x1000000 + blue + 0x100 * green + 0x10000 * red;
    return '#' + decColor.toString(16).substr(1);
}
function pinSymbol(color) {
    return {
        path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
        fillColor: color,
        fillOpacity: 1,
        strokeColor: '#Fff',
        strokeWeight: 2,
        scale: 0.8
    };
}
function rgb(r, g, b) {
    return "hsl(" + r + "," + g + "," + b + ")";
}



// TEST CODE

// var totalDIVs = 5;
// var totalColors = totalDIVs;

// for (var i = 0; i < totalDIVs; i++){
//     var element = document.createElement('div');
//     document.body.appendChild(element);
//     var color = "hsl( " + makeColor(i, totalColors) + ", 100%, 50% )";
//     element.style.backgroundColor = color;
//     element.innerHTML = color;
// }



function deleteRoute(id) {

    swal({
        title: 'Are you sure want to delete this route trip ?',
        showCancelButton: true,
        confirmButtonText: 'Submit',
        showLoaderOnConfirm: true,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger m-l-10',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                setTimeout(function () {
                    $.ajax({
                        url: api_url + 'mwa/deleteRoute',
                        data: {id: id},
                        dataType: 'json',
                        method: 'post',
                        success: function (d) {
                            if (d.success) {
                                resolve();

                            } else {
                                reject();
                            }
                        },
                        error: function () {
                            reject();
                        }
                    })
                }, 2000)
            })
        },
        allowOutsideClick: false
    }).then(function () {
        swal({
            type: 'success',
            title: 'Route has been deleted',
            showCancelButton: false,
            confirmButtonColor: '#4fa7f3'
        }).then(function () {
            var url = 'mwa/routeTrip.html',
                    modul = 'MWA',
                    title = 'Route Trip';
            moveUrl(url, modul, title);
        })
    })
}
function deleteOutletRoute(id, idrtd) {
    swal({
        title: 'Are you sure want to delete this outlet from route trip ?',
        showCancelButton: true,
        confirmButtonText: 'Submit',
        showLoaderOnConfirm: true,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger m-l-10',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                setTimeout(function () {
                    $.ajax({
                        url: api_url + 'mwa/deleteOutletsRoute',
                        method: 'post',
                        data: {id: id, idroute: idrtd},
                        dataType: 'json',
                        success: function (dt) {
                            if (dt.success) {

                                getOutletsShow(idrtd);
                                returnSuccessMessage(dt.msg);
                                resolve();
                            } else {
                                returnErrorMessage(dt.msg);
                                reject();
                            }
                        },
                        error: function () {
                            reject();
                        }
                    });
                }, 2000)
            })
        },
        allowOutsideClick: false
    });

}

// Utilities

function replaceDuitTigaDigit(str) {
    if (str != null) {

        var depan = str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return depan;
    } else {
        return "";
    }
}

function replaceDuit(duit) {
    // console.log(duit,'satu');
    duit = duit.toFixed(3).replace(/./g, function (c, i, a) {
        return i && c !== "." && ((a.length - i) % 4 === 0) ? ',' + c : c;
    });
    // console.log(duit,'dua');
    var duitbaru = duit.toString();
    var split = duitbaru.split('.');
    var depan = split[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    duit = depan + ',' + Math.ceil(parseFloat(split[1]) / 10);
    // duit = Math.ceil(duit);
    // console.log(duit,'tiga');
    return duit;
    // return duit.toFixed(2).replace(/./g, function(c, i, a) {
    //     return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
    // });
    // return duit;
}
function setSessionLogout() {
    localStorage.clear();
    localStorage.removeItem("bonitaLogin");
    localStorage.removeItem("username");
    localStorage.removeItem("position_id");
    localStorage.removeItem("id");
    localStorage.removeItem("status");
}
function cekSession() {
    var bl = localStorage['bonitaLogin'],
            id = localStorage['id'];
    if (bl == "" || bl == 'false' || bl == undefined || id == "" || id == undefined) {
        setSessionLogout();
        localStorage.setItem("sessionTimedOut", "true");

        window.location.href = '../';
    } else {

    }
}
function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);

    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();

    newDate.setHours(hours - offset);

    return newDate;
}

function microtime(get_as_float) {
    var now = new Date().getTime() / 1000;
    var s = parseInt(now, 10);

    return (get_as_float) ? now : (Math.round((now - s) * 1000) / 1000) + ' ' + s;
}
function strtotime(text, now) {

    var parsed, match, today, year, date, days, ranges, len, times, regex, i, fail = false;

    if (!text) {
        return fail;
    }

    // Unecessary spaces
    text = text.replace(/^\s+|\s+$/g, '')
            .replace(/\s{2,}/g, ' ')
            .replace(/[\t\r\n]/g, '')
            .toLowerCase();

    match = text.match(
            /^(\d{1,4})([\-\.\/\:])(\d{1,2})([\-\.\/\:])(\d{1,4})(?:\s(\d{1,2}):(\d{2})?:?(\d{2})?)?(?:\s([A-Z]+)?)?$/);

    if (match && match[2] === match[4]) {
        if (match[1] > 1901) {
            switch (match[2]) {
                case '-':
                {
                    if (match[3] > 12 || match[5] > 31) {
                        return fail;
                    }

                    return new Date(match[1], parseInt(match[3], 10) - 1, match[5],
                            match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
                }
                case '.':
                {
                    return fail;
                }
                case '/':
                {
                    if (match[3] > 12 || match[5] > 31) {
                        return fail;
                    }

                    return new Date(match[1], parseInt(match[3], 10) - 1, match[5],
                            match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
                }
            }
        } else if (match[5] > 1901) {
            switch (match[2]) {
                case '-':
                {
                    if (match[3] > 12 || match[1] > 31) {
                        return fail;
                    }

                    return new Date(match[5], parseInt(match[3], 10) - 1, match[1],
                            match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
                }
                case '.':
                {
                    if (match[3] > 12 || match[1] > 31) {
                        return fail;
                    }

                    return new Date(match[5], parseInt(match[3], 10) - 1, match[1],
                            match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
                }
                case '/':
                {
                    if (match[1] > 12 || match[3] > 31) {
                        return fail;
                    }

                    return new Date(match[5], parseInt(match[1], 10) - 1, match[3],
                            match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
                }
            }
        } else {
            switch (match[2]) {
                case '-':
                { // YY-M-D
                    if (match[3] > 12 || match[5] > 31 || (match[1] < 70 && match[1] > 38)) {
                        return fail;
                    }

                    year = match[1] >= 0 && match[1] <= 38 ? +match[1] + 2000 : match[1];
                    return new Date(year, parseInt(match[3], 10) - 1, match[5],
                            match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
                }
                case '.':
                { // D.M.YY or H.MM.SS
                    if (match[5] >= 70) { // D.M.YY
                        if (match[3] > 12 || match[1] > 31) {
                            return fail;
                        }

                        return new Date(match[5], parseInt(match[3], 10) - 1, match[1],
                                match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
                    }
                    if (match[5] < 60 && !match[6]) { // H.MM.SS
                        if (match[1] > 23 || match[3] > 59) {
                            return fail;
                        }

                        today = new Date();
                        return new Date(today.getFullYear(), today.getMonth(), today.getDate(),
                                match[1] || 0, match[3] || 0, match[5] || 0, match[9] || 0) / 1000;
                    }

                    return fail;
                }
                case '/':
                {
                    if (match[1] > 12 || match[3] > 31 || (match[5] < 70 && match[5] > 38)) {
                        return fail;
                    }

                    year = match[5] >= 0 && match[5] <= 38 ? +match[5] + 2000 : match[5];
                    return new Date(year, parseInt(match[1], 10) - 1, match[3],
                            match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
                }
                case ':':
                {
                    if (match[1] > 23 || match[3] > 59 || match[5] > 59) {
                        return fail;
                    }

                    today = new Date();
                    return new Date(today.getFullYear(), today.getMonth(), today.getDate(),
                            match[1] || 0, match[3] || 0, match[5] || 0) / 1000;
                }
            }
        }
    }

    if (text === 'now') {
        return now === null || isNaN(now) ? new Date()
                .getTime() / 1000 | 0 : now | 0;
    }
    if (!isNaN(parsed = Date.parse(text))) {
        return parsed / 1000 | 0;
    }

    date = now ? new Date(now * 1000) : new Date();
    days = {
        'sun': 0,
        'mon': 1,
        'tue': 2,
        'wed': 3,
        'thu': 4,
        'fri': 5,
        'sat': 6
    };
    ranges = {
        'yea': 'FullYear',
        'mon': 'Month',
        'day': 'Date',
        'hou': 'Hours',
        'min': 'Minutes',
        'sec': 'Seconds'
    };

    function lastNext(type, range, modifier) {
        var diff, day = days[range];

        if (typeof day !== 'undefined') {
            diff = day - date.getDay();

            if (diff === 0) {
                diff = 7 * modifier;
            } else if (diff > 0 && type === 'last') {
                diff -= 7;
            } else if (diff < 0 && type === 'next') {
                diff += 7;
            }

            date.setDate(date.getDate() + diff);
        }
    }

    function process(val) {
        var splt = val.split(' '), // Todo: Reconcile this with regex using \s, taking into account browser issues with split and regexes
                type = splt[0],
                range = splt[1].substring(0, 3),
                typeIsNumber = /\d+/.test(type),
                ago = splt[2] === 'ago',
                num = (type === 'last' ? -1 : 1) * (ago ? -1 : 1);

        if (typeIsNumber) {
            num *= parseInt(type, 10);
        }

        if (ranges.hasOwnProperty(range) && !splt[1].match(/^mon(day|\.)?$/i)) {
            return date['set' + ranges[range]](date['get' + ranges[range]]() + num);
        }

        if (range === 'wee') {
            return date.setDate(date.getDate() + (num * 7));
        }

        if (type === 'next' || type === 'last') {
            lastNext(type, range, num);
        } else if (!typeIsNumber) {
            return false;
        }

        return true;
    }

    times = '(years?|months?|weeks?|days?|hours?|minutes?|min|seconds?|sec' +
            '|sunday|sun\\.?|monday|mon\\.?|tuesday|tue\\.?|wednesday|wed\\.?' +
            '|thursday|thu\\.?|friday|fri\\.?|saturday|sat\\.?)';
    regex = '([+-]?\\d+\\s' + times + '|' + '(last|next)\\s' + times + ')(\\sago)?';

    match = text.match(new RegExp(regex, 'gi'));
    if (!match) {
        return fail;
    }

    for (i = 0, len = match.length; i < len; i++) {
        if (!process(match[i])) {
            return fail;
        }
    }
    return (date.getTime() / 1000);
}


function datePHPJS(format, timestamp) {

    var that = this;
    var jsdate, f;
    // Keep this here (works, but for code commented-out below for file size reasons)
    // var tal= [];
    var txt_words = [
        'Sun', 'Mon', 'Tues', 'Wednes', 'Thurs', 'Fri', 'Satur',
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
    ];
    // trailing backslash -> (dropped)
    // a backslash followed by any character (including backslash) -> the character
    // empty string -> empty string
    var formatChr = /\\?(.?)/gi;
    var formatChrCb = function (t, s) {
        return f[t] ? f[t]() : s;
    };
    var _pad = function (n, c) {
        n = String(n);
        while (n.length < c) {
            n = '0' + n;
        }
        return n;
    };
    f = {
        // Day
        d: function () { // Day of month w/leading 0; 01..31
            return _pad(f.j(), 2);
        },
        D: function () { // Shorthand day name; Mon...Sun
            return f.l()
                    .slice(0, 3);
        },
        j: function () { // Day of month; 1..31
            return jsdate.getDate();
        },
        l: function () { // Full day name; Monday...Sunday
            return txt_words[f.w()] + 'day';
        },
        N: function () { // ISO-8601 day of week; 1[Mon]..7[Sun]
            return f.w() || 7;
        },
        S: function () { // Ordinal suffix for day of month; st, nd, rd, th
            var j = f.j();
            var i = j % 10;
            if (i <= 3 && parseInt((j % 100) / 10, 10) == 1) {
                i = 0;
            }
            return ['st', 'nd', 'rd'][i - 1] || 'th';
        },
        w: function () { // Day of week; 0[Sun]..6[Sat]
            return jsdate.getDay();
        },
        z: function () { // Day of year; 0..365
            var a = new Date(f.Y(), f.n() - 1, f.j());
            var b = new Date(f.Y(), 0, 1);
            return Math.round((a - b) / 864e5);
        },
        // Week
        W: function () { // ISO-8601 week number
            var a = new Date(f.Y(), f.n() - 1, f.j() - f.N() + 3);
            var b = new Date(a.getFullYear(), 0, 4);
            return _pad(1 + Math.round((a - b) / 864e5 / 7), 2);
        },
        // Month
        F: function () { // Full month name; January...December
            return txt_words[6 + f.n()];
        },
        m: function () { // Month w/leading 0; 01...12
            return _pad(f.n(), 2);
        },
        M: function () { // Shorthand month name; Jan...Dec
            return f.F()
                    .slice(0, 3);
        },
        n: function () { // Month; 1...12
            return jsdate.getMonth() + 1;
        },
        t: function () { // Days in month; 28...31
            return (new Date(f.Y(), f.n(), 0))
                    .getDate();
        },
        // Year
        L: function () { // Is leap year?; 0 or 1
            var j = f.Y();
            return j % 4 === 0 & j % 100 !== 0 | j % 400 === 0;
        },
        o: function () { // ISO-8601 year
            var n = f.n();
            var W = f.W();
            var Y = f.Y();
            return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0);
        },
        Y: function () { // Full year; e.g. 1980...2010
            return jsdate.getFullYear();
        },
        y: function () { // Last two digits of year; 00...99
            return f.Y()
                    .toString()
                    .slice(-2);
        },
        // Time
        a: function () { // am or pm
            return jsdate.getHours() > 11 ? 'pm' : 'am';
        },
        A: function () { // AM or PM
            return f.a()
                    .toUpperCase();
        },
        B: function () { // Swatch Internet time; 000..999
            var H = jsdate.getUTCHours() * 36e2;
            // Hours
            var i = jsdate.getUTCMinutes() * 60;
            // Minutes
            var s = jsdate.getUTCSeconds(); // Seconds
            return _pad(Math.floor((H + i + s + 36e2) / 86.4) % 1e3, 3);
        },
        g: function () { // 12-Hours; 1..12
            return f.G() % 12 || 12;
        },
        G: function () { // 24-Hours; 0..23
            return jsdate.getHours();
        },
        h: function () { // 12-Hours w/leading 0; 01..12
            return _pad(f.g(), 2);
        },
        H: function () { // 24-Hours w/leading 0; 00..23
            return _pad(f.G(), 2);
        },
        i: function () { // Minutes w/leading 0; 00..59
            return _pad(jsdate.getMinutes(), 2);
        },
        s: function () { // Seconds w/leading 0; 00..59
            return _pad(jsdate.getSeconds(), 2);
        },
        u: function () { // Microseconds; 000000-999000
            return _pad(jsdate.getMilliseconds() * 1000, 6);
        },
        // Timezone
        e: function () { // Timezone identifier; e.g. Atlantic/Azores, ...
            // The following works, but requires inclusion of the very large
            // timezone_abbreviations_list() function.
            /*              return that.date_default_timezone_get();
             */
            throw 'Not supported (see source code of date() for timezone on how to add support)';
        },
        I: function () { // DST observed?; 0 or 1
            // Compares Jan 1 minus Jan 1 UTC to Jul 1 minus Jul 1 UTC.
            // If they are not equal, then DST is observed.
            var a = new Date(f.Y(), 0);
            // Jan 1
            var c = Date.UTC(f.Y(), 0);
            // Jan 1 UTC
            var b = new Date(f.Y(), 6);
            // Jul 1
            var d = Date.UTC(f.Y(), 6); // Jul 1 UTC
            return ((a - c) !== (b - d)) ? 1 : 0;
        },
        O: function () { // Difference to GMT in hour format; e.g. +0200
            var tzo = jsdate.getTimezoneOffset();
            var a = Math.abs(tzo);
            return (tzo > 0 ? '-' : '+') + _pad(Math.floor(a / 60) * 100 + a % 60, 4);
        },
        P: function () { // Difference to GMT w/colon; e.g. +02:00
            var O = f.O();
            return (O.substr(0, 3) + ':' + O.substr(3, 2));
        },
        T: function () { // Timezone abbreviation; e.g. EST, MDT, ...
            return 'UTC';
        },
        Z: function () { // Timezone offset in seconds (-43200...50400)
            return -jsdate.getTimezoneOffset() * 60;
        },
        // Full Date/Time
        c: function () { // ISO-8601 date.
            return 'Y-m-d\\TH:i:sP'.replace(formatChr, formatChrCb);
        },
        r: function () { // RFC 2822
            return 'D, d M Y H:i:s O'.replace(formatChr, formatChrCb);
        },
        U: function () { // Seconds since UNIX epoch
            return jsdate / 1000 | 0;
        }
    };
    this.date = function (format, timestamp) {
        that = this;
        jsdate = (timestamp === undefined ? new Date() : // Not provided
                (timestamp instanceof Date) ? new Date(timestamp) : // JS Date()
                new Date(timestamp * 1000) // UNIX timestamp (auto-convert to int)
                );
        return format.replace(formatChr, formatChrCb);
    };
    return this.date(format, timestamp);
}

function number_format(number, decimals, decPoint, thousandsSep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
    var n = !isFinite(+number) ? 0 : +number
    var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
    var s = ''

    var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec)
        return '' + (Math.round(n * k) / k)
                .toFixed(prec)
    }

    // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
    }

    return s.join(dec)
}

function commarize(val)
{
    // 1e6 = 1 Million, begin with number to word after 1e6.
    var uang = val / 1000;
    var nf = number_format(uang,'2','.',',');
    return nf + " K";
}