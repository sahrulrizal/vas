
Highcharts.chart('container', {
    chart: {
        type: 'pie',
           
        backgroundColor: "#f7f7f8",
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0,
           viewDistance: 20,
        }
    },
    exporting: { enabled: false },
    title: {
        text: 'Refill Success Rate'
    },
     credits: {
        enabled: false
    },
      subtitle: {
        text: '(2019-01-06 - 2019-02-20)'
    },


    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 80,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
    },
    series: [{
        type: 'pie',
        name: 'Browser share',
        colors: ['#336633', '#99CC33', '#ED561B', 'CC0000', '#FFFFFF'],
          fontSize: '30px',
        data: [
            ['Success Rav', 80],
            ['Success Non Rav', 17],
            ['Error', 3],
            ['Others', 0],
          
        ]
    }],
       
});



  



Highcharts.chart('linebasic', {
   chart: {
        backgroundColor: "#f7f7f8",
        type: 'line'
    },
    exporting: {
     enabled: false
      },
    title: {
        text: 'Refill All Trx (2019-01-01 - 2019-01-25)'
    },
     credits: {
        enabled: false
    },

    yAxis: {
        title: {
            text: 'Trx'
        }
    },
        xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
            day: '%e %b %Y'
        },
        tickLength: 30,
       tickInterval: 24 * 3600 * 1000
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'

    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
          
        }
    },

    series: [{
         pointStart: Date.UTC(2019, 0, 1),
        pointInterval: 24 * 3600 * 1000 *1, // one day
        name: 'Status',
        data: [190000, 170000,0,190000, 170000, 180000, 137133, 154175,43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175,43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175,43934]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 800
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});

Highcharts.chart('linebasic1', {
  chart: {
        backgroundColor: "#f7f7f8",
        type: 'line'
    },
    exporting: {
     enabled: false
      },
    title: {
        text: 'Refill Success Rate'
    },
     credits: {
        enabled: false
    },

    subtitle: {
        text: '(2019-01-1 - 2019-02-28)'
    },

    yAxis: {
        title: {
            text: ''
        }
    },
        xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
            day: '%e %b %Y'
        },
        tickLength: 60,
       tickInterval: 24 * 3600 * 1000
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'

    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
          
        }
    },
    tooltip: {
        valueSuffix: ' millions'
    },
    series: [{
        pointStart: Date.UTC(2019, 0, 1),
        pointInterval: 24 * 3600 * 1000,
        name: 'Success',
        color :'#3c763d',
        data: [5.2, 4.4,4.9, 0,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,5.2, 4.4,4.9, 0,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8,4.9, 4.8, ]
    }, {
       pointStart: Date.UTC(2019, 0, 1),
        pointInterval: 24 * 3600 * 1000,
        color :'#bbd027',
        name: 'Warning',
        data: [1.5,1.4,2.1,0.6,1,1.5,1.4,2.1,0.6,1,1.5,1.4,2.1,0.6,1,1.5,1.4,2.1,0.6,1,1.5,1.4,2.1,0.6,1,1.5,1.4,2.1,0.6,1,1.5,1.4,2.1,0.6,1,1.5,1.4,2.1,0.6,1,1.5,1.4,2.1,0.6,1,1.5,1.4,2.1,0.6,1]
    }, {
       pointStart: Date.UTC(2019, 0, 1),
        pointInterval: 24 * 3600 * 1000,
        color :'#c41815',
        name: 'Failed',
        data: [0,0,0,0,0.5,0,0,0,0,0.5,0,0,0,0,0.5,0,0,0,0,0.5,0,0,0,0,0.5,0,0,0,0,0.5,0,0,0,0,0.5,0,0,0,0,0.5,0,0,0,0,0.5,0,0,0,0,0.5,0,0,0,0,0.5,0,0,0,0,0.5]
    }],

   responsive: {
        rules: [{
            condition: {
                maxWidth: 800
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});




var perShapeGradient = {
            x1: 0,
            y1: 1,
            x2: 1,
            y2: 0
        };




var chart = new Highcharts.chart('fiddle', {
    chart: {
        backgroundColor: "#f7f7f8",
        type: 'bar',
       options3d: {
            enabled: true,
            alpha: 10,
            beta: 3,
            depth: 94,
        }
   
    },
    
    exporting: { enabled: false },
    title: {
        text: 'Refill Top 10 Error'

    },
     credits: {
        enabled: false
    },
    subtitle: {
        text: '(2019-11-01 - 2019-12-28)'
    },
    xAxis: {
        categories: ['Refill id'],
        
       labels: {
            rotation: -90
        },
    title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Population (millions)',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
            
        }
    },
    tooltip: {
        valueSuffix: ' millions'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true,
          
    },
    credits: {
        enabled: false
    },
       colors: [{
            linearGradient: perShapeGradient,
            stops: [
                [0, 'rgb(255,255,255)'],
                [1, '#8eb7f9']
                ]
            }, {
            linearGradient: perShapeGradient,
            stops: [
                [0, 'rgb(255,255,255)'],
                [1, '#f7b147']
                ]
            }, {
            linearGradient: perShapeGradient,
            stops: [
                [0, 'rgb(255,255,255)'],
                [1, '#4caf13']
                ]}, 
                {
            linearGradient: perShapeGradient,
            stops: [
                [0, 'rgb(255,255,255)'],
                [1, '#d66339']
                ]},
              {
            linearGradient: perShapeGradient,
            stops: [
                [0, 'rgb(255,255,255)'],
                [1, '#1f32e0']
                ]},
              {
            linearGradient: perShapeGradient,
            stops: [
                [0, 'rgb(255,255,255)'],
                [1, '#d30c4f']
                ]
            }, {
            linearGradient: perShapeGradient,
            stops: [
                [0, 'rgb(255,255,255)'],
                [1, '#773f52']
                ]
            }, {
            linearGradient: perShapeGradient,
            stops: [
                [0, 'rgb(255,255,255)'],
                [1, '#426342']
                ]}, 
                {
            linearGradient: perShapeGradient,
            stops: [
                [0, 'rgb(255,255,255)'],
                [1, '#b2a513']
                ]},
              {
            linearGradient: perShapeGradient,
            stops: [
                [0, 'rgb(255,255,255)'],
                [1, '#bed8e8']
                ]},



                ],
    series: [{
        name: 'P100',
        data: [1100]
    },  {
        name: 'P12X',
        data: [770]
    }, {
        name: 'P1G1',
        data: [750]
    },
    {
        name: 'P200',
        data: [500]
    },
    {
        name: 'P220',
        data: [250]
    },
    {
        name: 'P25G',
        data: [230]
    },
    {
        name: 'P40',
        data: [225]
    },
    {
        name: 'P40G',
        data: [210]
    },
    {
        name: 'P500',
        data: [150]
    },
    {
        name: 'P5G',
        data: [134]
    }]
});
